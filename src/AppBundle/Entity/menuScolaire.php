<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * menuScolaire
 *
 * @ORM\Table(name="menu_scolaire")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\menuScolaireRepository")
 */
class menuScolaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Symfony\Component\Validator\Constraints\NotBlank
     * @ORM\Column(name="name", type="string", length=50)
     * @Assert\Length(
      *      max = 50,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="actif", type="boolean")
     * @Symfony\Component\Validator\Constraints\NotBlank
     */
    private $actif = true;

    /**
     * @var string
     *
     * @ORM\Column(name="lundi_entree", type="string", length=60,  nullable=true)
     * @Assert\Length(
     *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $lundi_entree;

    /**
     * @var string
     *
     * @ORM\Column(name="lundi_plat", type="string", length=60,  nullable=true)
     * @Assert\Length(
     *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $lundi_plat;

    /**
     * @var string
     *
     * @ORM\Column(name="lundi_accompagnement", type="string", length=60,  nullable=true)
     * @Assert\Length(
     *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $lundi_accompagnement;

    /**
     * @var string
     *
     * @ORM\Column(name="lundi_dessert", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      max = 60,
      *      min = 0,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $lundi_dessert;

    /**
     * @var string
     *
     * @ORM\Column(name="mardi_entree", type="string", length=60,  nullable=true)
     * @Assert\Length(
     *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $mardi_entree;

    /**
     * @var string
     *
     * @ORM\Column(name="mardi_plat", type="string", length=60,  nullable=true)
     * @Assert\Length(
     *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $mardi_plat;

    /**
     * @var string
     *
     * @ORM\Column(name="mardi_accompagnement", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $mardi_accompagnement;

    /**
    * @var string
    *
    * @ORM\Column(name="mardi_dessert", type="string", length=60,  nullable=true)
    * @Assert\Length(
    *      min = 0,
    *      max = 60,
    *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
    * )
    */
    private $mardi_dessert;

    /**
     * @var string
     *
     * @ORM\Column(name="mercredi_entree", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $mercredi_entree;

    /**
     * @var string
     *
     * @ORM\Column(name="mercredi_plat", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $mercredi_plat;

    /**
     * @var string
     *
     * @ORM\Column(name="mercredi_accompagnement", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $mercredi_accompagnement;

    /**
     * @var string
     *
     * @ORM\Column(name="mercredi_dessert", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $mercredi_dessert;

    /**
     * @var string
     *
     * @ORM\Column(name="jeudi_entree", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $jeudi_entree;

    /**
     * @var string
     *
     * @ORM\Column(name="jeudi_plat", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $jeudi_plat;

    /**
     * @var string
     *
     * @ORM\Column(name="jeudi_accompagnement", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $jeudi_accompagnement;

    /**
     * @var string
     *
     * @ORM\Column(name="jeudi_dessert", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $jeudi_dessert;

    /**
     * @var string
     *
     * @ORM\Column(name="vendredi_entree", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $vendredi_entree;

    /**
     * @var string
     *
     * @ORM\Column(name="vendredi_plat", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $vendredi_plat;

    /**
     * @var string
     *
     * @ORM\Column(name="vendredi_accompagnement", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $vendredi_accompagnement;

    /**
     * @var string
     *
     * @ORM\Column(name="vendredi_dessert", type="string", length=60,  nullable=true)
     * @Assert\Length(
      *      min = 0,
      *      max = 60,
      *      maxMessage = "Ce champ ne peut faire plus de {{ limit }} charactères. "
      * )
     */
    private $vendredi_dessert;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return menuScolaire
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     *
     * @return menuScolaire
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return bool
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set lundiEntree
     *
     * @param string $lundiEntree
     *
     * @return menuScolaire
     */
    public function setLundiEntree($lundiEntree)
    {
        $this->lundi_entree = $lundiEntree;

        return $this;
    }

    /**
     * Get lundiEntree
     *
     * @return string
     */
    public function getLundiEntree()
    {
        return $this->lundi_entree;
    }

    /**
     * Set lundiPlat
     *
     * @param string $lundiPlat
     *
     * @return menuScolaire
     */
    public function setLundiPlat($lundiPlat)
    {
        $this->lundi_plat = $lundiPlat;

        return $this;
    }

    /**
     * Get lundiPlat
     *
     * @return string
     */
    public function getLundiPlat()
    {
        return $this->lundi_plat;
    }

    /**
     * Set lundiAccompagnement
     *
     * @param string $lundiAccompagnement
     *
     * @return menuScolaire
     */
    public function setLundiAccompagnement($lundiAccompagnement)
    {
        $this->lundi_accompagnement = $lundiAccompagnement;

        return $this;
    }

    /**
     * Get lundiAccompagnement
     *
     * @return string
     */
    public function getLundiAccompagnement()
    {
        return $this->lundi_accompagnement;
    }

    /**
     * Set lundiDessert
     *
     * @param string $lundiDessert
     *
     * @return menuScolaire
     */
    public function setLundiDessert($lundiDessert)
    {
        $this->lundi_dessert = $lundiDessert;

        return $this;
    }

    /**
     * Get lundiDessert
     *
     * @return string
     */
    public function getLundiDessert()
    {
        return $this->lundi_dessert;
    }

    /**
     * Set mardiEntree
     *
     * @param string $mardiEntree
     *
     * @return menuScolaire
     */
    public function setMardiEntree($mardiEntree)
    {
        $this->mardi_entree = $mardiEntree;

        return $this;
    }

    /**
     * Get mardiEntree
     *
     * @return string
     */
    public function getMardiEntree()
    {
        return $this->mardi_entree;
    }

    /**
     * Set mardiPlat
     *
     * @param string $mardiPlat
     *
     * @return menuScolaire
     */
    public function setMardiPlat($mardiPlat)
    {
        $this->mardi_plat = $mardiPlat;

        return $this;
    }

    /**
     * Get mardiPlat
     *
     * @return string
     */
    public function getMardiPlat()
    {
        return $this->mardi_plat;
    }

    /**
     * Set mardiAccompagnement
     *
     * @param string $mardiAccompagnement
     *
     * @return menuScolaire
     */
    public function setMardiAccompagnement($mardiAccompagnement)
    {
        $this->mardi_accompagnement = $mardiAccompagnement;

        return $this;
    }

    /**
     * Get mardiAccompagnement
     *
     * @return string
     */
    public function getMardiAccompagnement()
    {
        return $this->mardi_accompagnement;
    }

    /**
     * Set mardiDessert
     *
     * @param string $mardiDessert
     *
     * @return menuScolaire
     */
    public function setMardiDessert($mardiDessert)
    {
        $this->mardi_dessert = $mardiDessert;

        return $this;
    }

    /**
     * Get mardiDessert
     *
     * @return string
     */
    public function getMardiDessert()
    {
        return $this->mardi_dessert;
    }

    /**
     * Set mercrediEntree
     *
     * @param string $mercrediEntree
     *
     * @return menuScolaire
     */
    public function setMercrediEntree($mercrediEntree)
    {
        $this->mercredi_entree = $mercrediEntree;

        return $this;
    }

    /**
     * Get mercrediEntree
     *
     * @return string
     */
    public function getMercrediEntree()
    {
        return $this->mercredi_entree;
    }

    /**
     * Set mercrediPlat
     *
     * @param string $mercrediPlat
     *
     * @return menuScolaire
     */
    public function setMercrediPlat($mercrediPlat)
    {
        $this->mercredi_plat = $mercrediPlat;

        return $this;
    }

    /**
     * Get mercrediPlat
     *
     * @return string
     */
    public function getMercrediPlat()
    {
        return $this->mercredi_plat;
    }

    /**
     * Set mercrediAccompagnement
     *
     * @param string $mercrediAccompagnement
     *
     * @return menuScolaire
     */
    public function setMercrediAccompagnement($mercrediAccompagnement)
    {
        $this->mercredi_accompagnement = $mercrediAccompagnement;

        return $this;
    }

    /**
     * Get mercrediAccompagnement
     *
     * @return string
     */
    public function getMercrediAccompagnement()
    {
        return $this->mercredi_accompagnement;
    }

    /**
     * Set mercrediDessert
     *
     * @param string $mercrediDessert
     *
     * @return menuScolaire
     */
    public function setMercrediDessert($mercrediDessert)
    {
        $this->mercredi_dessert = $mercrediDessert;

        return $this;
    }

    /**
     * Get mercrediDessert
     *
     * @return string
     */
    public function getMercrediDessert()
    {
        return $this->mercredi_dessert;
    }

    /**
     * Set jeudiEntree
     *
     * @param string $jeudiEntree
     *
     * @return menuScolaire
     */
    public function setJeudiEntree($jeudiEntree)
    {
        $this->jeudi_entree = $jeudiEntree;

        return $this;
    }

    /**
     * Get jeudiEntree
     *
     * @return string
     */
    public function getJeudiEntree()
    {
        return $this->jeudi_entree;
    }

    /**
     * Set jeudiPlat
     *
     * @param string $jeudiPlat
     *
     * @return menuScolaire
     */
    public function setJeudiPlat($jeudiPlat)
    {
        $this->jeudi_plat = $jeudiPlat;

        return $this;
    }

    /**
     * Get jeudiPlat
     *
     * @return string
     */
    public function getJeudiPlat()
    {
        return $this->jeudi_plat;
    }

    /**
     * Set jeudiAccompagnement
     *
     * @param string $jeudiAccompagnement
     *
     * @return menuScolaire
     */
    public function setJeudiAccompagnement($jeudiAccompagnement)
    {
        $this->jeudi_accompagnement = $jeudiAccompagnement;

        return $this;
    }

    /**
     * Get jeudiAccompagnement
     *
     * @return string
     */
    public function getJeudiAccompagnement()
    {
        return $this->jeudi_accompagnement;
    }

    /**
     * Set jeudiDessert
     *
     * @param string $jeudiDessert
     *
     * @return menuScolaire
     */
    public function setJeudiDessert($jeudiDessert)
    {
        $this->jeudi_dessert = $jeudiDessert;

        return $this;
    }

    /**
     * Get jeudiDessert
     *
     * @return string
     */
    public function getJeudiDessert()
    {
        return $this->jeudi_dessert;
    }

    /**
     * Set vendrediEntree
     *
     * @param string $vendrediEntree
     *
     * @return menuScolaire
     */
    public function setVendrediEntree($vendrediEntree)
    {
        $this->vendredi_entree = $vendrediEntree;

        return $this;
    }

    /**
     * Get vendrediEntree
     *
     * @return string
     */
    public function getVendrediEntree()
    {
        return $this->vendredi_entree;
    }

    /**
     * Set vendrediPlat
     *
     * @param string $vendrediPlat
     *
     * @return menuScolaire
     */
    public function setVendrediPlat($vendrediPlat)
    {
        $this->vendredi_plat = $vendrediPlat;

        return $this;
    }

    /**
     * Get vendrediPlat
     *
     * @return string
     */
    public function getVendrediPlat()
    {
        return $this->vendredi_plat;
    }

    /**
     * Set vendrediAccompagnement
     *
     * @param string $vendrediAccompagnement
     *
     * @return menuScolaire
     */
    public function setVendrediAccompagnement($vendrediAccompagnement)
    {
        $this->vendredi_accompagnement = $vendrediAccompagnement;

        return $this;
    }

    /**
     * Get vendrediAccompagnement
     *
     * @return string
     */
    public function getVendrediAccompagnement()
    {
        return $this->vendredi_accompagnement;
    }

    /**
     * Set vendrediDessert
     *
     * @param string $vendrediDessert
     *
     * @return menuScolaire
     */
    public function setVendrediDessert($vendrediDessert)
    {
        $this->vendredi_dessert = $vendrediDessert;

        return $this;
    }

    /**
     * Get vendrediDessert
     *
     * @return string
     */
    public function getVendrediDessert()
    {
        return $this->vendredi_dessert;
    }

}
