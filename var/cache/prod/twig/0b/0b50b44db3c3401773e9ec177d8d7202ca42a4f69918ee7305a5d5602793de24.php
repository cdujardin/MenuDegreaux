<?php

/* form/fields.html.twig */
class __TwigTemplate_d84d151421fa26ed395749486c75da094b99d7f81cb87ff73ad498485e434fb5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'tags_input_widget' => array($this, 'block_tags_input_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_39e0392853d2c670073306d593dacb40111750ca49028dee9e7fa6ddb40ab9c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_39e0392853d2c670073306d593dacb40111750ca49028dee9e7fa6ddb40ab9c7->enter($__internal_39e0392853d2c670073306d593dacb40111750ca49028dee9e7fa6ddb40ab9c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/fields.html.twig"));

        // line 1
        echo "



";
        // line 5
        $this->displayBlock('tags_input_widget', $context, $blocks);
        
        $__internal_39e0392853d2c670073306d593dacb40111750ca49028dee9e7fa6ddb40ab9c7->leave($__internal_39e0392853d2c670073306d593dacb40111750ca49028dee9e7fa6ddb40ab9c7_prof);

    }

    public function block_tags_input_widget($context, array $blocks = array())
    {
        $__internal_5f25b42d352ce3db1fe4516b116b0629b3d443786e25578967ed27f79123c904 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f25b42d352ce3db1fe4516b116b0629b3d443786e25578967ed27f79123c904->enter($__internal_5f25b42d352ce3db1fe4516b116b0629b3d443786e25578967ed27f79123c904_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        // line 6
        echo "    <div class=\"input-group\">
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget', array("attr" => array("data-toggle" => "tagsinput", "data-tags" => twig_jsonencode_filter((isset($context["tags"]) ? $context["tags"] : $this->getContext($context, "tags"))))));
        echo "
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
";
        
        $__internal_5f25b42d352ce3db1fe4516b116b0629b3d443786e25578967ed27f79123c904->leave($__internal_5f25b42d352ce3db1fe4516b116b0629b3d443786e25578967ed27f79123c904_prof);

    }

    public function getTemplateName()
    {
        return "form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  44 => 7,  41 => 6,  29 => 5,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("



{% block tags_input_widget %}
    <div class=\"input-group\">
        {{ form_widget(form, {'attr': {'data-toggle': 'tagsinput', 'data-tags': tags|json_encode}}) }}
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
{% endblock %}
", "form/fields.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\form\\fields.html.twig");
    }
}
