<?php

/* :buttons:delete_button.html.twig */
class __TwigTemplate_781eee7bd30570c7d3e52a5903b5dde738c0ba40e3b53627fde00392f5071b02 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bdefdbe34a2f7c3303678a92d8959cda85209e123c0356b80feb3fe13206d683 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bdefdbe34a2f7c3303678a92d8959cda85209e123c0356b80feb3fe13206d683->enter($__internal_bdefdbe34a2f7c3303678a92d8959cda85209e123c0356b80feb3fe13206d683_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":buttons:delete_button.html.twig"));

        // line 1
        if (($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "hasAccess", array(0 => "delete", 1 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method") && $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "hasRoute", array(0 => "delete"), "method"))) {
            // line 2
            echo "
    <a href=\"";
            // line 3
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "generateObjectUrl", array(0 => "delete", 1 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), "html", null, true);
            echo "\" class=\"btn btn-sm btn-danger delete_link\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action_delete", array(), "SonataAdminBundle"), "html", null, true);
            echo "\">

        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>

        Supprimer

    </a>

";
        }
        
        $__internal_bdefdbe34a2f7c3303678a92d8959cda85209e123c0356b80feb3fe13206d683->leave($__internal_bdefdbe34a2f7c3303678a92d8959cda85209e123c0356b80feb3fe13206d683_prof);

    }

    public function getTemplateName()
    {
        return ":buttons:delete_button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  24 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if admin.hasAccess('delete', object) and admin.hasRoute('delete') %}

    <a href=\"{{ admin.generateObjectUrl('delete', object) }}\" class=\"btn btn-sm btn-danger delete_link\" title=\"{{ 'action_delete'|trans({}, 'SonataAdminBundle') }}\">

        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>

        Supprimer

    </a>

{% endif %}
", ":buttons:delete_button.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources\\views/buttons/delete_button.html.twig");
    }
}
