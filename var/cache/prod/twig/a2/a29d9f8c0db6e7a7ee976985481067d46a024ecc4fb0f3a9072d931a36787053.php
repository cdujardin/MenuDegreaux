<?php

/* menu/scolaire.html.twig */
class __TwigTemplate_53fa58b1b4c359c9df9d70371e98fd942f15d21f0f5ffe2cb8b2a15c514383d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "menu/scolaire.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5882aaa96c9dd8f97213622465087a2650319b16d329bd95bc37c08c9fff818e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5882aaa96c9dd8f97213622465087a2650319b16d329bd95bc37c08c9fff818e->enter($__internal_5882aaa96c9dd8f97213622465087a2650319b16d329bd95bc37c08c9fff818e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "menu/scolaire.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5882aaa96c9dd8f97213622465087a2650319b16d329bd95bc37c08c9fff818e->leave($__internal_5882aaa96c9dd8f97213622465087a2650319b16d329bd95bc37c08c9fff818e_prof);

    }

    // line 5
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_9d909559b43c115716f08a3ceee8b70c99748dca3795e57189f5cdadd51644ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d909559b43c115716f08a3ceee8b70c99748dca3795e57189f5cdadd51644ec->enter($__internal_9d909559b43c115716f08a3ceee8b70c99748dca3795e57189f5cdadd51644ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "menu_scolaire";
        
        $__internal_9d909559b43c115716f08a3ceee8b70c99748dca3795e57189f5cdadd51644ec->leave($__internal_9d909559b43c115716f08a3ceee8b70c99748dca3795e57189f5cdadd51644ec_prof);

    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        $__internal_e5537ec36c6aa2fc29381d75f43f3119a6f1d7d7a009948376889a43b755e942 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5537ec36c6aa2fc29381d75f43f3119a6f1d7d7a009948376889a43b755e942->enter($__internal_e5537ec36c6aa2fc29381d75f43f3119a6f1d7d7a009948376889a43b755e942_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 8
        echo "<h1> Menu Scolaire</h1>
<p class=\"subtitle\"> Menu du midi uniquement</p>

            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Lundi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Mardi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Mercredi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Jeudi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>


            </div>



";
        
        $__internal_e5537ec36c6aa2fc29381d75f43f3119a6f1d7d7a009948376889a43b755e942->leave($__internal_e5537ec36c6aa2fc29381d75f43f3119a6f1d7d7a009948376889a43b755e942_prof);

    }

    // line 93
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_91265daba1ff8c95e25ca2d00b29f2a6925343b4eba93503143344506844cc0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91265daba1ff8c95e25ca2d00b29f2a6925343b4eba93503143344506844cc0d->enter($__internal_91265daba1ff8c95e25ca2d00b29f2a6925343b4eba93503143344506844cc0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 94
        echo "<p class=\"titre\">
    Voir le menu :
</p>
<div>
    <a href=\"";
        // line 98
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
        Externe
    </a>
</div>
<div>
    <a  href=\"";
        // line 103
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
        Scolaire
    </a>
</div>
<div>
    <a href=\"";
        // line 108
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
        Résident
    </a>
</div>



";
        
        $__internal_91265daba1ff8c95e25ca2d00b29f2a6925343b4eba93503143344506844cc0d->leave($__internal_91265daba1ff8c95e25ca2d00b29f2a6925343b4eba93503143344506844cc0d_prof);

    }

    public function getTemplateName()
    {
        return "menu/scolaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 108,  224 => 103,  216 => 98,  210 => 94,  204 => 93,  186 => 81,  182 => 80,  178 => 79,  174 => 78,  160 => 67,  156 => 66,  152 => 65,  148 => 64,  131 => 50,  127 => 49,  123 => 48,  119 => 47,  105 => 36,  101 => 35,  97 => 34,  93 => 33,  79 => 22,  75 => 21,  71 => 20,  67 => 19,  54 => 8,  48 => 7,  36 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}



{% block body_id 'menu_scolaire' %}

{% block main %}
<h1> Menu Scolaire</h1>
<p class=\"subtitle\"> Menu du midi uniquement</p>

            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Lundi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.lundiEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.lundiPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.lundiAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.lundiDessert }}</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Mardi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.mardiEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.mardiPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.mardiAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.mardiDessert }}</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Mercredi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.mercrediEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.mercrediPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.mercrediAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.mercrediDessert }}</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Jeudi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.jeudiEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.jeudiPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.jeudiAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.jeudiDessert }}</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.vendrediEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.vendrediPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.vendrediAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.vendrediDessert }}</div>
                        </div>
                    </div>
                </div>


            </div>



{% endblock %}

{% block sidebar %}
<p class=\"titre\">
    Voir le menu :
</p>
<div>
    <a href=\"{{ path('externe') }}\" >
        Externe
    </a>
</div>
<div>
    <a  href=\"{{ path('scolaire') }}\">
        Scolaire
    </a>
</div>
<div>
    <a href=\"{{ path('resident') }}\" >
        Résident
    </a>
</div>



{% endblock %}
", "menu/scolaire.html.twig", "/home/lievininqd/restauration/app/Resources/views/menu/scolaire.html.twig");
    }
}
