<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_348c0ca4c7641981cbce796be340a8cffbbc23a29c5608ed2bf3e23d557f33c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0076672b53a01ce12d7a802828d926e39a59ff64f6b6c3598b75bfb865f868e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0076672b53a01ce12d7a802828d926e39a59ff64f6b6c3598b75bfb865f868e2->enter($__internal_0076672b53a01ce12d7a802828d926e39a59ff64f6b6c3598b75bfb865f868e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0076672b53a01ce12d7a802828d926e39a59ff64f6b6c3598b75bfb865f868e2->leave($__internal_0076672b53a01ce12d7a802828d926e39a59ff64f6b6c3598b75bfb865f868e2_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_3c73410f6a4553148f496b905f9674e2fcd8b242d3d8bb95dfba6b4334648a66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c73410f6a4553148f496b905f9674e2fcd8b242d3d8bb95dfba6b4334648a66->enter($__internal_3c73410f6a4553148f496b905f9674e2fcd8b242d3d8bb95dfba6b4334648a66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Se connecter";
        
        $__internal_3c73410f6a4553148f496b905f9674e2fcd8b242d3d8bb95dfba6b4334648a66->leave($__internal_3c73410f6a4553148f496b905f9674e2fcd8b242d3d8bb95dfba6b4334648a66_prof);

    }

    // line 5
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_cde0e9a7774cd2db67510273e206e9e4aedd33b9738569b954887028d090d620 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cde0e9a7774cd2db67510273e206e9e4aedd33b9738569b954887028d090d620->enter($__internal_cde0e9a7774cd2db67510273e206e9e4aedd33b9738569b954887028d090d620_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "login";
        
        $__internal_cde0e9a7774cd2db67510273e206e9e4aedd33b9738569b954887028d090d620->leave($__internal_cde0e9a7774cd2db67510273e206e9e4aedd33b9738569b954887028d090d620_prof);

    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        $__internal_46adac5f5cc0baffca0889baebdc172f5fd4c4b62b9118d05767f9463e53c32f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46adac5f5cc0baffca0889baebdc172f5fd4c4b62b9118d05767f9463e53c32f->enter($__internal_46adac5f5cc0baffca0889baebdc172f5fd4c4b62b9118d05767f9463e53c32f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 8
        echo "
    ";
        // line 9
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_46adac5f5cc0baffca0889baebdc172f5fd4c4b62b9118d05767f9463e53c32f->leave($__internal_46adac5f5cc0baffca0889baebdc172f5fd4c4b62b9118d05767f9463e53c32f_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 9,  66 => 8,  60 => 7,  48 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

{% block title %}Se connecter{% endblock %}

{% block body_id 'login' %}

{% block main %}

    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock main %}
", "@FOSUser/Security/login.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\FOSUserBundle\\views\\Security\\login.html.twig");
    }
}
