<?php

/* menu/homepage.html.twig */
class __TwigTemplate_28a9a9d2964c74478cf7c28f9ee90a0aa71551945681467d1d8fae0960bcde83 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "menu/homepage.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8dd1ed39599ff4cc65609740def3eb642f1d7f013dc8a48123b5f69d6b9544f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8dd1ed39599ff4cc65609740def3eb642f1d7f013dc8a48123b5f69d6b9544f4->enter($__internal_8dd1ed39599ff4cc65609740def3eb642f1d7f013dc8a48123b5f69d6b9544f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "menu/homepage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8dd1ed39599ff4cc65609740def3eb642f1d7f013dc8a48123b5f69d6b9544f4->leave($__internal_8dd1ed39599ff4cc65609740def3eb642f1d7f013dc8a48123b5f69d6b9544f4_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_eed3b44dc33f17aad11d1a920345f44a56239e6774b6d05530ccf65fdb3229db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eed3b44dc33f17aad11d1a920345f44a56239e6774b6d05530ccf65fdb3229db->enter($__internal_eed3b44dc33f17aad11d1a920345f44a56239e6774b6d05530ccf65fdb3229db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "homepage";
        
        $__internal_eed3b44dc33f17aad11d1a920345f44a56239e6774b6d05530ccf65fdb3229db->leave($__internal_eed3b44dc33f17aad11d1a920345f44a56239e6774b6d05530ccf65fdb3229db_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_a406ab40e6f80d61d03d16a68f63b9cb81e2db540b521d37ea5ee0151156c5e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a406ab40e6f80d61d03d16a68f63b9cb81e2db540b521d37ea5ee0151156c5e9->enter($__internal_a406ab40e6f80d61d03d16a68f63b9cb81e2db540b521d37ea5ee0151156c5e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    <h1> Bienvenue sur le menu Degreaux</h1>
    <!-- choix 3 btn -->
    <div class=\"row categories\">
      <p> Vous êtes : </p>
      <div class=\"col-md-4 col-xs-12\">
          <a href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
            <button id=\"externebtn\" type=\"button\" class=\"btn btn-default\" >
                Externe
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a  href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
            <button id=\"scolairebtn\"  type=\"button\" class=\"btn btn-default \">
                Scolaire
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
            <button id=\"residentbtn\"   type=\"button\" class=\"btn btn-default \">
                Résident
            </button>
        </a>
      </div>
    </div>
";
        
        $__internal_a406ab40e6f80d61d03d16a68f63b9cb81e2db540b521d37ea5ee0151156c5e9->leave($__internal_a406ab40e6f80d61d03d16a68f63b9cb81e2db540b521d37ea5ee0151156c5e9_prof);

    }

    public function getTemplateName()
    {
        return "menu/homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 25,  70 => 18,  60 => 11,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'homepage' %}

{% block main %}
    <h1> Bienvenue sur le menu Degreaux</h1>
    <!-- choix 3 btn -->
    <div class=\"row categories\">
      <p> Vous êtes : </p>
      <div class=\"col-md-4 col-xs-12\">
          <a href=\"{{ path('externe') }}\" >
            <button id=\"externebtn\" type=\"button\" class=\"btn btn-default\" >
                Externe
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a  href=\"{{ path('scolaire') }}\">
            <button id=\"scolairebtn\"  type=\"button\" class=\"btn btn-default \">
                Scolaire
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a href=\"{{ path('resident') }}\" >
            <button id=\"residentbtn\"   type=\"button\" class=\"btn btn-default \">
                Résident
            </button>
        </a>
      </div>
    </div>
{% endblock %}
", "menu/homepage.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\menu\\homepage.html.twig");
    }
}
