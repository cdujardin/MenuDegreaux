<?php

/* menu/resident.html.twig */
class __TwigTemplate_554c2ef918b38db72ad8285e6f850bcb4a5070fe6353b9c3f8a465734c43a8c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "menu/resident.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        echo "menu_resident";
    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        // line 6
        echo "<h1> Menu Résident</h1>
<p class=\"subtitle\"> Menu du midi et du soir</p>


    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Lundi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "lundiEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "lundiPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "lundiDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "lundiEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "lundiPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "lundiDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

    </div>


    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Mardi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mardiEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mardiPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mardiDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "mardiEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "mardiPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "mardiDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

    </div>

    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Mercredi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 76
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mercrediDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 89
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "mercrediDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

    </div>
    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Jeudi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "JeudiEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 104
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "JeudiPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 105
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "JeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 106
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "JeudiDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "JeudiEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "JeudiPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "JeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 116
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "JeudiDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

    </div>

    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Vendredi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 131
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 132
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 133
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 134
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "vendrediDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 141
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 142
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 143
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 144
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "vendrediDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

    </div>

    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Samedi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 159
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "samediEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 160
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "samediPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 161
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "samediAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 162
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "samediDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 169
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "samediEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 170
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "samediPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 171
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "samediAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 172
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : null), "samediDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

    </div>
    <div class=\"row ligne\">

    <div class=\"col-md-2 col-xs-12\">
        <div class=\"titre\">Dimanche</div>
    </div>

    <div class=\"col-md-5 col-xs-12\">
        <div class=\"titre\">Midi</div>
        <div class=\"menu\">
            <div class=\" text\">";
        // line 186
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "dimancheEntree", array()), "html", null, true);
        echo "</div>
            <div class=\" text\">";
        // line 187
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "dimanchePlat", array()), "html", null, true);
        echo "</div>
            <div class=\"text\">";
        // line 188
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "dimancheAccompagnement", array()), "html", null, true);
        echo "</div>
            <div class=\" text\">";
        // line 189
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "dimancheDessert", array()), "html", null, true);
        echo "</div>
        </div>
    </div>
    <div class=\"col-md-5 col-xs-12\">
        <div class=\"titre\">Soir</div>
        <div class=\"menu\">
            <div class=\" text\">Pas de menu le dimanche soir</div>
        </div>
    </div>

</div>


";
    }

    // line 204
    public function block_sidebar($context, array $blocks = array())
    {
        // line 205
        echo "<p class=\"titre\">
    Voir le menu :
</p>
<div>
    <a href=\"";
        // line 209
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
        Externe
    </a>
</div>
<div>
    <a  href=\"";
        // line 214
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
        Scolaire
    </a>
</div>
<div>
    <a href=\"";
        // line 219
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
        Résident
    </a>
</div>



";
    }

    public function getTemplateName()
    {
        return "menu/resident.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  420 => 219,  412 => 214,  404 => 209,  398 => 205,  395 => 204,  377 => 189,  373 => 188,  369 => 187,  365 => 186,  348 => 172,  344 => 171,  340 => 170,  336 => 169,  326 => 162,  322 => 161,  318 => 160,  314 => 159,  296 => 144,  292 => 143,  288 => 142,  284 => 141,  274 => 134,  270 => 133,  266 => 132,  262 => 131,  244 => 116,  240 => 115,  236 => 114,  232 => 113,  222 => 106,  218 => 105,  214 => 104,  210 => 103,  193 => 89,  189 => 88,  185 => 87,  181 => 86,  171 => 79,  167 => 78,  163 => 77,  159 => 76,  141 => 61,  137 => 60,  133 => 59,  129 => 58,  119 => 51,  115 => 50,  111 => 49,  107 => 48,  88 => 32,  84 => 31,  80 => 30,  76 => 29,  66 => 22,  62 => 21,  58 => 20,  54 => 19,  39 => 6,  36 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "menu/resident.html.twig", "/home/lievininqd/restauration/app/Resources/views/menu/resident.html.twig");
    }
}
