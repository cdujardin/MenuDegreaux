<?php

/* base.html.twig */
class __TwigTemplate_b96bc874382880893d5de9e20f4162bdeb8300e83c18edad4edafa8f21eb964c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'header_navigation_links' => array($this, 'block_header_navigation_links'),
            'body' => array($this, 'block_body'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9c30cd89a868a2c70bee1e946c8bd799008729f6b7b049911d37733f8bd0fad4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c30cd89a868a2c70bee1e946c8bd799008729f6b7b049911d37733f8bd0fad4->enter($__internal_9c30cd89a868a2c70bee1e946c8bd799008729f6b7b049911d37733f8bd0fad4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "locale", array()), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <!-- police -->
        <link href=\"https://fonts.googleapis.com/css?family=Dosis\" rel=\"stylesheet\">
        <!-- Favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.png"), "html", null, true);
        echo "\" >
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-flatly-3.3.7.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-awesome-4.6.3.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-lato.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/highlight-solarized-light.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-tagsinput.css"), "html", null, true);
        echo "\">
        <!-- style perso -->
        <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/css_sass/css/style.css"), "html", null, true);
        echo "\">
        ";
        // line 19
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 22
        echo "    </head>

    <body id=\"";
        // line 24
        $this->displayBlock('body_id', $context, $blocks);
        echo "\">

        ";
        // line 26
        $this->displayBlock('header', $context, $blocks);
        // line 143
        echo "
        <div class=\"container body-container\">
            ";
        // line 145
        $this->displayBlock('body', $context, $blocks);
        // line 159
        echo "        </div>

        ";
        // line 161
        $this->displayBlock('footer', $context, $blocks);
        // line 172
        echo "
        ";
        // line 173
        $this->displayBlock('javascripts', $context, $blocks);
        // line 182
        echo "

    </body>
</html>
";
        
        $__internal_9c30cd89a868a2c70bee1e946c8bd799008729f6b7b049911d37733f8bd0fad4->leave($__internal_9c30cd89a868a2c70bee1e946c8bd799008729f6b7b049911d37733f8bd0fad4_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_ca925e0fdf0d599aebf638007bfc76ae6806079a9e0e06b97edf3e7e4fcf313e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca925e0fdf0d599aebf638007bfc76ae6806079a9e0e06b97edf3e7e4fcf313e->enter($__internal_ca925e0fdf0d599aebf638007bfc76ae6806079a9e0e06b97edf3e7e4fcf313e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Menu Degreaux";
        
        $__internal_ca925e0fdf0d599aebf638007bfc76ae6806079a9e0e06b97edf3e7e4fcf313e->leave($__internal_ca925e0fdf0d599aebf638007bfc76ae6806079a9e0e06b97edf3e7e4fcf313e_prof);

    }

    // line 19
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6e9a1c5f8e6734f9c7163c94cb8a38c3edc8bc3c904cd9fbbd16a4811d07c790 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e9a1c5f8e6734f9c7163c94cb8a38c3edc8bc3c904cd9fbbd16a4811d07c790->enter($__internal_6e9a1c5f8e6734f9c7163c94cb8a38c3edc8bc3c904cd9fbbd16a4811d07c790_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 20
        echo "
        ";
        
        $__internal_6e9a1c5f8e6734f9c7163c94cb8a38c3edc8bc3c904cd9fbbd16a4811d07c790->leave($__internal_6e9a1c5f8e6734f9c7163c94cb8a38c3edc8bc3c904cd9fbbd16a4811d07c790_prof);

    }

    // line 24
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_1d4eb20b656767b412b75d4983bf0efb0b0c616d7c32cb2e5e424eac7de4cca5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d4eb20b656767b412b75d4983bf0efb0b0c616d7c32cb2e5e424eac7de4cca5->enter($__internal_1d4eb20b656767b412b75d4983bf0efb0b0c616d7c32cb2e5e424eac7de4cca5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_1d4eb20b656767b412b75d4983bf0efb0b0c616d7c32cb2e5e424eac7de4cca5->leave($__internal_1d4eb20b656767b412b75d4983bf0efb0b0c616d7c32cb2e5e424eac7de4cca5_prof);

    }

    // line 26
    public function block_header($context, array $blocks = array())
    {
        $__internal_6896e20091ca10d9490560255b0722ca9573958bcfd6e77229895b6c41569c14 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6896e20091ca10d9490560255b0722ca9573958bcfd6e77229895b6c41569c14->enter($__internal_6896e20091ca10d9490560255b0722ca9573958bcfd6e77229895b6c41569c14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 27
        echo "            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                Menu Degreaux
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Menu</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">
                                ";
        // line 45
        $this->displayBlock('header_navigation_links', $context, $blocks);
        // line 132
        echo "




                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        ";
        
        $__internal_6896e20091ca10d9490560255b0722ca9573958bcfd6e77229895b6c41569c14->leave($__internal_6896e20091ca10d9490560255b0722ca9573958bcfd6e77229895b6c41569c14_prof);

    }

    // line 45
    public function block_header_navigation_links($context, array $blocks = array())
    {
        $__internal_8a35964f2dd71456a883da129abba09b318ee8d7d16e92322734d3bf46ad1c22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a35964f2dd71456a883da129abba09b318ee8d7d16e92322734d3bf46ad1c22->enter($__internal_8a35964f2dd71456a883da129abba09b318ee8d7d16e92322734d3bf46ad1c22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        // line 46
        echo "
                                    ";
        // line 47
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 48
            echo "                                        <li class=\"dropdown\">
                                                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                      ";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo "
                                                       <span class=\"caret\"></span>
                                                   </a>
                                                  <ul class=\"dropdown-menu\">
                                                    <li>
                                                        <a href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/profile"), "html", null, true);
            echo "\">
                                                            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> Profil
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/resseting"), "html", null, true);
            echo "\">
                                                            <i class=\"fa fa-key\" aria-hidden=\"true\"></i> Changer le mot de passe
                                                        </a>
                                                    </li>
                                                    ";
            // line 64
            if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
                // line 65
                echo "                                                        <li>
                                                            <a href=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/register"), "html", null, true);
                echo "\">
                                                                <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i> Ajouter un utilisateur
                                                            </a>
                                                        </li>
                                                    ";
            }
            // line 71
            echo "                                                  </ul>
                                                </li>

                                    ";
        }
        // line 75
        echo "
                                    <li>
                                        <a href=\"";
        // line 77
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>
                                            Homepage
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"http://lievin.fr/\" target=\"_blank\">
                                            <i class=\"fa fa-reply \" aria-hidden=\"true\"></i>
                                            Ville de Liévin
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:window.print()\">
                                            <i class=\"fa fa-print \" aria-hidden=\"true\"></i>
                                             Imprimer
                                        </a>
                                    </li>




                                    ";
        // line 98
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 99
            echo "                                        <li>
                                            <a href=\"";
            // line 100
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/adminSonata"), "html", null, true);
            echo "\">

                                                <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> Gestion des menus
                                            </a>
                                        </li>
                                    ";
        }
        // line 106
        echo "
                                    ";
        // line 107
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 108
            echo "                                        <li>
                                            <a href=\"";
            // line 109
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
                                                <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>
                                                ";
            // line 111
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a>
                                        </li>
                                        ";
        } else {
            // line 115
            echo "                                        <li>
                                            <a href=\"";
            // line 116
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">
                                                <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>
                                                ";
            // line 118
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a>
                                        </li>
                                    ";
        }
        // line 122
        echo "
                                    ";
        // line 123
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "all", array()));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 124
            echo "                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 125
                echo "                                            <div class=\"";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\">
                                                ";
                // line 126
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["message"], array(), "FOSUserBundle"), "html", null, true);
                echo "
                                            </div>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 129
            echo "                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "
                                ";
        
        $__internal_8a35964f2dd71456a883da129abba09b318ee8d7d16e92322734d3bf46ad1c22->leave($__internal_8a35964f2dd71456a883da129abba09b318ee8d7d16e92322734d3bf46ad1c22_prof);

    }

    // line 145
    public function block_body($context, array $blocks = array())
    {
        $__internal_526382405e9f5e1d73956251574e693ca72df49027d74b3b3a6317d2a0544150 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_526382405e9f5e1d73956251574e693ca72df49027d74b3b3a6317d2a0544150->enter($__internal_526382405e9f5e1d73956251574e693ca72df49027d74b3b3a6317d2a0544150_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 146
        echo "                ";
        echo twig_include($this->env, $context, "default/_flash_messages.html.twig");
        echo "
                <div class=\"row\">
                    <div id=\"main\" class=\"col-md-9 col-xs-12\">
                        ";
        // line 149
        $this->displayBlock('main', $context, $blocks);
        // line 151
        echo "                    </div>

                    <div id=\"sidebar\" class=\"col-md-3 col-xs-12\">
                        ";
        // line 154
        $this->displayBlock('sidebar', $context, $blocks);
        // line 156
        echo "                    </div>
                </div>
            ";
        
        $__internal_526382405e9f5e1d73956251574e693ca72df49027d74b3b3a6317d2a0544150->leave($__internal_526382405e9f5e1d73956251574e693ca72df49027d74b3b3a6317d2a0544150_prof);

    }

    // line 149
    public function block_main($context, array $blocks = array())
    {
        $__internal_cecb03f77edbdf0b85ae616b116eea185ecf765b8fca2e50b0f94083ad0ea0af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cecb03f77edbdf0b85ae616b116eea185ecf765b8fca2e50b0f94083ad0ea0af->enter($__internal_cecb03f77edbdf0b85ae616b116eea185ecf765b8fca2e50b0f94083ad0ea0af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 150
        echo "                        ";
        
        $__internal_cecb03f77edbdf0b85ae616b116eea185ecf765b8fca2e50b0f94083ad0ea0af->leave($__internal_cecb03f77edbdf0b85ae616b116eea185ecf765b8fca2e50b0f94083ad0ea0af_prof);

    }

    // line 154
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_094c56f67dbdbb748d923b1a0780a98d2310aca67bb8da7f8d69d72ee9031af0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_094c56f67dbdbb748d923b1a0780a98d2310aca67bb8da7f8d69d72ee9031af0->enter($__internal_094c56f67dbdbb748d923b1a0780a98d2310aca67bb8da7f8d69d72ee9031af0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 155
        echo "                        ";
        
        $__internal_094c56f67dbdbb748d923b1a0780a98d2310aca67bb8da7f8d69d72ee9031af0->leave($__internal_094c56f67dbdbb748d923b1a0780a98d2310aca67bb8da7f8d69d72ee9031af0_prof);

    }

    // line 161
    public function block_footer($context, array $blocks = array())
    {
        $__internal_594336d14ccdc39e4a7bd7462f3956306ab960d28332dc4db53ecc440647cf52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_594336d14ccdc39e4a7bd7462f3956306ab960d28332dc4db53ecc440647cf52->enter($__internal_594336d14ccdc39e4a7bd7462f3956306ab960d28332dc4db53ecc440647cf52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 162
        echo "            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-12 col-xs-12\">
                            <p>&copy; ";
        // line 166
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " - Liévin</p>
                        </div>
                    </div>
                </div>
            </footer>
        ";
        
        $__internal_594336d14ccdc39e4a7bd7462f3956306ab960d28332dc4db53ecc440647cf52->leave($__internal_594336d14ccdc39e4a7bd7462f3956306ab960d28332dc4db53ecc440647cf52_prof);

    }

    // line 173
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_d6db175db620f0bf8d7553b01573854f61a2e4021381a234e6924a974a983c7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6db175db620f0bf8d7553b01573854f61a2e4021381a234e6924a974a983c7f->enter($__internal_d6db175db620f0bf8d7553b01573854f61a2e4021381a234e6924a974a983c7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 174
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.4.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/moment.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-3.3.7.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/highlight.pack.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-tagsinput.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_d6db175db620f0bf8d7553b01573854f61a2e4021381a234e6924a974a983c7f->leave($__internal_d6db175db620f0bf8d7553b01573854f61a2e4021381a234e6924a974a983c7f_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  495 => 180,  491 => 179,  487 => 178,  483 => 177,  479 => 176,  475 => 175,  470 => 174,  464 => 173,  451 => 166,  445 => 162,  439 => 161,  432 => 155,  426 => 154,  419 => 150,  413 => 149,  404 => 156,  402 => 154,  397 => 151,  395 => 149,  388 => 146,  382 => 145,  374 => 130,  368 => 129,  359 => 126,  354 => 125,  349 => 124,  345 => 123,  342 => 122,  335 => 118,  330 => 116,  327 => 115,  320 => 111,  315 => 109,  312 => 108,  310 => 107,  307 => 106,  298 => 100,  295 => 99,  293 => 98,  269 => 77,  265 => 75,  259 => 71,  251 => 66,  248 => 65,  246 => 64,  239 => 60,  231 => 55,  223 => 50,  219 => 48,  217 => 47,  214 => 46,  208 => 45,  191 => 132,  189 => 45,  172 => 31,  166 => 27,  160 => 26,  149 => 24,  141 => 20,  135 => 19,  123 => 6,  112 => 182,  110 => 173,  107 => 172,  105 => 161,  101 => 159,  99 => 145,  95 => 143,  93 => 26,  88 => 24,  84 => 22,  82 => 19,  78 => 18,  73 => 16,  69 => 15,  65 => 14,  61 => 13,  57 => 12,  53 => 11,  49 => 10,  42 => 6,  35 => 2,  32 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"{{ app.request.locale }}\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>{% block title %}Menu Degreaux{% endblock %}</title>
        <!-- police -->
        <link href=\"https://fonts.googleapis.com/css?family=Dosis\" rel=\"stylesheet\">
        <!-- Favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.png') }}\" >
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-flatly-3.3.7.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/font-awesome-4.6.3.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/font-lato.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-datetimepicker.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/highlight-solarized-light.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-tagsinput.css') }}\">
        <!-- style perso -->
        <link rel=\"stylesheet\" href=\"{{ asset('css/css_sass/css/style.css') }}\">
        {% block stylesheets %}

        {% endblock %}
    </head>

    <body id=\"{% block body_id %}{% endblock %}\">

        {% block header %}
            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"{{ path('homepage') }}\">
                                Menu Degreaux
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Menu</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">
                                {% block header_navigation_links %}

                                    {% if is_granted(\"ROLE_USER\") %}
                                        <li class=\"dropdown\">
                                                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                      {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }}
                                                       <span class=\"caret\"></span>
                                                   </a>
                                                  <ul class=\"dropdown-menu\">
                                                    <li>
                                                        <a href=\"{{ asset('/profile') }}\">
                                                            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> Profil
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"{{ asset('/resseting') }}\">
                                                            <i class=\"fa fa-key\" aria-hidden=\"true\"></i> Changer le mot de passe
                                                        </a>
                                                    </li>
                                                    {% if is_granted(\"ROLE_ADMIN\") %}
                                                        <li>
                                                            <a href=\"{{ asset('/register') }}\">
                                                                <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i> Ajouter un utilisateur
                                                            </a>
                                                        </li>
                                                    {% endif %}
                                                  </ul>
                                                </li>

                                    {% endif %}

                                    <li>
                                        <a href=\"{{ path('homepage') }}\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>
                                            Homepage
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"http://lievin.fr/\" target=\"_blank\">
                                            <i class=\"fa fa-reply \" aria-hidden=\"true\"></i>
                                            Ville de Liévin
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:window.print()\">
                                            <i class=\"fa fa-print \" aria-hidden=\"true\"></i>
                                             Imprimer
                                        </a>
                                    </li>




                                    {% if is_granted(\"ROLE_USER\") %}
                                        <li>
                                            <a href=\"{{ asset('/adminSonata') }}\">

                                                <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> Gestion des menus
                                            </a>
                                        </li>
                                    {% endif %}

                                    {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                                        <li>
                                            <a href=\"{{ path('fos_user_security_logout') }}\">
                                                <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>
                                                {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                                            </a>
                                        </li>
                                        {% else %}
                                        <li>
                                            <a href=\"{{ path('fos_user_security_login') }}\">
                                                <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>
                                                {{ 'layout.login'|trans({}, 'FOSUserBundle') }}
                                            </a>
                                        </li>
                                    {% endif %}

                                    {% for type, messages in app.session.flashBag.all %}
                                        {% for message in messages %}
                                            <div class=\"{{ type }}\">
                                                {{ message|trans({}, 'FOSUserBundle') }}
                                            </div>
                                        {% endfor %}
                                    {% endfor %}

                                {% endblock %}





                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        {% endblock %}

        <div class=\"container body-container\">
            {% block body %}
                {{ include('default/_flash_messages.html.twig') }}
                <div class=\"row\">
                    <div id=\"main\" class=\"col-md-9 col-xs-12\">
                        {% block main %}
                        {% endblock %}
                    </div>

                    <div id=\"sidebar\" class=\"col-md-3 col-xs-12\">
                        {% block sidebar %}
                        {% endblock %}
                    </div>
                </div>
            {% endblock %}
        </div>

        {% block footer %}
            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-12 col-xs-12\">
                            <p>&copy; {{ 'now'|date('Y') }} - Liévin</p>
                        </div>
                    </div>
                </div>
            </footer>
        {% endblock %}

        {% block javascripts %}
            <script src=\"{{ asset('js/jquery-2.2.4.min.js') }}\"></script>
            <script src=\"{{ asset('js/moment.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-3.3.7.min.js') }}\"></script>
            <script src=\"{{ asset('js/highlight.pack.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-datetimepicker.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-tagsinput.min.js') }}\"></script>
            <script src=\"{{ asset('js/main.js') }}\"></script>
        {% endblock %}


    </body>
</html>
", "base.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\base.html.twig");
    }
}
