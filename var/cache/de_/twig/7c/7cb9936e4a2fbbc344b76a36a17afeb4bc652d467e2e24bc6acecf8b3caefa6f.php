<?php

/* SonataAdminBundle:CRUD:base_acl.html.twig */
class __TwigTemplate_9382e4da974cc76acf46577b5ab162e862ebcc9a6faa1939bc1bf301c171c16e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'actions' => array($this, 'block_actions'),
            'form' => array($this, 'block_form'),
            'form_acl_roles' => array($this, 'block_form_acl_roles'),
            'form_acl_users' => array($this, 'block_form_acl_users'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:base_acl.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ae85274c4b2a73fd58c1f37a2f54975eee2d70965e1e56516a66d7f8788c27d5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae85274c4b2a73fd58c1f37a2f54975eee2d70965e1e56516a66d7f8788c27d5->enter($__internal_ae85274c4b2a73fd58c1f37a2f54975eee2d70965e1e56516a66d7f8788c27d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_acl.html.twig"));

        $__internal_ed3f87f2ee7079c19480d9994a10f62b1092d838d39b8ed8f9f8c773353cf898 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed3f87f2ee7079c19480d9994a10f62b1092d838d39b8ed8f9f8c773353cf898->enter($__internal_ed3f87f2ee7079c19480d9994a10f62b1092d838d39b8ed8f9f8c773353cf898_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_acl.html.twig"));

        // line 18
        $context["acl"] = $this->loadTemplate("SonataAdminBundle:CRUD:base_acl_macro.html.twig", "SonataAdminBundle:CRUD:base_acl.html.twig", 18);
        // line 12
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ae85274c4b2a73fd58c1f37a2f54975eee2d70965e1e56516a66d7f8788c27d5->leave($__internal_ae85274c4b2a73fd58c1f37a2f54975eee2d70965e1e56516a66d7f8788c27d5_prof);

        
        $__internal_ed3f87f2ee7079c19480d9994a10f62b1092d838d39b8ed8f9f8c773353cf898->leave($__internal_ed3f87f2ee7079c19480d9994a10f62b1092d838d39b8ed8f9f8c773353cf898_prof);

    }

    // line 14
    public function block_actions($context, array $blocks = array())
    {
        $__internal_c3f98dfad495da369f77c6a34cca4575c955ba5bc4cded0681cf7f2fbcf2b988 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3f98dfad495da369f77c6a34cca4575c955ba5bc4cded0681cf7f2fbcf2b988->enter($__internal_c3f98dfad495da369f77c6a34cca4575c955ba5bc4cded0681cf7f2fbcf2b988_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        $__internal_0b7e3c024f78ee760879dff42888a15a4dfa6603a60183841e9b1a3dca8fae48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b7e3c024f78ee760879dff42888a15a4dfa6603a60183841e9b1a3dca8fae48->enter($__internal_0b7e3c024f78ee760879dff42888a15a4dfa6603a60183841e9b1a3dca8fae48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:action_buttons.html.twig", "SonataAdminBundle:CRUD:base_acl.html.twig", 15)->display($context);
        
        $__internal_0b7e3c024f78ee760879dff42888a15a4dfa6603a60183841e9b1a3dca8fae48->leave($__internal_0b7e3c024f78ee760879dff42888a15a4dfa6603a60183841e9b1a3dca8fae48_prof);

        
        $__internal_c3f98dfad495da369f77c6a34cca4575c955ba5bc4cded0681cf7f2fbcf2b988->leave($__internal_c3f98dfad495da369f77c6a34cca4575c955ba5bc4cded0681cf7f2fbcf2b988_prof);

    }

    // line 20
    public function block_form($context, array $blocks = array())
    {
        $__internal_95884649a0ac1aec507dabd162ef0b8bdbc255f5442f4c8b5a8b7814df134ee8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95884649a0ac1aec507dabd162ef0b8bdbc255f5442f4c8b5a8b7814df134ee8->enter($__internal_95884649a0ac1aec507dabd162ef0b8bdbc255f5442f4c8b5a8b7814df134ee8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        $__internal_a8ef3cfd508d8597892117751e931a454ce841fa55b707c72e51d619ad12effe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a8ef3cfd508d8597892117751e931a454ce841fa55b707c72e51d619ad12effe->enter($__internal_a8ef3cfd508d8597892117751e931a454ce841fa55b707c72e51d619ad12effe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 21
        echo "    ";
        $this->displayBlock('form_acl_roles', $context, $blocks);
        // line 24
        echo "    ";
        $this->displayBlock('form_acl_users', $context, $blocks);
        
        $__internal_a8ef3cfd508d8597892117751e931a454ce841fa55b707c72e51d619ad12effe->leave($__internal_a8ef3cfd508d8597892117751e931a454ce841fa55b707c72e51d619ad12effe_prof);

        
        $__internal_95884649a0ac1aec507dabd162ef0b8bdbc255f5442f4c8b5a8b7814df134ee8->leave($__internal_95884649a0ac1aec507dabd162ef0b8bdbc255f5442f4c8b5a8b7814df134ee8_prof);

    }

    // line 21
    public function block_form_acl_roles($context, array $blocks = array())
    {
        $__internal_80161f88d83f927c9418fbf3f033e9e693bbb256b731a728a86954e6a6807853 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80161f88d83f927c9418fbf3f033e9e693bbb256b731a728a86954e6a6807853->enter($__internal_80161f88d83f927c9418fbf3f033e9e693bbb256b731a728a86954e6a6807853_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_acl_roles"));

        $__internal_e5e657981b328e0ee8c025ff00fce245e1513c981fea9e514ac87d0f79f3d6b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5e657981b328e0ee8c025ff00fce245e1513c981fea9e514ac87d0f79f3d6b9->enter($__internal_e5e657981b328e0ee8c025ff00fce245e1513c981fea9e514ac87d0f79f3d6b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_acl_roles"));

        // line 22
        echo "        ";
        echo $context["acl"]->getrender_form((isset($context["aclRolesForm"]) ? $context["aclRolesForm"] : $this->getContext($context, "aclRolesForm")), (isset($context["permissions"]) ? $context["permissions"] : $this->getContext($context, "permissions")), "td_role", (isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), $this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object")));
        echo "
    ";
        
        $__internal_e5e657981b328e0ee8c025ff00fce245e1513c981fea9e514ac87d0f79f3d6b9->leave($__internal_e5e657981b328e0ee8c025ff00fce245e1513c981fea9e514ac87d0f79f3d6b9_prof);

        
        $__internal_80161f88d83f927c9418fbf3f033e9e693bbb256b731a728a86954e6a6807853->leave($__internal_80161f88d83f927c9418fbf3f033e9e693bbb256b731a728a86954e6a6807853_prof);

    }

    // line 24
    public function block_form_acl_users($context, array $blocks = array())
    {
        $__internal_e2d82005b4a8ab74d110bec30ed9cd5804903b4a9c600131327f292b810d1013 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e2d82005b4a8ab74d110bec30ed9cd5804903b4a9c600131327f292b810d1013->enter($__internal_e2d82005b4a8ab74d110bec30ed9cd5804903b4a9c600131327f292b810d1013_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_acl_users"));

        $__internal_52c45fd1dfe1a73bccc7d675f42f1fc145cd196bc35a6f258fc8ed931ec80323 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52c45fd1dfe1a73bccc7d675f42f1fc145cd196bc35a6f258fc8ed931ec80323->enter($__internal_52c45fd1dfe1a73bccc7d675f42f1fc145cd196bc35a6f258fc8ed931ec80323_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_acl_users"));

        // line 25
        echo "        ";
        echo $context["acl"]->getrender_form((isset($context["aclUsersForm"]) ? $context["aclUsersForm"] : $this->getContext($context, "aclUsersForm")), (isset($context["permissions"]) ? $context["permissions"] : $this->getContext($context, "permissions")), "td_username", (isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), $this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object")));
        echo "
    ";
        
        $__internal_52c45fd1dfe1a73bccc7d675f42f1fc145cd196bc35a6f258fc8ed931ec80323->leave($__internal_52c45fd1dfe1a73bccc7d675f42f1fc145cd196bc35a6f258fc8ed931ec80323_prof);

        
        $__internal_e2d82005b4a8ab74d110bec30ed9cd5804903b4a9c600131327f292b810d1013->leave($__internal_e2d82005b4a8ab74d110bec30ed9cd5804903b4a9c600131327f292b810d1013_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_acl.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 25,  109 => 24,  96 => 22,  87 => 21,  76 => 24,  73 => 21,  64 => 20,  54 => 15,  45 => 14,  35 => 12,  33 => 18,  21 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:action_buttons.html.twig' %}
{%- endblock -%}

{% import 'SonataAdminBundle:CRUD:base_acl_macro.html.twig' as acl %}

{% block form %}
    {% block form_acl_roles %}
        {{ acl.render_form(aclRolesForm, permissions, 'td_role', admin, sonata_admin.adminPool, object) }}
    {% endblock %}
    {% block form_acl_users %}
        {{ acl.render_form(aclUsersForm, permissions, 'td_username', admin, sonata_admin.adminPool, object) }}
    {% endblock %}
{% endblock %}
", "SonataAdminBundle:CRUD:base_acl.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/base_acl.html.twig");
    }
}
