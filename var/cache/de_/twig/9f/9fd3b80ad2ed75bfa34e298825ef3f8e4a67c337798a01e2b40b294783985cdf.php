<?php

/* SonataAdminBundle::empty_layout.html.twig */
class __TwigTemplate_c15039b71f5cc5714763cf559b2db412be8036392ccaf82a188322e4d17f27f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'sonata_header' => array($this, 'block_sonata_header'),
            'sonata_left_side' => array($this, 'block_sonata_left_side'),
            'sonata_nav' => array($this, 'block_sonata_nav'),
            'sonata_breadcrumb' => array($this, 'block_sonata_breadcrumb'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'sonata_wrapper' => array($this, 'block_sonata_wrapper'),
            'sonata_page_content' => array($this, 'block_sonata_page_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : $this->getContext($context, "admin_pool")), "getTemplate", array(0 => "layout"), "method"), "SonataAdminBundle::empty_layout.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ca55c53f128a09908cc93880f6a1a6afe57a9c780db26912688102fca9382c2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ca55c53f128a09908cc93880f6a1a6afe57a9c780db26912688102fca9382c2->enter($__internal_6ca55c53f128a09908cc93880f6a1a6afe57a9c780db26912688102fca9382c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle::empty_layout.html.twig"));

        $__internal_020cb77e1b6bc1e8e6a149c9d17a24609c74c0918b75ed06be5cdaad1db3810a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_020cb77e1b6bc1e8e6a149c9d17a24609c74c0918b75ed06be5cdaad1db3810a->enter($__internal_020cb77e1b6bc1e8e6a149c9d17a24609c74c0918b75ed06be5cdaad1db3810a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle::empty_layout.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6ca55c53f128a09908cc93880f6a1a6afe57a9c780db26912688102fca9382c2->leave($__internal_6ca55c53f128a09908cc93880f6a1a6afe57a9c780db26912688102fca9382c2_prof);

        
        $__internal_020cb77e1b6bc1e8e6a149c9d17a24609c74c0918b75ed06be5cdaad1db3810a->leave($__internal_020cb77e1b6bc1e8e6a149c9d17a24609c74c0918b75ed06be5cdaad1db3810a_prof);

    }

    // line 14
    public function block_sonata_header($context, array $blocks = array())
    {
        $__internal_da0e5736ff65d6569a415eed4afa8187cf9af27715b9d05cf73381541bca2ab7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da0e5736ff65d6569a415eed4afa8187cf9af27715b9d05cf73381541bca2ab7->enter($__internal_da0e5736ff65d6569a415eed4afa8187cf9af27715b9d05cf73381541bca2ab7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_header"));

        $__internal_d2955473aaefdd806724713d4ca57f4d0e8897e6f59193ff1c91e757dcd7f6e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d2955473aaefdd806724713d4ca57f4d0e8897e6f59193ff1c91e757dcd7f6e7->enter($__internal_d2955473aaefdd806724713d4ca57f4d0e8897e6f59193ff1c91e757dcd7f6e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_header"));

        
        $__internal_d2955473aaefdd806724713d4ca57f4d0e8897e6f59193ff1c91e757dcd7f6e7->leave($__internal_d2955473aaefdd806724713d4ca57f4d0e8897e6f59193ff1c91e757dcd7f6e7_prof);

        
        $__internal_da0e5736ff65d6569a415eed4afa8187cf9af27715b9d05cf73381541bca2ab7->leave($__internal_da0e5736ff65d6569a415eed4afa8187cf9af27715b9d05cf73381541bca2ab7_prof);

    }

    // line 15
    public function block_sonata_left_side($context, array $blocks = array())
    {
        $__internal_2c622efcca15590772ee41cb0b2edc91fae28e1c3ee94a97dca486f4791d1b2b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2c622efcca15590772ee41cb0b2edc91fae28e1c3ee94a97dca486f4791d1b2b->enter($__internal_2c622efcca15590772ee41cb0b2edc91fae28e1c3ee94a97dca486f4791d1b2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_left_side"));

        $__internal_a471fc2741fdcf960bfc28af6f5b399d3af332311396506d7456af0b74d50f61 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a471fc2741fdcf960bfc28af6f5b399d3af332311396506d7456af0b74d50f61->enter($__internal_a471fc2741fdcf960bfc28af6f5b399d3af332311396506d7456af0b74d50f61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_left_side"));

        
        $__internal_a471fc2741fdcf960bfc28af6f5b399d3af332311396506d7456af0b74d50f61->leave($__internal_a471fc2741fdcf960bfc28af6f5b399d3af332311396506d7456af0b74d50f61_prof);

        
        $__internal_2c622efcca15590772ee41cb0b2edc91fae28e1c3ee94a97dca486f4791d1b2b->leave($__internal_2c622efcca15590772ee41cb0b2edc91fae28e1c3ee94a97dca486f4791d1b2b_prof);

    }

    // line 16
    public function block_sonata_nav($context, array $blocks = array())
    {
        $__internal_8f76b0365288d532d21ab4643e3247d8466273fe1e82bc5720a5d6a7c5f6ad0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f76b0365288d532d21ab4643e3247d8466273fe1e82bc5720a5d6a7c5f6ad0d->enter($__internal_8f76b0365288d532d21ab4643e3247d8466273fe1e82bc5720a5d6a7c5f6ad0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_nav"));

        $__internal_aedf07765a793f8438d1c4a8efa1218794ccab192792e7b9b28d005eb2ff44be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aedf07765a793f8438d1c4a8efa1218794ccab192792e7b9b28d005eb2ff44be->enter($__internal_aedf07765a793f8438d1c4a8efa1218794ccab192792e7b9b28d005eb2ff44be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_nav"));

        
        $__internal_aedf07765a793f8438d1c4a8efa1218794ccab192792e7b9b28d005eb2ff44be->leave($__internal_aedf07765a793f8438d1c4a8efa1218794ccab192792e7b9b28d005eb2ff44be_prof);

        
        $__internal_8f76b0365288d532d21ab4643e3247d8466273fe1e82bc5720a5d6a7c5f6ad0d->leave($__internal_8f76b0365288d532d21ab4643e3247d8466273fe1e82bc5720a5d6a7c5f6ad0d_prof);

    }

    // line 17
    public function block_sonata_breadcrumb($context, array $blocks = array())
    {
        $__internal_f7339f87edd72ac890d8868a4e2d9aa50f8e9ec262b432eb0b3092d1963b3f83 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f7339f87edd72ac890d8868a4e2d9aa50f8e9ec262b432eb0b3092d1963b3f83->enter($__internal_f7339f87edd72ac890d8868a4e2d9aa50f8e9ec262b432eb0b3092d1963b3f83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_breadcrumb"));

        $__internal_4d20e620153fd6f2002f4b12b6c1c7464131d40486a3442ba221772645789f10 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d20e620153fd6f2002f4b12b6c1c7464131d40486a3442ba221772645789f10->enter($__internal_4d20e620153fd6f2002f4b12b6c1c7464131d40486a3442ba221772645789f10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_breadcrumb"));

        
        $__internal_4d20e620153fd6f2002f4b12b6c1c7464131d40486a3442ba221772645789f10->leave($__internal_4d20e620153fd6f2002f4b12b6c1c7464131d40486a3442ba221772645789f10_prof);

        
        $__internal_f7339f87edd72ac890d8868a4e2d9aa50f8e9ec262b432eb0b3092d1963b3f83->leave($__internal_f7339f87edd72ac890d8868a4e2d9aa50f8e9ec262b432eb0b3092d1963b3f83_prof);

    }

    // line 19
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_3da9d54b0652e12bbf2b426e45a2d4b1ea0c31547e4cca32d87f5eaf7e067aa2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3da9d54b0652e12bbf2b426e45a2d4b1ea0c31547e4cca32d87f5eaf7e067aa2->enter($__internal_3da9d54b0652e12bbf2b426e45a2d4b1ea0c31547e4cca32d87f5eaf7e067aa2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_38ad6740eb6d773a90ab3c43e4d0ea91cf4c1695e32c1f41651c19bf18923951 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38ad6740eb6d773a90ab3c43e4d0ea91cf4c1695e32c1f41651c19bf18923951->enter($__internal_38ad6740eb6d773a90ab3c43e4d0ea91cf4c1695e32c1f41651c19bf18923951_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 20
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
        .content {
            margin: 0px;
            padding: 0px;
        }
    </style>
";
        
        $__internal_38ad6740eb6d773a90ab3c43e4d0ea91cf4c1695e32c1f41651c19bf18923951->leave($__internal_38ad6740eb6d773a90ab3c43e4d0ea91cf4c1695e32c1f41651c19bf18923951_prof);

        
        $__internal_3da9d54b0652e12bbf2b426e45a2d4b1ea0c31547e4cca32d87f5eaf7e067aa2->leave($__internal_3da9d54b0652e12bbf2b426e45a2d4b1ea0c31547e4cca32d87f5eaf7e067aa2_prof);

    }

    // line 30
    public function block_sonata_wrapper($context, array $blocks = array())
    {
        $__internal_844a50354709ba866968743b4f340d8dfc51023f8c85161839c21a81a61c50f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_844a50354709ba866968743b4f340d8dfc51023f8c85161839c21a81a61c50f6->enter($__internal_844a50354709ba866968743b4f340d8dfc51023f8c85161839c21a81a61c50f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_wrapper"));

        $__internal_58e6c95581aa53c0e2e4bf6d32eeb1de95ab6f928fff997b7a5b1a68fd9a317f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58e6c95581aa53c0e2e4bf6d32eeb1de95ab6f928fff997b7a5b1a68fd9a317f->enter($__internal_58e6c95581aa53c0e2e4bf6d32eeb1de95ab6f928fff997b7a5b1a68fd9a317f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_wrapper"));

        // line 31
        echo "    ";
        $this->displayBlock('sonata_page_content', $context, $blocks);
        
        $__internal_58e6c95581aa53c0e2e4bf6d32eeb1de95ab6f928fff997b7a5b1a68fd9a317f->leave($__internal_58e6c95581aa53c0e2e4bf6d32eeb1de95ab6f928fff997b7a5b1a68fd9a317f_prof);

        
        $__internal_844a50354709ba866968743b4f340d8dfc51023f8c85161839c21a81a61c50f6->leave($__internal_844a50354709ba866968743b4f340d8dfc51023f8c85161839c21a81a61c50f6_prof);

    }

    public function block_sonata_page_content($context, array $blocks = array())
    {
        $__internal_19a00c5f3578740cd0850bf2b0d13c71aebb060ec6cf8b4fd089e403f1fb1d4f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19a00c5f3578740cd0850bf2b0d13c71aebb060ec6cf8b4fd089e403f1fb1d4f->enter($__internal_19a00c5f3578740cd0850bf2b0d13c71aebb060ec6cf8b4fd089e403f1fb1d4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_page_content"));

        $__internal_4a43329dcc0493a2fc2bb13cc188a8155222ad6bdef99dae7328b838ed24b51d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a43329dcc0493a2fc2bb13cc188a8155222ad6bdef99dae7328b838ed24b51d->enter($__internal_4a43329dcc0493a2fc2bb13cc188a8155222ad6bdef99dae7328b838ed24b51d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_page_content"));

        // line 32
        echo "        ";
        $this->displayParentBlock("sonata_page_content", $context, $blocks);
        echo "
    ";
        
        $__internal_4a43329dcc0493a2fc2bb13cc188a8155222ad6bdef99dae7328b838ed24b51d->leave($__internal_4a43329dcc0493a2fc2bb13cc188a8155222ad6bdef99dae7328b838ed24b51d_prof);

        
        $__internal_19a00c5f3578740cd0850bf2b0d13c71aebb060ec6cf8b4fd089e403f1fb1d4f->leave($__internal_19a00c5f3578740cd0850bf2b0d13c71aebb060ec6cf8b4fd089e403f1fb1d4f_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle::empty_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 32,  151 => 31,  142 => 30,  122 => 20,  113 => 19,  96 => 17,  79 => 16,  62 => 15,  45 => 14,  24 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin_pool.getTemplate('layout') %}

{% block sonata_header %}{% endblock %}
{% block sonata_left_side %}{% endblock %}
{% block sonata_nav %}{% endblock %}
{% block sonata_breadcrumb %}{% endblock %}

{% block stylesheets %}
    {{ parent() }}

    <style>
        .content {
            margin: 0px;
            padding: 0px;
        }
    </style>
{% endblock %}

{% block sonata_wrapper %}
    {% block sonata_page_content %}
        {{ parent() }}
    {% endblock %}
{% endblock %}
", "SonataAdminBundle::empty_layout.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/empty_layout.html.twig");
    }
}
