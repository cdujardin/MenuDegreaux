<?php

/* SonataAdminBundle:CRUD:list_trans.html.twig */
class __TwigTemplate_ed043f134594400ee24562dd0d438fe8621e6687ab86993ca6e15b347fae7c3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_trans.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_80720384528d2dc5ffc1e1621e301502adcc0a117c45ca6e3c01aa1ffaa8c544 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80720384528d2dc5ffc1e1621e301502adcc0a117c45ca6e3c01aa1ffaa8c544->enter($__internal_80720384528d2dc5ffc1e1621e301502adcc0a117c45ca6e3c01aa1ffaa8c544_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_trans.html.twig"));

        $__internal_14b764fe33c4f4127de9d06cc9a159aec0af2e6cd4d93743465dd0560c39ce63 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_14b764fe33c4f4127de9d06cc9a159aec0af2e6cd4d93743465dd0560c39ce63->enter($__internal_14b764fe33c4f4127de9d06cc9a159aec0af2e6cd4d93743465dd0560c39ce63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_trans.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_80720384528d2dc5ffc1e1621e301502adcc0a117c45ca6e3c01aa1ffaa8c544->leave($__internal_80720384528d2dc5ffc1e1621e301502adcc0a117c45ca6e3c01aa1ffaa8c544_prof);

        
        $__internal_14b764fe33c4f4127de9d06cc9a159aec0af2e6cd4d93743465dd0560c39ce63->leave($__internal_14b764fe33c4f4127de9d06cc9a159aec0af2e6cd4d93743465dd0560c39ce63_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_766ae81d3ceeeaff73b4fc89a64c9d70bd485bf378344f02565d5e545dce6a26 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_766ae81d3ceeeaff73b4fc89a64c9d70bd485bf378344f02565d5e545dce6a26->enter($__internal_766ae81d3ceeeaff73b4fc89a64c9d70bd485bf378344f02565d5e545dce6a26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_277c4c22b37fa6087c837ef981e55cf3852fd729d2db0b4f682ff9e06aa8deb4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_277c4c22b37fa6087c837ef981e55cf3852fd729d2db0b4f682ff9e06aa8deb4->enter($__internal_277c4c22b37fa6087c837ef981e55cf3852fd729d2db0b4f682ff9e06aa8deb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["translationDomain"] = (($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "catalogue", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "catalogue", array()), $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "translationDomain", array()))) : ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "translationDomain", array())));
        // line 16
        echo "    ";
        $context["valueFormat"] = (($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "format", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "format", array()), "%s")) : ("%s"));
        // line 17
        echo "
    ";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(sprintf((isset($context["valueFormat"]) ? $context["valueFormat"] : $this->getContext($context, "valueFormat")), (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))), array(), (isset($context["translationDomain"]) ? $context["translationDomain"] : $this->getContext($context, "translationDomain"))), "html", null, true);
        echo "
";
        
        $__internal_277c4c22b37fa6087c837ef981e55cf3852fd729d2db0b4f682ff9e06aa8deb4->leave($__internal_277c4c22b37fa6087c837ef981e55cf3852fd729d2db0b4f682ff9e06aa8deb4_prof);

        
        $__internal_766ae81d3ceeeaff73b4fc89a64c9d70bd485bf378344f02565d5e545dce6a26->leave($__internal_766ae81d3ceeeaff73b4fc89a64c9d70bd485bf378344f02565d5e545dce6a26_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_trans.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 18,  54 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field%}
    {% set translationDomain = field_description.options.catalogue|default(admin.translationDomain) %}
    {% set valueFormat = field_description.options.format|default('%s') %}

    {{valueFormat|format(value)|trans({}, translationDomain)}}
{% endblock %}
", "SonataAdminBundle:CRUD:list_trans.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list_trans.html.twig");
    }
}
