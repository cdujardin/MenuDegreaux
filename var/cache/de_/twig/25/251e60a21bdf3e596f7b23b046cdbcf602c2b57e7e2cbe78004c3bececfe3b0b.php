<?php

/* SonataAdminBundle:CRUD:show_trans.html.twig */
class __TwigTemplate_2d5d598ee721ecf028b7cbdd79054e3d1a9d658893c6162766156d3cc27df608 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 11
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_trans.html.twig", 11);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f89f1d5eb277929dbd3e6ad67b3af896528c670dce96b253af73472ad14139d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f89f1d5eb277929dbd3e6ad67b3af896528c670dce96b253af73472ad14139d8->enter($__internal_f89f1d5eb277929dbd3e6ad67b3af896528c670dce96b253af73472ad14139d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_trans.html.twig"));

        $__internal_36bd64f97fb979ace3766a6929a6e2d2901fc98ae16f5a9795a1975bb0b75333 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36bd64f97fb979ace3766a6929a6e2d2901fc98ae16f5a9795a1975bb0b75333->enter($__internal_36bd64f97fb979ace3766a6929a6e2d2901fc98ae16f5a9795a1975bb0b75333_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_trans.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f89f1d5eb277929dbd3e6ad67b3af896528c670dce96b253af73472ad14139d8->leave($__internal_f89f1d5eb277929dbd3e6ad67b3af896528c670dce96b253af73472ad14139d8_prof);

        
        $__internal_36bd64f97fb979ace3766a6929a6e2d2901fc98ae16f5a9795a1975bb0b75333->leave($__internal_36bd64f97fb979ace3766a6929a6e2d2901fc98ae16f5a9795a1975bb0b75333_prof);

    }

    // line 13
    public function block_field($context, array $blocks = array())
    {
        $__internal_103d50c7490c68acd1aa6b56ce4989138f35fb1b8aa428f47cb69d8f683a3e4d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_103d50c7490c68acd1aa6b56ce4989138f35fb1b8aa428f47cb69d8f683a3e4d->enter($__internal_103d50c7490c68acd1aa6b56ce4989138f35fb1b8aa428f47cb69d8f683a3e4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_cb203003152885b8e0b0bf54d5d9c68eb798e17a5fff3a5c6a511e65d1064aaa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb203003152885b8e0b0bf54d5d9c68eb798e17a5fff3a5c6a511e65d1064aaa->enter($__internal_cb203003152885b8e0b0bf54d5d9c68eb798e17a5fff3a5c6a511e65d1064aaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 14
        echo "    ";
        if ( !$this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "catalogue", array(), "any", true, true)) {
            // line 15
            echo "        ";
            $context["value"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")));
            // line 16
            echo "    ";
        } else {
            // line 17
            echo "        ";
            $context["value"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), array(), $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "catalogue", array()));
            // line 18
            echo "    ";
        }
        // line 19
        echo "
    ";
        // line 20
        if ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "safe", array())) {
            // line 21
            echo "        ";
            echo (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"));
            echo "
    ";
        } else {
            // line 23
            echo "        ";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "
    ";
        }
        
        $__internal_cb203003152885b8e0b0bf54d5d9c68eb798e17a5fff3a5c6a511e65d1064aaa->leave($__internal_cb203003152885b8e0b0bf54d5d9c68eb798e17a5fff3a5c6a511e65d1064aaa_prof);

        
        $__internal_103d50c7490c68acd1aa6b56ce4989138f35fb1b8aa428f47cb69d8f683a3e4d->leave($__internal_103d50c7490c68acd1aa6b56ce4989138f35fb1b8aa428f47cb69d8f683a3e4d_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_trans.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 23,  69 => 21,  67 => 20,  64 => 19,  61 => 18,  58 => 17,  55 => 16,  52 => 15,  49 => 14,  40 => 13,  11 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field%}
    {% if field_description.options.catalogue is not defined %}
        {% set value = value|trans %}
    {% else %}
        {% set value = value|trans({}, field_description.options.catalogue) %}
    {% endif %}

    {% if field_description.options.safe %}
        {{ value|raw }}
    {% else %}
        {{ value }}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:show_trans.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/show_trans.html.twig");
    }
}
