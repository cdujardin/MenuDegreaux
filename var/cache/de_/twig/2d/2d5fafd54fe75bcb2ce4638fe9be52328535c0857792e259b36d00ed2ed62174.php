<?php

/* @SonataCore/Form/colorpicker.html.twig */
class __TwigTemplate_7e57b15fe0be84d3f6c102e5d755946434c4dfe053e5071cbd9300798c0e3b97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sonata_type_color_selector_widget' => array($this, 'block_sonata_type_color_selector_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d419755af19d7181369e14b226221bca7e45d7df652561ddd98d73156994b55d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d419755af19d7181369e14b226221bca7e45d7df652561ddd98d73156994b55d->enter($__internal_d419755af19d7181369e14b226221bca7e45d7df652561ddd98d73156994b55d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataCore/Form/colorpicker.html.twig"));

        $__internal_376cc71c59f657de1c2704b25097ff25e51e93eba5d690f63f6d7a389e94fda8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_376cc71c59f657de1c2704b25097ff25e51e93eba5d690f63f6d7a389e94fda8->enter($__internal_376cc71c59f657de1c2704b25097ff25e51e93eba5d690f63f6d7a389e94fda8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataCore/Form/colorpicker.html.twig"));

        // line 11
        $this->displayBlock('sonata_type_color_selector_widget', $context, $blocks);
        
        $__internal_d419755af19d7181369e14b226221bca7e45d7df652561ddd98d73156994b55d->leave($__internal_d419755af19d7181369e14b226221bca7e45d7df652561ddd98d73156994b55d_prof);

        
        $__internal_376cc71c59f657de1c2704b25097ff25e51e93eba5d690f63f6d7a389e94fda8->leave($__internal_376cc71c59f657de1c2704b25097ff25e51e93eba5d690f63f6d7a389e94fda8_prof);

    }

    public function block_sonata_type_color_selector_widget($context, array $blocks = array())
    {
        $__internal_eea9ca283eff5bc0b0c80780adc82b3556a8346f569f7f14acbc37f778c94767 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eea9ca283eff5bc0b0c80780adc82b3556a8346f569f7f14acbc37f778c94767->enter($__internal_eea9ca283eff5bc0b0c80780adc82b3556a8346f569f7f14acbc37f778c94767_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_color_selector_widget"));

        $__internal_fea7df4162b1311c5004ef564a260792bf86c91d91d69c6115eb57fe60eb1a68 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fea7df4162b1311c5004ef564a260792bf86c91d91d69c6115eb57fe60eb1a68->enter($__internal_fea7df4162b1311c5004ef564a260792bf86c91d91d69c6115eb57fe60eb1a68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_color_selector_widget"));

        // line 12
        echo "    ";
        $this->displayBlock("choice_widget", $context, $blocks);
        echo "
    ";
        // line 13
        ob_start();
        // line 14
        echo "        <script type=\"text/javascript\">
            jQuery(function (\$) {
                var select2FormatColorSelect = function format(state) {
                    if (!state.id || state.disabled) {
                        return state.text;
                    }

                    return ' <i class=\"fa fa-square\" style=\"color: '+ state.id +'\"></i> ' + state.text;
                };

                \$('#";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "').select2({
                    formatResult:    select2FormatColorSelect,
                    formatSelection: select2FormatColorSelect,
                    width:           '100%',
                    escapeMarkup:    function(m) { return m; }
                });
            });
        </script>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_fea7df4162b1311c5004ef564a260792bf86c91d91d69c6115eb57fe60eb1a68->leave($__internal_fea7df4162b1311c5004ef564a260792bf86c91d91d69c6115eb57fe60eb1a68_prof);

        
        $__internal_eea9ca283eff5bc0b0c80780adc82b3556a8346f569f7f14acbc37f778c94767->leave($__internal_eea9ca283eff5bc0b0c80780adc82b3556a8346f569f7f14acbc37f778c94767_prof);

    }

    public function getTemplateName()
    {
        return "@SonataCore/Form/colorpicker.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  63 => 24,  51 => 14,  49 => 13,  44 => 12,  26 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% block sonata_type_color_selector_widget %}
    {{ block('choice_widget') }}
    {% spaceless %}
        <script type=\"text/javascript\">
            jQuery(function (\$) {
                var select2FormatColorSelect = function format(state) {
                    if (!state.id || state.disabled) {
                        return state.text;
                    }

                    return ' <i class=\"fa fa-square\" style=\"color: '+ state.id +'\"></i> ' + state.text;
                };

                \$('#{{ id }}').select2({
                    formatResult:    select2FormatColorSelect,
                    formatSelection: select2FormatColorSelect,
                    width:           '100%',
                    escapeMarkup:    function(m) { return m; }
                });
            });
        </script>
    {% endspaceless %}
{% endblock sonata_type_color_selector_widget %}
", "@SonataCore/Form/colorpicker.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\core-bundle\\Resources\\views\\Form\\colorpicker.html.twig");
    }
}
