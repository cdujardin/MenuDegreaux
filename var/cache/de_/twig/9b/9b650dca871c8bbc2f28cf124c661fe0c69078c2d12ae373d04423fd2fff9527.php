<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_d2a3a2fa9918a32213f24ad9f3973458fd7906e54250f3640dd02f9009c25d68 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_47d080f82cdbb0293aa494fad6e48da4a7d11d1a70645d64ab4f059369e0a760 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_47d080f82cdbb0293aa494fad6e48da4a7d11d1a70645d64ab4f059369e0a760->enter($__internal_47d080f82cdbb0293aa494fad6e48da4a7d11d1a70645d64ab4f059369e0a760_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_190a216dfecd16517a16c9e37bc73f10cbb85310df00a623de33f6b151dcc008 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_190a216dfecd16517a16c9e37bc73f10cbb85310df00a623de33f6b151dcc008->enter($__internal_190a216dfecd16517a16c9e37bc73f10cbb85310df00a623de33f6b151dcc008_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_47d080f82cdbb0293aa494fad6e48da4a7d11d1a70645d64ab4f059369e0a760->leave($__internal_47d080f82cdbb0293aa494fad6e48da4a7d11d1a70645d64ab4f059369e0a760_prof);

        
        $__internal_190a216dfecd16517a16c9e37bc73f10cbb85310df00a623de33f6b151dcc008->leave($__internal_190a216dfecd16517a16c9e37bc73f10cbb85310df00a623de33f6b151dcc008_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_errors.html.php");
    }
}
