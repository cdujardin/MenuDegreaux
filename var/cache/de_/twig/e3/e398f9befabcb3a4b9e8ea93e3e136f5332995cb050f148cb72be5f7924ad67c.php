<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_2d787f56fc92fb5e25d4220a9966cff624da26731d3dbc468f321d8f6e6f8e7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d2fbcc0d27b45bee1525604fe1db682ffab8ed3abfad6af597820ad86a402ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d2fbcc0d27b45bee1525604fe1db682ffab8ed3abfad6af597820ad86a402ae->enter($__internal_3d2fbcc0d27b45bee1525604fe1db682ffab8ed3abfad6af597820ad86a402ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_eb69d66f0cff0c361481ea16ee18a63c9af7108b2128a124b3830d673c104910 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb69d66f0cff0c361481ea16ee18a63c9af7108b2128a124b3830d673c104910->enter($__internal_eb69d66f0cff0c361481ea16ee18a63c9af7108b2128a124b3830d673c104910_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_3d2fbcc0d27b45bee1525604fe1db682ffab8ed3abfad6af597820ad86a402ae->leave($__internal_3d2fbcc0d27b45bee1525604fe1db682ffab8ed3abfad6af597820ad86a402ae_prof);

        
        $__internal_eb69d66f0cff0c361481ea16ee18a63c9af7108b2128a124b3830d673c104910->leave($__internal_eb69d66f0cff0c361481ea16ee18a63c9af7108b2128a124b3830d673c104910_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.atom.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
