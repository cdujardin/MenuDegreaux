<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_e584ac73b1cb70c09dd45718db3f55d1f4d38c82fd78bbd71111da64f2a9328a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a1a6ecf164c17948fb1f917951d60efb9f6f73b83719980be2b33876e555fbd2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a1a6ecf164c17948fb1f917951d60efb9f6f73b83719980be2b33876e555fbd2->enter($__internal_a1a6ecf164c17948fb1f917951d60efb9f6f73b83719980be2b33876e555fbd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        $__internal_f90e546be20f6e386a139f8464c0693474c52cf51505a294610520614361c183 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f90e546be20f6e386a139f8464c0693474c52cf51505a294610520614361c183->enter($__internal_f90e546be20f6e386a139f8464c0693474c52cf51505a294610520614361c183_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_a1a6ecf164c17948fb1f917951d60efb9f6f73b83719980be2b33876e555fbd2->leave($__internal_a1a6ecf164c17948fb1f917951d60efb9f6f73b83719980be2b33876e555fbd2_prof);

        
        $__internal_f90e546be20f6e386a139f8464c0693474c52cf51505a294610520614361c183->leave($__internal_f90e546be20f6e386a139f8464c0693474c52cf51505a294610520614361c183_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.css.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.css.twig");
    }
}
