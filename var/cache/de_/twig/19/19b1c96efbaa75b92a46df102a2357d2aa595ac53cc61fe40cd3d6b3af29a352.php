<?php

/* SonataAdminBundle:CRUD:edit.html.twig */
class __TwigTemplate_39cafdfc3160565391fef82332700a69e58507878a2dda3119efb177cb6b7da9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_edit.html.twig", "SonataAdminBundle:CRUD:edit.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_edit.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6edb6c97b594590709e66810d0a794930a5ec555819da868c5ea0145e88438a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6edb6c97b594590709e66810d0a794930a5ec555819da868c5ea0145e88438a3->enter($__internal_6edb6c97b594590709e66810d0a794930a5ec555819da868c5ea0145e88438a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit.html.twig"));

        $__internal_c1fc359f041ce44713c61b88727be3a3bd8ef3de8e6d1b8f193c4f6a016ac731 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1fc359f041ce44713c61b88727be3a3bd8ef3de8e6d1b8f193c4f6a016ac731->enter($__internal_c1fc359f041ce44713c61b88727be3a3bd8ef3de8e6d1b8f193c4f6a016ac731_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6edb6c97b594590709e66810d0a794930a5ec555819da868c5ea0145e88438a3->leave($__internal_6edb6c97b594590709e66810d0a794930a5ec555819da868c5ea0145e88438a3_prof);

        
        $__internal_c1fc359f041ce44713c61b88727be3a3bd8ef3de8e6d1b8f193c4f6a016ac731->leave($__internal_c1fc359f041ce44713c61b88727be3a3bd8ef3de8e6d1b8f193c4f6a016ac731_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_edit.html.twig' %}
", "SonataAdminBundle:CRUD:edit.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/edit.html.twig");
    }
}
