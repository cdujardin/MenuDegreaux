<?php

/* ::base.html.twig */
class __TwigTemplate_914d1a8d68c5f0cdbe7472cf9fec0e88deef67f83eb15205749a2c429831efee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'header_navigation_links' => array($this, 'block_header_navigation_links'),
            'body' => array($this, 'block_body'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_12a447989c54fcce001f9f259462ce9607634f416f63a6eac39ef0341eb37e22 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_12a447989c54fcce001f9f259462ce9607634f416f63a6eac39ef0341eb37e22->enter($__internal_12a447989c54fcce001f9f259462ce9607634f416f63a6eac39ef0341eb37e22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_f520464d6a01dabf86f96033f526b797771d44b4f6d1987b66619b1431d8c6c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f520464d6a01dabf86f96033f526b797771d44b4f6d1987b66619b1431d8c6c2->enter($__internal_f520464d6a01dabf86f96033f526b797771d44b4f6d1987b66619b1431d8c6c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "locale", array()), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <!-- police -->
        <link href=\"https://fonts.googleapis.com/css?family=Dosis\" rel=\"stylesheet\">
        <!-- Favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.png"), "html", null, true);
        echo "\" >
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-flatly-3.3.7.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-awesome-4.6.3.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-lato.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/highlight-solarized-light.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-tagsinput.css"), "html", null, true);
        echo "\">
        <!-- style perso -->
        <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/css_sass/css/style.css"), "html", null, true);
        echo "\">
        ";
        // line 19
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 22
        echo "    </head>

    <body id=\"";
        // line 24
        $this->displayBlock('body_id', $context, $blocks);
        echo "\">

        ";
        // line 26
        $this->displayBlock('header', $context, $blocks);
        // line 143
        echo "
        <div class=\"container body-container\">
            ";
        // line 145
        $this->displayBlock('body', $context, $blocks);
        // line 159
        echo "        </div>

        ";
        // line 161
        $this->displayBlock('footer', $context, $blocks);
        // line 172
        echo "
        ";
        // line 173
        $this->displayBlock('javascripts', $context, $blocks);
        // line 182
        echo "

    </body>
</html>
";
        
        $__internal_12a447989c54fcce001f9f259462ce9607634f416f63a6eac39ef0341eb37e22->leave($__internal_12a447989c54fcce001f9f259462ce9607634f416f63a6eac39ef0341eb37e22_prof);

        
        $__internal_f520464d6a01dabf86f96033f526b797771d44b4f6d1987b66619b1431d8c6c2->leave($__internal_f520464d6a01dabf86f96033f526b797771d44b4f6d1987b66619b1431d8c6c2_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_9b6b10834baaac9ca244d53ff700a497f21ea7b16ca6de622f511f91e56c9271 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b6b10834baaac9ca244d53ff700a497f21ea7b16ca6de622f511f91e56c9271->enter($__internal_9b6b10834baaac9ca244d53ff700a497f21ea7b16ca6de622f511f91e56c9271_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_acbadb59783f2f80b94137a7458f0285e0520b2203f9c2ebee3930c9473873a6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_acbadb59783f2f80b94137a7458f0285e0520b2203f9c2ebee3930c9473873a6->enter($__internal_acbadb59783f2f80b94137a7458f0285e0520b2203f9c2ebee3930c9473873a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Menu Degreaux";
        
        $__internal_acbadb59783f2f80b94137a7458f0285e0520b2203f9c2ebee3930c9473873a6->leave($__internal_acbadb59783f2f80b94137a7458f0285e0520b2203f9c2ebee3930c9473873a6_prof);

        
        $__internal_9b6b10834baaac9ca244d53ff700a497f21ea7b16ca6de622f511f91e56c9271->leave($__internal_9b6b10834baaac9ca244d53ff700a497f21ea7b16ca6de622f511f91e56c9271_prof);

    }

    // line 19
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6da5ac927a97a99d4742b024fc5659ea759524dc025f82a86507dfef278b18bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6da5ac927a97a99d4742b024fc5659ea759524dc025f82a86507dfef278b18bd->enter($__internal_6da5ac927a97a99d4742b024fc5659ea759524dc025f82a86507dfef278b18bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_59be9450ff4829933581f86d3f0d7661135b92b1d54a13e5f5e7d514cd673143 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59be9450ff4829933581f86d3f0d7661135b92b1d54a13e5f5e7d514cd673143->enter($__internal_59be9450ff4829933581f86d3f0d7661135b92b1d54a13e5f5e7d514cd673143_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 20
        echo "
        ";
        
        $__internal_59be9450ff4829933581f86d3f0d7661135b92b1d54a13e5f5e7d514cd673143->leave($__internal_59be9450ff4829933581f86d3f0d7661135b92b1d54a13e5f5e7d514cd673143_prof);

        
        $__internal_6da5ac927a97a99d4742b024fc5659ea759524dc025f82a86507dfef278b18bd->leave($__internal_6da5ac927a97a99d4742b024fc5659ea759524dc025f82a86507dfef278b18bd_prof);

    }

    // line 24
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_9389a66b8af2f7c408613d7532c9f07644b62fa87ce19f1efb2e9fb533f73cfb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9389a66b8af2f7c408613d7532c9f07644b62fa87ce19f1efb2e9fb533f73cfb->enter($__internal_9389a66b8af2f7c408613d7532c9f07644b62fa87ce19f1efb2e9fb533f73cfb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_15107e20b7c0a7966053fe333e2ab5848a8a70b9e8fe1ab64a86882b6f4e67ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15107e20b7c0a7966053fe333e2ab5848a8a70b9e8fe1ab64a86882b6f4e67ff->enter($__internal_15107e20b7c0a7966053fe333e2ab5848a8a70b9e8fe1ab64a86882b6f4e67ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_15107e20b7c0a7966053fe333e2ab5848a8a70b9e8fe1ab64a86882b6f4e67ff->leave($__internal_15107e20b7c0a7966053fe333e2ab5848a8a70b9e8fe1ab64a86882b6f4e67ff_prof);

        
        $__internal_9389a66b8af2f7c408613d7532c9f07644b62fa87ce19f1efb2e9fb533f73cfb->leave($__internal_9389a66b8af2f7c408613d7532c9f07644b62fa87ce19f1efb2e9fb533f73cfb_prof);

    }

    // line 26
    public function block_header($context, array $blocks = array())
    {
        $__internal_35f7416115ef8f2a943f3154f31f74834a0208be4e3b15182b95fb9bbb7a4a36 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35f7416115ef8f2a943f3154f31f74834a0208be4e3b15182b95fb9bbb7a4a36->enter($__internal_35f7416115ef8f2a943f3154f31f74834a0208be4e3b15182b95fb9bbb7a4a36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_c6b359f4401517a5672fc063fdc2025b86d84f58517320fb9316ff990060cf11 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c6b359f4401517a5672fc063fdc2025b86d84f58517320fb9316ff990060cf11->enter($__internal_c6b359f4401517a5672fc063fdc2025b86d84f58517320fb9316ff990060cf11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 27
        echo "            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                Menu Degreaux
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Menu</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">
                                ";
        // line 45
        $this->displayBlock('header_navigation_links', $context, $blocks);
        // line 132
        echo "




                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        ";
        
        $__internal_c6b359f4401517a5672fc063fdc2025b86d84f58517320fb9316ff990060cf11->leave($__internal_c6b359f4401517a5672fc063fdc2025b86d84f58517320fb9316ff990060cf11_prof);

        
        $__internal_35f7416115ef8f2a943f3154f31f74834a0208be4e3b15182b95fb9bbb7a4a36->leave($__internal_35f7416115ef8f2a943f3154f31f74834a0208be4e3b15182b95fb9bbb7a4a36_prof);

    }

    // line 45
    public function block_header_navigation_links($context, array $blocks = array())
    {
        $__internal_647d057458d8309f63ad496d7a242a7ef9722d13e4c24af35e1a9e8dcd999be4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_647d057458d8309f63ad496d7a242a7ef9722d13e4c24af35e1a9e8dcd999be4->enter($__internal_647d057458d8309f63ad496d7a242a7ef9722d13e4c24af35e1a9e8dcd999be4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        $__internal_25af714da43258e145eaf32e4475a622e2784b84e252edd5f82edd2b3782615a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25af714da43258e145eaf32e4475a622e2784b84e252edd5f82edd2b3782615a->enter($__internal_25af714da43258e145eaf32e4475a622e2784b84e252edd5f82edd2b3782615a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        // line 46
        echo "
                                    ";
        // line 47
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 48
            echo "                                        <li class=\"dropdown\">
                                                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                      ";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo "
                                                       <span class=\"caret\"></span>
                                                   </a>
                                                  <ul class=\"dropdown-menu\">
                                                    <li>
                                                        <a href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/{_locale}/profile"), "html", null, true);
            echo "\">
                                                            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> Profil
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/{_locale}/resseting"), "html", null, true);
            echo "\">
                                                            <i class=\"fa fa-key\" aria-hidden=\"true\"></i> Changer le mot de passe
                                                        </a>
                                                    </li>
                                                    ";
            // line 64
            if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
                // line 65
                echo "                                                        <li>
                                                            <a href=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/{_locale}/register"), "html", null, true);
                echo "\">
                                                                <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i> Ajouter un utilisateur
                                                            </a>
                                                        </li>
                                                    ";
            }
            // line 71
            echo "                                                  </ul>
                                                </li>

                                    ";
        }
        // line 75
        echo "
                                    <li>
                                        <a href=\"";
        // line 77
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>
                                            Homepage
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"http://lievin.fr/\" target=\"_blank\">
                                            <i class=\"fa fa-reply \" aria-hidden=\"true\"></i>
                                            Ville de Liévin
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:window.print()\">
                                            <i class=\"fa fa-print \" aria-hidden=\"true\"></i>
                                             Imprimer
                                        </a>
                                    </li>




                                    ";
        // line 98
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 99
            echo "                                        <li>
                                            <a href=\"";
            // line 100
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("{_locale}adminSonata"), "html", null, true);
            echo "\">

                                                <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> Gestion des menus
                                            </a>
                                        </li>
                                    ";
        }
        // line 106
        echo "
                                    ";
        // line 107
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 108
            echo "                                        <li>
                                            <a href=\"";
            // line 109
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
                                                <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>
                                                ";
            // line 111
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a>
                                        </li>
                                        ";
        } else {
            // line 115
            echo "                                        <li>
                                            <a href=\"";
            // line 116
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">
                                                <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>
                                                ";
            // line 118
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a>
                                        </li>
                                    ";
        }
        // line 122
        echo "
                                    ";
        // line 123
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "all", array()));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 124
            echo "                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 125
                echo "                                            <div class=\"";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\">
                                                ";
                // line 126
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["message"], array(), "FOSUserBundle"), "html", null, true);
                echo "
                                            </div>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 129
            echo "                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "
                                ";
        
        $__internal_25af714da43258e145eaf32e4475a622e2784b84e252edd5f82edd2b3782615a->leave($__internal_25af714da43258e145eaf32e4475a622e2784b84e252edd5f82edd2b3782615a_prof);

        
        $__internal_647d057458d8309f63ad496d7a242a7ef9722d13e4c24af35e1a9e8dcd999be4->leave($__internal_647d057458d8309f63ad496d7a242a7ef9722d13e4c24af35e1a9e8dcd999be4_prof);

    }

    // line 145
    public function block_body($context, array $blocks = array())
    {
        $__internal_8d48a504025100d7cfac8bda0a5aa851f6ec6ca38dbc08441e8749f7d5c08610 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d48a504025100d7cfac8bda0a5aa851f6ec6ca38dbc08441e8749f7d5c08610->enter($__internal_8d48a504025100d7cfac8bda0a5aa851f6ec6ca38dbc08441e8749f7d5c08610_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_213566d09474e823527b775b89f5ab56e3464f5b010f0102f18d5117e0541c5b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_213566d09474e823527b775b89f5ab56e3464f5b010f0102f18d5117e0541c5b->enter($__internal_213566d09474e823527b775b89f5ab56e3464f5b010f0102f18d5117e0541c5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 146
        echo "                ";
        echo twig_include($this->env, $context, "default/_flash_messages.html.twig");
        echo "
                <div class=\"row\">
                    <div id=\"main\" class=\"col-md-9 col-xs-12\">
                        ";
        // line 149
        $this->displayBlock('main', $context, $blocks);
        // line 151
        echo "                    </div>

                    <div id=\"sidebar\" class=\"col-md-3 col-xs-12\">
                        ";
        // line 154
        $this->displayBlock('sidebar', $context, $blocks);
        // line 156
        echo "                    </div>
                </div>
            ";
        
        $__internal_213566d09474e823527b775b89f5ab56e3464f5b010f0102f18d5117e0541c5b->leave($__internal_213566d09474e823527b775b89f5ab56e3464f5b010f0102f18d5117e0541c5b_prof);

        
        $__internal_8d48a504025100d7cfac8bda0a5aa851f6ec6ca38dbc08441e8749f7d5c08610->leave($__internal_8d48a504025100d7cfac8bda0a5aa851f6ec6ca38dbc08441e8749f7d5c08610_prof);

    }

    // line 149
    public function block_main($context, array $blocks = array())
    {
        $__internal_a092beffa65cba3b847aea56b3afafbca57e340d0694eb01bb86f18f6db76b3a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a092beffa65cba3b847aea56b3afafbca57e340d0694eb01bb86f18f6db76b3a->enter($__internal_a092beffa65cba3b847aea56b3afafbca57e340d0694eb01bb86f18f6db76b3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_45c806b57fe58d46238ef3881275e8974e1c0a649b042aab6d2c06a073f0064c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_45c806b57fe58d46238ef3881275e8974e1c0a649b042aab6d2c06a073f0064c->enter($__internal_45c806b57fe58d46238ef3881275e8974e1c0a649b042aab6d2c06a073f0064c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 150
        echo "                        ";
        
        $__internal_45c806b57fe58d46238ef3881275e8974e1c0a649b042aab6d2c06a073f0064c->leave($__internal_45c806b57fe58d46238ef3881275e8974e1c0a649b042aab6d2c06a073f0064c_prof);

        
        $__internal_a092beffa65cba3b847aea56b3afafbca57e340d0694eb01bb86f18f6db76b3a->leave($__internal_a092beffa65cba3b847aea56b3afafbca57e340d0694eb01bb86f18f6db76b3a_prof);

    }

    // line 154
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_845a52680f55e0ee11e79869509b77e4a61dea195f24ca4954bc446772763fe3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_845a52680f55e0ee11e79869509b77e4a61dea195f24ca4954bc446772763fe3->enter($__internal_845a52680f55e0ee11e79869509b77e4a61dea195f24ca4954bc446772763fe3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_9f2bf01954b80b6351b5e839661e7440c47692b8975ea391fc0dd17f58f66b20 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f2bf01954b80b6351b5e839661e7440c47692b8975ea391fc0dd17f58f66b20->enter($__internal_9f2bf01954b80b6351b5e839661e7440c47692b8975ea391fc0dd17f58f66b20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 155
        echo "                        ";
        
        $__internal_9f2bf01954b80b6351b5e839661e7440c47692b8975ea391fc0dd17f58f66b20->leave($__internal_9f2bf01954b80b6351b5e839661e7440c47692b8975ea391fc0dd17f58f66b20_prof);

        
        $__internal_845a52680f55e0ee11e79869509b77e4a61dea195f24ca4954bc446772763fe3->leave($__internal_845a52680f55e0ee11e79869509b77e4a61dea195f24ca4954bc446772763fe3_prof);

    }

    // line 161
    public function block_footer($context, array $blocks = array())
    {
        $__internal_e0179705320a9cd2fc696399f23f4e06b04f1edd7459f902dc94ab8c460a0393 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0179705320a9cd2fc696399f23f4e06b04f1edd7459f902dc94ab8c460a0393->enter($__internal_e0179705320a9cd2fc696399f23f4e06b04f1edd7459f902dc94ab8c460a0393_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_e832b21bdce271179ffda93a2d2e1808a00fbd418d746442fd7be39dd4cc51b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e832b21bdce271179ffda93a2d2e1808a00fbd418d746442fd7be39dd4cc51b4->enter($__internal_e832b21bdce271179ffda93a2d2e1808a00fbd418d746442fd7be39dd4cc51b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 162
        echo "            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-12 col-xs-12\">
                            <p>&copy; ";
        // line 166
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " - Liévin</p>
                        </div>
                    </div>
                </div>
            </footer>
        ";
        
        $__internal_e832b21bdce271179ffda93a2d2e1808a00fbd418d746442fd7be39dd4cc51b4->leave($__internal_e832b21bdce271179ffda93a2d2e1808a00fbd418d746442fd7be39dd4cc51b4_prof);

        
        $__internal_e0179705320a9cd2fc696399f23f4e06b04f1edd7459f902dc94ab8c460a0393->leave($__internal_e0179705320a9cd2fc696399f23f4e06b04f1edd7459f902dc94ab8c460a0393_prof);

    }

    // line 173
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f0f32ff2590bf0e80a3d03c3a1a530260c32ca005bf48fd5773b71f85c88de0e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0f32ff2590bf0e80a3d03c3a1a530260c32ca005bf48fd5773b71f85c88de0e->enter($__internal_f0f32ff2590bf0e80a3d03c3a1a530260c32ca005bf48fd5773b71f85c88de0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_042c4f5c21e3af94eb50377891d5aea35fc3229a166fe19d82f11b54d7dd52ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_042c4f5c21e3af94eb50377891d5aea35fc3229a166fe19d82f11b54d7dd52ee->enter($__internal_042c4f5c21e3af94eb50377891d5aea35fc3229a166fe19d82f11b54d7dd52ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 174
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.4.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/moment.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-3.3.7.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/highlight.pack.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-tagsinput.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_042c4f5c21e3af94eb50377891d5aea35fc3229a166fe19d82f11b54d7dd52ee->leave($__internal_042c4f5c21e3af94eb50377891d5aea35fc3229a166fe19d82f11b54d7dd52ee_prof);

        
        $__internal_f0f32ff2590bf0e80a3d03c3a1a530260c32ca005bf48fd5773b71f85c88de0e->leave($__internal_f0f32ff2590bf0e80a3d03c3a1a530260c32ca005bf48fd5773b71f85c88de0e_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  558 => 180,  554 => 179,  550 => 178,  546 => 177,  542 => 176,  538 => 175,  533 => 174,  524 => 173,  508 => 166,  502 => 162,  493 => 161,  483 => 155,  474 => 154,  464 => 150,  455 => 149,  443 => 156,  441 => 154,  436 => 151,  434 => 149,  427 => 146,  418 => 145,  407 => 130,  401 => 129,  392 => 126,  387 => 125,  382 => 124,  378 => 123,  375 => 122,  368 => 118,  363 => 116,  360 => 115,  353 => 111,  348 => 109,  345 => 108,  343 => 107,  340 => 106,  331 => 100,  328 => 99,  326 => 98,  302 => 77,  298 => 75,  292 => 71,  284 => 66,  281 => 65,  279 => 64,  272 => 60,  264 => 55,  256 => 50,  252 => 48,  250 => 47,  247 => 46,  238 => 45,  218 => 132,  216 => 45,  199 => 31,  193 => 27,  184 => 26,  167 => 24,  156 => 20,  147 => 19,  129 => 6,  115 => 182,  113 => 173,  110 => 172,  108 => 161,  104 => 159,  102 => 145,  98 => 143,  96 => 26,  91 => 24,  87 => 22,  85 => 19,  81 => 18,  76 => 16,  72 => 15,  68 => 14,  64 => 13,  60 => 12,  56 => 11,  52 => 10,  45 => 6,  38 => 2,  35 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"{{ app.request.locale }}\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>{% block title %}Menu Degreaux{% endblock %}</title>
        <!-- police -->
        <link href=\"https://fonts.googleapis.com/css?family=Dosis\" rel=\"stylesheet\">
        <!-- Favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.png') }}\" >
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-flatly-3.3.7.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/font-awesome-4.6.3.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/font-lato.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-datetimepicker.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/highlight-solarized-light.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-tagsinput.css') }}\">
        <!-- style perso -->
        <link rel=\"stylesheet\" href=\"{{ asset('css/css_sass/css/style.css') }}\">
        {% block stylesheets %}

        {% endblock %}
    </head>

    <body id=\"{% block body_id %}{% endblock %}\">

        {% block header %}
            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"{{ path('homepage') }}\">
                                Menu Degreaux
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Menu</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">
                                {% block header_navigation_links %}

                                    {% if is_granted(\"ROLE_USER\") %}
                                        <li class=\"dropdown\">
                                                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                      {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }}
                                                       <span class=\"caret\"></span>
                                                   </a>
                                                  <ul class=\"dropdown-menu\">
                                                    <li>
                                                        <a href=\"{{ asset('/{_locale}/profile') }}\">
                                                            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> Profil
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"{{ asset('/{_locale}/resseting') }}\">
                                                            <i class=\"fa fa-key\" aria-hidden=\"true\"></i> Changer le mot de passe
                                                        </a>
                                                    </li>
                                                    {% if is_granted(\"ROLE_ADMIN\") %}
                                                        <li>
                                                            <a href=\"{{ asset('/{_locale}/register') }}\">
                                                                <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i> Ajouter un utilisateur
                                                            </a>
                                                        </li>
                                                    {% endif %}
                                                  </ul>
                                                </li>

                                    {% endif %}

                                    <li>
                                        <a href=\"{{ path('homepage') }}\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>
                                            Homepage
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"http://lievin.fr/\" target=\"_blank\">
                                            <i class=\"fa fa-reply \" aria-hidden=\"true\"></i>
                                            Ville de Liévin
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:window.print()\">
                                            <i class=\"fa fa-print \" aria-hidden=\"true\"></i>
                                             Imprimer
                                        </a>
                                    </li>




                                    {% if is_granted(\"ROLE_USER\") %}
                                        <li>
                                            <a href=\"{{ asset('{_locale}adminSonata') }}\">

                                                <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> Gestion des menus
                                            </a>
                                        </li>
                                    {% endif %}

                                    {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                                        <li>
                                            <a href=\"{{ path('fos_user_security_logout') }}\">
                                                <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>
                                                {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                                            </a>
                                        </li>
                                        {% else %}
                                        <li>
                                            <a href=\"{{ path('fos_user_security_login') }}\">
                                                <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>
                                                {{ 'layout.login'|trans({}, 'FOSUserBundle') }}
                                            </a>
                                        </li>
                                    {% endif %}

                                    {% for type, messages in app.session.flashBag.all %}
                                        {% for message in messages %}
                                            <div class=\"{{ type }}\">
                                                {{ message|trans({}, 'FOSUserBundle') }}
                                            </div>
                                        {% endfor %}
                                    {% endfor %}

                                {% endblock %}





                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        {% endblock %}

        <div class=\"container body-container\">
            {% block body %}
                {{ include('default/_flash_messages.html.twig') }}
                <div class=\"row\">
                    <div id=\"main\" class=\"col-md-9 col-xs-12\">
                        {% block main %}
                        {% endblock %}
                    </div>

                    <div id=\"sidebar\" class=\"col-md-3 col-xs-12\">
                        {% block sidebar %}
                        {% endblock %}
                    </div>
                </div>
            {% endblock %}
        </div>

        {% block footer %}
            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-12 col-xs-12\">
                            <p>&copy; {{ 'now'|date('Y') }} - Liévin</p>
                        </div>
                    </div>
                </div>
            </footer>
        {% endblock %}

        {% block javascripts %}
            <script src=\"{{ asset('js/jquery-2.2.4.min.js') }}\"></script>
            <script src=\"{{ asset('js/moment.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-3.3.7.min.js') }}\"></script>
            <script src=\"{{ asset('js/highlight.pack.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-datetimepicker.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-tagsinput.min.js') }}\"></script>
            <script src=\"{{ asset('js/main.js') }}\"></script>
        {% endblock %}


    </body>
</html>
", "::base.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources\\views/base.html.twig");
    }
}
