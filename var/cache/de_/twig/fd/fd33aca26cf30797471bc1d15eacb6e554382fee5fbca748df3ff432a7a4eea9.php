<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_78f288bf0473956209444909b7f93e91b22c25db1bb685c42ef22e5abaaeb03f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0590ef0356c364544355591a23b5bd0f09f33a705c3dd1a189cb70effe8f152e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0590ef0356c364544355591a23b5bd0f09f33a705c3dd1a189cb70effe8f152e->enter($__internal_0590ef0356c364544355591a23b5bd0f09f33a705c3dd1a189cb70effe8f152e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_ec0aee577888d952adf6ca8c3404b8db7b2a6a33dca0a211f7b6bc792fa34908 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ec0aee577888d952adf6ca8c3404b8db7b2a6a33dca0a211f7b6bc792fa34908->enter($__internal_ec0aee577888d952adf6ca8c3404b8db7b2a6a33dca0a211f7b6bc792fa34908_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form); ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form); ?>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_0590ef0356c364544355591a23b5bd0f09f33a705c3dd1a189cb70effe8f152e->leave($__internal_0590ef0356c364544355591a23b5bd0f09f33a705c3dd1a189cb70effe8f152e_prof);

        
        $__internal_ec0aee577888d952adf6ca8c3404b8db7b2a6a33dca0a211f7b6bc792fa34908->leave($__internal_ec0aee577888d952adf6ca8c3404b8db7b2a6a33dca0a211f7b6bc792fa34908_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form); ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form); ?>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\form_row.html.php");
    }
}
