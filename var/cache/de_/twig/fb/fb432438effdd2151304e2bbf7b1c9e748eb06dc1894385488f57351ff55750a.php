<?php

/* :menu:externe.html.twig */
class __TwigTemplate_7b940379bfcceb2105d121a504c95e6fc232217c3df64e1dfea32fe0cfd8fd65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":menu:externe.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_15c8e6f876eb969fa9f6d17feb02c5ff277ba83deafe0f43a23b9d57c21366e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15c8e6f876eb969fa9f6d17feb02c5ff277ba83deafe0f43a23b9d57c21366e2->enter($__internal_15c8e6f876eb969fa9f6d17feb02c5ff277ba83deafe0f43a23b9d57c21366e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":menu:externe.html.twig"));

        $__internal_8c53f3a4b8366453009f2bc40b1a469063349bbc2ce29dd180e6d9a5885b105b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c53f3a4b8366453009f2bc40b1a469063349bbc2ce29dd180e6d9a5885b105b->enter($__internal_8c53f3a4b8366453009f2bc40b1a469063349bbc2ce29dd180e6d9a5885b105b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":menu:externe.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_15c8e6f876eb969fa9f6d17feb02c5ff277ba83deafe0f43a23b9d57c21366e2->leave($__internal_15c8e6f876eb969fa9f6d17feb02c5ff277ba83deafe0f43a23b9d57c21366e2_prof);

        
        $__internal_8c53f3a4b8366453009f2bc40b1a469063349bbc2ce29dd180e6d9a5885b105b->leave($__internal_8c53f3a4b8366453009f2bc40b1a469063349bbc2ce29dd180e6d9a5885b105b_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_66edf5643a6fe1e5e8156351cb4175e06f1824d17eae50e5bda3b87996d6a7de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66edf5643a6fe1e5e8156351cb4175e06f1824d17eae50e5bda3b87996d6a7de->enter($__internal_66edf5643a6fe1e5e8156351cb4175e06f1824d17eae50e5bda3b87996d6a7de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_d4c524c3496fadf5805958a76bad4366293d046179b7e8986f30b2f8be5e327b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4c524c3496fadf5805958a76bad4366293d046179b7e8986f30b2f8be5e327b->enter($__internal_d4c524c3496fadf5805958a76bad4366293d046179b7e8986f30b2f8be5e327b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "menu_externe";
        
        $__internal_d4c524c3496fadf5805958a76bad4366293d046179b7e8986f30b2f8be5e327b->leave($__internal_d4c524c3496fadf5805958a76bad4366293d046179b7e8986f30b2f8be5e327b_prof);

        
        $__internal_66edf5643a6fe1e5e8156351cb4175e06f1824d17eae50e5bda3b87996d6a7de->leave($__internal_66edf5643a6fe1e5e8156351cb4175e06f1824d17eae50e5bda3b87996d6a7de_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_c907c8a0c11706e08cf7195b222821ba5b2b72fd0ebe4e5467aa56e6f5ef803a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c907c8a0c11706e08cf7195b222821ba5b2b72fd0ebe4e5467aa56e6f5ef803a->enter($__internal_c907c8a0c11706e08cf7195b222821ba5b2b72fd0ebe4e5467aa56e6f5ef803a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_37412bb074223ade70f3c44293982b0132906deab35284c099e58a7fbc58f40a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37412bb074223ade70f3c44293982b0132906deab35284c099e58a7fbc58f40a->enter($__internal_37412bb074223ade70f3c44293982b0132906deab35284c099e58a7fbc58f40a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "        <h1> Menu Externe</h1>
        <p class=\"subtitle\"> Menu du midi uniquement</p>


            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Lundi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mardi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mercredi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Jeudi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "jeudiEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "jeudiPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "jeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "jeudiDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Samedi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-md-offset-4 col-xs-12\">
                    <div class=\"titre\">Dimanche </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimancheEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimanchePlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimancheAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimancheDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

            </div>




";
        
        $__internal_37412bb074223ade70f3c44293982b0132906deab35284c099e58a7fbc58f40a->leave($__internal_37412bb074223ade70f3c44293982b0132906deab35284c099e58a7fbc58f40a_prof);

        
        $__internal_c907c8a0c11706e08cf7195b222821ba5b2b72fd0ebe4e5467aa56e6f5ef803a->leave($__internal_c907c8a0c11706e08cf7195b222821ba5b2b72fd0ebe4e5467aa56e6f5ef803a_prof);

    }

    // line 96
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_61fcc20e1446aa706788bdbc25a510286ef5928bfea4822b5a73559bc80e90b3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_61fcc20e1446aa706788bdbc25a510286ef5928bfea4822b5a73559bc80e90b3->enter($__internal_61fcc20e1446aa706788bdbc25a510286ef5928bfea4822b5a73559bc80e90b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_a3f0db9328411109df18aac720d7bf83e6cc70a0fd83c5e04db2f1edeaef9223 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3f0db9328411109df18aac720d7bf83e6cc70a0fd83c5e04db2f1edeaef9223->enter($__internal_a3f0db9328411109df18aac720d7bf83e6cc70a0fd83c5e04db2f1edeaef9223_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 97
        echo "    <p class=\"titre\">
        Voir le menu :
    </p>
    <div>
        <a href=\"";
        // line 101
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
            Externe
        </a>
    </div>
    <div>
        <a  href=\"";
        // line 106
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
            Scolaire
        </a>
    </div>
    <div>
        <a href=\"";
        // line 111
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
            Résident
        </a>
    </div>



";
        
        $__internal_a3f0db9328411109df18aac720d7bf83e6cc70a0fd83c5e04db2f1edeaef9223->leave($__internal_a3f0db9328411109df18aac720d7bf83e6cc70a0fd83c5e04db2f1edeaef9223_prof);

        
        $__internal_61fcc20e1446aa706788bdbc25a510286ef5928bfea4822b5a73559bc80e90b3->leave($__internal_61fcc20e1446aa706788bdbc25a510286ef5928bfea4822b5a73559bc80e90b3_prof);

    }

    public function getTemplateName()
    {
        return ":menu:externe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 111,  273 => 106,  265 => 101,  259 => 97,  250 => 96,  230 => 84,  226 => 83,  222 => 82,  218 => 81,  205 => 71,  201 => 70,  197 => 69,  193 => 68,  183 => 61,  179 => 60,  175 => 59,  171 => 58,  161 => 51,  157 => 50,  153 => 49,  149 => 48,  136 => 38,  132 => 37,  128 => 36,  124 => 35,  114 => 28,  110 => 27,  106 => 26,  102 => 25,  92 => 18,  88 => 17,  84 => 16,  80 => 15,  69 => 6,  60 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'menu_externe' %}

{% block main %}
        <h1> Menu Externe</h1>
        <p class=\"subtitle\"> Menu du midi uniquement</p>


            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Lundi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.lundiEntree }}</div>
                        <div class=\" text\">{{ menuMidi.lundiPlat }}</div>
                        <div class=\"text\">{{ menuMidi.lundiAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.lundiDessert }}</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mardi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.mardiEntree }}</div>
                        <div class=\" text\">{{ menuMidi.mardiPlat }}</div>
                        <div class=\"text\">{{ menuMidi.mardiAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.mardiDessert }}</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mercredi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.mercrediEntree }}</div>
                        <div class=\" text\">{{ menuMidi.mercrediPlat }}</div>
                        <div class=\"text\">{{ menuMidi.mercrediAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.mercrediDessert }}</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Jeudi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.jeudiEntree }}</div>
                        <div class=\" text\">{{ menuMidi.jeudiPlat }}</div>
                        <div class=\"text\">{{ menuMidi.jeudiAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.jeudiDessert }}</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuMidi.vendrediEntree }}</div>
                            <div class=\" text\">{{ menuMidi.vendrediPlat }}</div>
                            <div class=\"text\">{{ menuMidi.vendrediAccompagnement }}</div>
                            <div class=\" text\">{{ menuMidi.vendrediDessert }}</div>
                        </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Samedi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.samediEntree }}</div>
                        <div class=\" text\">{{ menuMidi.samediPlat }}</div>
                        <div class=\"text\">{{ menuMidi.samediAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.samediDessert }}</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-md-offset-4 col-xs-12\">
                    <div class=\"titre\">Dimanche </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.dimancheEntree }}</div>
                        <div class=\" text\">{{ menuMidi.dimanchePlat }}</div>
                        <div class=\"text\">{{ menuMidi.dimancheAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.dimancheDessert }}</div>
                    </div>
                </div>

            </div>




{% endblock %}


{% block sidebar %}
    <p class=\"titre\">
        Voir le menu :
    </p>
    <div>
        <a href=\"{{ path('externe') }}\" >
            Externe
        </a>
    </div>
    <div>
        <a  href=\"{{ path('scolaire') }}\">
            Scolaire
        </a>
    </div>
    <div>
        <a href=\"{{ path('resident') }}\" >
            Résident
        </a>
    </div>



{% endblock %}
", ":menu:externe.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources\\views/menu/externe.html.twig");
    }
}
