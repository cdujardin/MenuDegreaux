<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_d3041a5419c20d950013206858c82ea6d8ddf42e14d46f278e9e5bbac2ce28a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca1f4d2586a1d917c9efdfb620b00ece102b675499a585c8fa67c730a16a7ddf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ca1f4d2586a1d917c9efdfb620b00ece102b675499a585c8fa67c730a16a7ddf->enter($__internal_ca1f4d2586a1d917c9efdfb620b00ece102b675499a585c8fa67c730a16a7ddf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        $__internal_389923003b2921877da6f1929cdf969075c274ed7aa250b8b89ed0c277edef9c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_389923003b2921877da6f1929cdf969075c274ed7aa250b8b89ed0c277edef9c->enter($__internal_389923003b2921877da6f1929cdf969075c274ed7aa250b8b89ed0c277edef9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_ca1f4d2586a1d917c9efdfb620b00ece102b675499a585c8fa67c730a16a7ddf->leave($__internal_ca1f4d2586a1d917c9efdfb620b00ece102b675499a585c8fa67c730a16a7ddf_prof);

        
        $__internal_389923003b2921877da6f1929cdf969075c274ed7aa250b8b89ed0c277edef9c->leave($__internal_389923003b2921877da6f1929cdf969075c274ed7aa250b8b89ed0c277edef9c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"{{ _charset }}\" ?>

<error code=\"{{ status_code }}\" message=\"{{ status_text }}\" />
", "TwigBundle:Exception:error.xml.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.xml.twig");
    }
}
