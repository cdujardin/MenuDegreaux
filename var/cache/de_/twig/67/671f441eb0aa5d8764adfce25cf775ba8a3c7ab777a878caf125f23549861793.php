<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_6e1d1a97b76ec20fdcd07f96a1e290d6588de55e5e5bed1298b295bde0f5aabf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_af00ecc6bb197deb15e1f3c8f62e28636c735d4fa6023c8068d97f3375023114 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af00ecc6bb197deb15e1f3c8f62e28636c735d4fa6023c8068d97f3375023114->enter($__internal_af00ecc6bb197deb15e1f3c8f62e28636c735d4fa6023c8068d97f3375023114_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $__internal_c4f73ffd6b299f658ff1f2afb04e1813244c4e95c0863865e6fad7d9fb32b36c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4f73ffd6b299f658ff1f2afb04e1813244c4e95c0863865e6fad7d9fb32b36c->enter($__internal_c4f73ffd6b299f658ff1f2afb04e1813244c4e95c0863865e6fad7d9fb32b36c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_af00ecc6bb197deb15e1f3c8f62e28636c735d4fa6023c8068d97f3375023114->leave($__internal_af00ecc6bb197deb15e1f3c8f62e28636c735d4fa6023c8068d97f3375023114_prof);

        
        $__internal_c4f73ffd6b299f658ff1f2afb04e1813244c4e95c0863865e6fad7d9fb32b36c->leave($__internal_c4f73ffd6b299f658ff1f2afb04e1813244c4e95c0863865e6fad7d9fb32b36c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_0172d515fe769c6db2fad62dc81312e3aa4bd8deb04cc986e712a02d237c8edb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0172d515fe769c6db2fad62dc81312e3aa4bd8deb04cc986e712a02d237c8edb->enter($__internal_0172d515fe769c6db2fad62dc81312e3aa4bd8deb04cc986e712a02d237c8edb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_8e08aeea8df247950fc74f601b03ac5ee36a6ca14f4b4f107dfa89885872e262 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e08aeea8df247950fc74f601b03ac5ee36a6ca14f4b4f107dfa89885872e262->enter($__internal_8e08aeea8df247950fc74f601b03ac5ee36a6ca14f4b4f107dfa89885872e262_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_8e08aeea8df247950fc74f601b03ac5ee36a6ca14f4b4f107dfa89885872e262->leave($__internal_8e08aeea8df247950fc74f601b03ac5ee36a6ca14f4b4f107dfa89885872e262_prof);

        
        $__internal_0172d515fe769c6db2fad62dc81312e3aa4bd8deb04cc986e712a02d237c8edb->leave($__internal_0172d515fe769c6db2fad62dc81312e3aa4bd8deb04cc986e712a02d237c8edb_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/new.html.twig");
    }
}
