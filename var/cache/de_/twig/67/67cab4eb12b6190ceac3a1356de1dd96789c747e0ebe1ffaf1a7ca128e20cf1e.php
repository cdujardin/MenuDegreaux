<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_55f400435937dba65b74d3f4c289ef4f43c698096bf76f93ccc9f4a7a82b61ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3078ef0f5de17684d90854768a8e4bf859a07a30da4ee55ef911b9cc8f6266be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3078ef0f5de17684d90854768a8e4bf859a07a30da4ee55ef911b9cc8f6266be->enter($__internal_3078ef0f5de17684d90854768a8e4bf859a07a30da4ee55ef911b9cc8f6266be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $__internal_b7e20d93f03e70dd4b32159c09c79a3f649b73c8597480865790bad998098c2b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b7e20d93f03e70dd4b32159c09c79a3f649b73c8597480865790bad998098c2b->enter($__internal_b7e20d93f03e70dd4b32159c09c79a3f649b73c8597480865790bad998098c2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3078ef0f5de17684d90854768a8e4bf859a07a30da4ee55ef911b9cc8f6266be->leave($__internal_3078ef0f5de17684d90854768a8e4bf859a07a30da4ee55ef911b9cc8f6266be_prof);

        
        $__internal_b7e20d93f03e70dd4b32159c09c79a3f649b73c8597480865790bad998098c2b->leave($__internal_b7e20d93f03e70dd4b32159c09c79a3f649b73c8597480865790bad998098c2b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6c673f31bb0533858c5d6f7332c673f304ee1c81eaa06a33f2409059957da499 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6c673f31bb0533858c5d6f7332c673f304ee1c81eaa06a33f2409059957da499->enter($__internal_6c673f31bb0533858c5d6f7332c673f304ee1c81eaa06a33f2409059957da499_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_5629e4ea5e49b05210d919d3298e4dbc572f1c6f66fa0f7c22935059c1a49e7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5629e4ea5e49b05210d919d3298e4dbc572f1c6f66fa0f7c22935059c1a49e7d->enter($__internal_5629e4ea5e49b05210d919d3298e4dbc572f1c6f66fa0f7c22935059c1a49e7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_5629e4ea5e49b05210d919d3298e4dbc572f1c6f66fa0f7c22935059c1a49e7d->leave($__internal_5629e4ea5e49b05210d919d3298e4dbc572f1c6f66fa0f7c22935059c1a49e7d_prof);

        
        $__internal_6c673f31bb0533858c5d6f7332c673f304ee1c81eaa06a33f2409059957da499->leave($__internal_6c673f31bb0533858c5d6f7332c673f304ee1c81eaa06a33f2409059957da499_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:edit.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/edit.html.twig");
    }
}
