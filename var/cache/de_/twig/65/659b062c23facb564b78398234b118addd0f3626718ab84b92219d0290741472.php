<?php

/* SonataAdminBundle:CRUD:list_time.html.twig */
class __TwigTemplate_797c81bfa57e0cbf0055d6083161219fd12906e0b769be4ee11fd409d4a9ea27 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_time.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95af30578559e556363b440ba7cdbd81686275d7537c1322fe1fa8b0e29a2126 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95af30578559e556363b440ba7cdbd81686275d7537c1322fe1fa8b0e29a2126->enter($__internal_95af30578559e556363b440ba7cdbd81686275d7537c1322fe1fa8b0e29a2126_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_time.html.twig"));

        $__internal_a9d1c0ff463f83e8fc7308929954b137a73456c4188dfd7b02c67259fb49cfac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a9d1c0ff463f83e8fc7308929954b137a73456c4188dfd7b02c67259fb49cfac->enter($__internal_a9d1c0ff463f83e8fc7308929954b137a73456c4188dfd7b02c67259fb49cfac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_time.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_95af30578559e556363b440ba7cdbd81686275d7537c1322fe1fa8b0e29a2126->leave($__internal_95af30578559e556363b440ba7cdbd81686275d7537c1322fe1fa8b0e29a2126_prof);

        
        $__internal_a9d1c0ff463f83e8fc7308929954b137a73456c4188dfd7b02c67259fb49cfac->leave($__internal_a9d1c0ff463f83e8fc7308929954b137a73456c4188dfd7b02c67259fb49cfac_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_43f5e8e18e05ccd19a855d93ac3a708bc2e9ffb17b62acb5b5e72139344d96ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_43f5e8e18e05ccd19a855d93ac3a708bc2e9ffb17b62acb5b5e72139344d96ab->enter($__internal_43f5e8e18e05ccd19a855d93ac3a708bc2e9ffb17b62acb5b5e72139344d96ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_eddf059e1e06cfc2f5ad8f8413fb4f4ef50bb66d0547167085ba8d7fde64189b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eddf059e1e06cfc2f5ad8f8413fb4f4ef50bb66d0547167085ba8d7fde64189b->enter($__internal_eddf059e1e06cfc2f5ad8f8413fb4f4ef50bb66d0547167085ba8d7fde64189b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            // line 16
            echo "&nbsp;";
        } else {
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "H:i:s"), "html", null, true);
        }
        
        $__internal_eddf059e1e06cfc2f5ad8f8413fb4f4ef50bb66d0547167085ba8d7fde64189b->leave($__internal_eddf059e1e06cfc2f5ad8f8413fb4f4ef50bb66d0547167085ba8d7fde64189b_prof);

        
        $__internal_43f5e8e18e05ccd19a855d93ac3a708bc2e9ffb17b62acb5b5e72139344d96ab->leave($__internal_43f5e8e18e05ccd19a855d93ac3a708bc2e9ffb17b62acb5b5e72139344d96ab_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 18,  50 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {%- if value is empty -%}
        &nbsp;
    {%- else -%}
        {{ value|date('H:i:s') }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:list_time.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list_time.html.twig");
    }
}
