<?php

/* form/layout.html.twig */
class __TwigTemplate_0f0bff0c7deff48618aaf5dcb89db84a8994048f8d07f3372f78672400a67d88 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("bootstrap_3_layout.html.twig", "form/layout.html.twig", 1);
        $this->blocks = array(
            'form_errors' => array($this, 'block_form_errors'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "bootstrap_3_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_237f08dbc848cb243716cccaad0c5265a3b60822b9b9b820e77bf28b375a637e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_237f08dbc848cb243716cccaad0c5265a3b60822b9b9b820e77bf28b375a637e->enter($__internal_237f08dbc848cb243716cccaad0c5265a3b60822b9b9b820e77bf28b375a637e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/layout.html.twig"));

        $__internal_6c523193e688ce26529a843a80fcbdf62dd7daf6f5ca05823e3739912d6dfe93 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c523193e688ce26529a843a80fcbdf62dd7daf6f5ca05823e3739912d6dfe93->enter($__internal_6c523193e688ce26529a843a80fcbdf62dd7daf6f5ca05823e3739912d6dfe93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_237f08dbc848cb243716cccaad0c5265a3b60822b9b9b820e77bf28b375a637e->leave($__internal_237f08dbc848cb243716cccaad0c5265a3b60822b9b9b820e77bf28b375a637e_prof);

        
        $__internal_6c523193e688ce26529a843a80fcbdf62dd7daf6f5ca05823e3739912d6dfe93->leave($__internal_6c523193e688ce26529a843a80fcbdf62dd7daf6f5ca05823e3739912d6dfe93_prof);

    }

    // line 5
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_ded1e66932e114c2e3e2d4265047a9bd50ff948caa0e09be4ff6b035c45a2cca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ded1e66932e114c2e3e2d4265047a9bd50ff948caa0e09be4ff6b035c45a2cca->enter($__internal_ded1e66932e114c2e3e2d4265047a9bd50ff948caa0e09be4ff6b035c45a2cca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_4435767fe34b6e353e04dc339804079b301ad5cf67a7ef1e567893099007bb9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4435767fe34b6e353e04dc339804079b301ad5cf67a7ef1e567893099007bb9b->enter($__internal_4435767fe34b6e353e04dc339804079b301ad5cf67a7ef1e567893099007bb9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 6
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 7
            if ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array())) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 8
            echo "        <ul class=\"list-unstyled\">";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 11
                echo "            <li><span class=\"fa fa-exclamation-triangle\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "</ul>
        ";
            // line 14
            if ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array())) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_4435767fe34b6e353e04dc339804079b301ad5cf67a7ef1e567893099007bb9b->leave($__internal_4435767fe34b6e353e04dc339804079b301ad5cf67a7ef1e567893099007bb9b_prof);

        
        $__internal_ded1e66932e114c2e3e2d4265047a9bd50ff948caa0e09be4ff6b035c45a2cca->leave($__internal_ded1e66932e114c2e3e2d4265047a9bd50ff948caa0e09be4ff6b035c45a2cca_prof);

    }

    public function getTemplateName()
    {
        return "form/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 14,  71 => 13,  63 => 11,  59 => 9,  57 => 8,  51 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'bootstrap_3_layout.html.twig' %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
        {% if form.parent %}<span class=\"help-block\">{% else %}<div class=\"alert alert-danger\">{% endif %}
        <ul class=\"list-unstyled\">
        {%- for error in errors -%}
            {# use font-awesome icon library #}
            <li><span class=\"fa fa-exclamation-triangle\"></span> {{ error.message }}</li>
        {%- endfor -%}
        </ul>
        {% if form.parent %}</span>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "form/layout.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\form\\layout.html.twig");
    }
}
