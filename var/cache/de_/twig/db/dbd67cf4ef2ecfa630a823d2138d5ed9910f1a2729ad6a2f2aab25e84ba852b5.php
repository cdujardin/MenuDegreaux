<?php

/* SonataAdminBundle:CRUD:edit_text.html.twig */
class __TwigTemplate_360215d1150dfa201ab626dab1d018938a3c33e559cc1151fac39b40ce4edc42 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:edit_text.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3f37ae09b4967ba4a0afbebab05d10b2056961e2660db29660ae1e5a50b9c7d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f37ae09b4967ba4a0afbebab05d10b2056961e2660db29660ae1e5a50b9c7d2->enter($__internal_3f37ae09b4967ba4a0afbebab05d10b2056961e2660db29660ae1e5a50b9c7d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_text.html.twig"));

        $__internal_1bbb700c3ce21331938c15b0dd201747ce31b763413edd91dfb00fe7d53c4064 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1bbb700c3ce21331938c15b0dd201747ce31b763413edd91dfb00fe7d53c4064->enter($__internal_1bbb700c3ce21331938c15b0dd201747ce31b763413edd91dfb00fe7d53c4064_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_text.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3f37ae09b4967ba4a0afbebab05d10b2056961e2660db29660ae1e5a50b9c7d2->leave($__internal_3f37ae09b4967ba4a0afbebab05d10b2056961e2660db29660ae1e5a50b9c7d2_prof);

        
        $__internal_1bbb700c3ce21331938c15b0dd201747ce31b763413edd91dfb00fe7d53c4064->leave($__internal_1bbb700c3ce21331938c15b0dd201747ce31b763413edd91dfb00fe7d53c4064_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_5b1f4fcf41127a3d842369dc2e24cf062e1a1c3dfb926acc96a3577900bfce2f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b1f4fcf41127a3d842369dc2e24cf062e1a1c3dfb926acc96a3577900bfce2f->enter($__internal_5b1f4fcf41127a3d842369dc2e24cf062e1a1c3dfb926acc96a3577900bfce2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_4dc2f7690325e3e2b911d5eb4b49923bf58c6fecce5949064763ca50cfe14724 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4dc2f7690325e3e2b911d5eb4b49923bf58c6fecce5949064763ca50cfe14724->enter($__internal_4dc2f7690325e3e2b911d5eb4b49923bf58c6fecce5949064763ca50cfe14724_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'widget', array("attr" => array("class" => "title")));
        
        $__internal_4dc2f7690325e3e2b911d5eb4b49923bf58c6fecce5949064763ca50cfe14724->leave($__internal_4dc2f7690325e3e2b911d5eb4b49923bf58c6fecce5949064763ca50cfe14724_prof);

        
        $__internal_5b1f4fcf41127a3d842369dc2e24cf062e1a1c3dfb926acc96a3577900bfce2f->leave($__internal_5b1f4fcf41127a3d842369dc2e24cf062e1a1c3dfb926acc96a3577900bfce2f_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_text.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block field %}{{ form_widget(field_element, {'attr': {'class' : 'title'}}) }}{% endblock %}
", "SonataAdminBundle:CRUD:edit_text.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/edit_text.html.twig");
    }
}
