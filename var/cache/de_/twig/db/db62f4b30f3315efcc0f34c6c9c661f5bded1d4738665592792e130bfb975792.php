<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_853bc97d83b4bed21b2fbe69829e0fe8a5d34c67605393d6ed53c96988a68600 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2dbf025fdd55eee92649f1408c69e8f889728c8f196bc87fb5f213dcd81a2f00 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2dbf025fdd55eee92649f1408c69e8f889728c8f196bc87fb5f213dcd81a2f00->enter($__internal_2dbf025fdd55eee92649f1408c69e8f889728c8f196bc87fb5f213dcd81a2f00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_054e90283da1b6d0b4bd67c1bf3bb2da686218f5db6efa01e50851db63a62603 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_054e90283da1b6d0b4bd67c1bf3bb2da686218f5db6efa01e50851db63a62603->enter($__internal_054e90283da1b6d0b4bd67c1bf3bb2da686218f5db6efa01e50851db63a62603_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_2dbf025fdd55eee92649f1408c69e8f889728c8f196bc87fb5f213dcd81a2f00->leave($__internal_2dbf025fdd55eee92649f1408c69e8f889728c8f196bc87fb5f213dcd81a2f00_prof);

        
        $__internal_054e90283da1b6d0b4bd67c1bf3bb2da686218f5db6efa01e50851db63a62603->leave($__internal_054e90283da1b6d0b4bd67c1bf3bb2da686218f5db6efa01e50851db63a62603_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\radio_widget.html.php");
    }
}
