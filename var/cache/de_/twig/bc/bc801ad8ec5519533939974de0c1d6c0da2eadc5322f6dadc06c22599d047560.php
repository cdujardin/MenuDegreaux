<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_ca4cf30c1c69e65cd9ec7c228acbd7e46a812a00d994083d2898c680194a2365 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_84510f8a87ff5072892dfc9e7542d6a5afedb481f1d70f1898f78392bbdc2589 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_84510f8a87ff5072892dfc9e7542d6a5afedb481f1d70f1898f78392bbdc2589->enter($__internal_84510f8a87ff5072892dfc9e7542d6a5afedb481f1d70f1898f78392bbdc2589_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_668fde3537108821ee6df85fcb7ec5b411be4b2f35ee3a7ec00a66011b18a87f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_668fde3537108821ee6df85fcb7ec5b411be4b2f35ee3a7ec00a66011b18a87f->enter($__internal_668fde3537108821ee6df85fcb7ec5b411be4b2f35ee3a7ec00a66011b18a87f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_84510f8a87ff5072892dfc9e7542d6a5afedb481f1d70f1898f78392bbdc2589->leave($__internal_84510f8a87ff5072892dfc9e7542d6a5afedb481f1d70f1898f78392bbdc2589_prof);

        
        $__internal_668fde3537108821ee6df85fcb7ec5b411be4b2f35ee3a7ec00a66011b18a87f->leave($__internal_668fde3537108821ee6df85fcb7ec5b411be4b2f35ee3a7ec00a66011b18a87f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\textarea_widget.html.php");
    }
}
