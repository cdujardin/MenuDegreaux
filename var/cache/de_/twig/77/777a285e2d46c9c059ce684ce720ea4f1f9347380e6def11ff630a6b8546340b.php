<?php

/* SonataAdminBundle:CRUD:show_percent.html.twig */
class __TwigTemplate_b8747f520650b89553160d2542ba04e4a878f7e28496209b03ed887295cb572e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_percent.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2a9827304952222e747967052e295b2bab8daad4b0f0786f440217f9e0c41c68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a9827304952222e747967052e295b2bab8daad4b0f0786f440217f9e0c41c68->enter($__internal_2a9827304952222e747967052e295b2bab8daad4b0f0786f440217f9e0c41c68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_percent.html.twig"));

        $__internal_886a6b718c96c2ffaaac16d9bf978faa4bf71e106f616a42cda8bb9fb19f8b8d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_886a6b718c96c2ffaaac16d9bf978faa4bf71e106f616a42cda8bb9fb19f8b8d->enter($__internal_886a6b718c96c2ffaaac16d9bf978faa4bf71e106f616a42cda8bb9fb19f8b8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_percent.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2a9827304952222e747967052e295b2bab8daad4b0f0786f440217f9e0c41c68->leave($__internal_2a9827304952222e747967052e295b2bab8daad4b0f0786f440217f9e0c41c68_prof);

        
        $__internal_886a6b718c96c2ffaaac16d9bf978faa4bf71e106f616a42cda8bb9fb19f8b8d->leave($__internal_886a6b718c96c2ffaaac16d9bf978faa4bf71e106f616a42cda8bb9fb19f8b8d_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_99919154e8ffc11702dd7f8dc9f5204618d7e2000d054cb4a9863eafa7c22a9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_99919154e8ffc11702dd7f8dc9f5204618d7e2000d054cb4a9863eafa7c22a9d->enter($__internal_99919154e8ffc11702dd7f8dc9f5204618d7e2000d054cb4a9863eafa7c22a9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_5ebb64d982fd8779a1b935332cda29b228cbd2f515a3510936b152d3b9984288 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ebb64d982fd8779a1b935332cda29b228cbd2f515a3510936b152d3b9984288->enter($__internal_5ebb64d982fd8779a1b935332cda29b228cbd2f515a3510936b152d3b9984288_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["value"] = ((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")) * 100);
        // line 16
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
        echo " %
";
        
        $__internal_5ebb64d982fd8779a1b935332cda29b228cbd2f515a3510936b152d3b9984288->leave($__internal_5ebb64d982fd8779a1b935332cda29b228cbd2f515a3510936b152d3b9984288_prof);

        
        $__internal_99919154e8ffc11702dd7f8dc9f5204618d7e2000d054cb4a9863eafa7c22a9d->leave($__internal_99919154e8ffc11702dd7f8dc9f5204618d7e2000d054cb4a9863eafa7c22a9d_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_percent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field %}
    {% set value = value * 100 %}
    {{ value }} %
{% endblock %}
", "SonataAdminBundle:CRUD:show_percent.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/show_percent.html.twig");
    }
}
