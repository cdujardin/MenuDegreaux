<?php

/* SonataAdminBundle::ajax_layout.html.twig */
class __TwigTemplate_36bbd9437ace8a522e53ec7eeca6b7ba5e8a77dec8b98472aea348d613588799 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'preview' => array($this, 'block_preview'),
            'form' => array($this, 'block_form'),
            'list' => array($this, 'block_list'),
            'show' => array($this, 'block_show'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9249e76eb22012be5ba2d142cc9357de70a038e6ed98e59834bb3a93a27f028 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9249e76eb22012be5ba2d142cc9357de70a038e6ed98e59834bb3a93a27f028->enter($__internal_a9249e76eb22012be5ba2d142cc9357de70a038e6ed98e59834bb3a93a27f028_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle::ajax_layout.html.twig"));

        $__internal_c9d8fc0c48abbb1a2e337c1bee8646dca04728a92bd0ef62fa386c2076a8be7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9d8fc0c48abbb1a2e337c1bee8646dca04728a92bd0ef62fa386c2076a8be7d->enter($__internal_c9d8fc0c48abbb1a2e337c1bee8646dca04728a92bd0ef62fa386c2076a8be7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle::ajax_layout.html.twig"));

        // line 11
        echo "
";
        // line 12
        $this->displayBlock('content', $context, $blocks);
        
        $__internal_a9249e76eb22012be5ba2d142cc9357de70a038e6ed98e59834bb3a93a27f028->leave($__internal_a9249e76eb22012be5ba2d142cc9357de70a038e6ed98e59834bb3a93a27f028_prof);

        
        $__internal_c9d8fc0c48abbb1a2e337c1bee8646dca04728a92bd0ef62fa386c2076a8be7d->leave($__internal_c9d8fc0c48abbb1a2e337c1bee8646dca04728a92bd0ef62fa386c2076a8be7d_prof);

    }

    public function block_content($context, array $blocks = array())
    {
        $__internal_d1190e9a5fe077f959c054938080555512c33b99c76f073d69705628d7f3006a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1190e9a5fe077f959c054938080555512c33b99c76f073d69705628d7f3006a->enter($__internal_d1190e9a5fe077f959c054938080555512c33b99c76f073d69705628d7f3006a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_bfb62d255203fe6e815d3747bcd567308b29eb844e49cdf3b677c671092b8e72 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bfb62d255203fe6e815d3747bcd567308b29eb844e49cdf3b677c671092b8e72->enter($__internal_bfb62d255203fe6e815d3747bcd567308b29eb844e49cdf3b677c671092b8e72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 13
        echo "    ";
        $context["_list_table"] = ((        $this->hasBlock("list_table", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_table", $context, $blocks))) : (null));
        // line 14
        echo "    ";
        $context["_list_filters"] = ((        $this->hasBlock("list_filters", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_filters", $context, $blocks))) : (null));
        // line 15
        echo "    ";
        $context["_list_filters_actions"] = ((        $this->hasBlock("list_filters_actions", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_filters_actions", $context, $blocks))) : (null));
        // line 16
        echo "
    ";
        // line 17
        $this->displayBlock('preview', $context, $blocks);
        // line 18
        echo "    ";
        $this->displayBlock('form', $context, $blocks);
        // line 19
        echo "    ";
        $this->displayBlock('list', $context, $blocks);
        // line 20
        echo "    ";
        $this->displayBlock('show', $context, $blocks);
        // line 21
        echo "
    ";
        // line 22
        if (( !twig_test_empty((isset($context["_list_table"]) ? $context["_list_table"] : $this->getContext($context, "_list_table"))) ||  !twig_test_empty((isset($context["_list_filters"]) ? $context["_list_filters"] : $this->getContext($context, "_list_filters"))))) {
            // line 23
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"navbar navbar-default sonata-list-table\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-collapse\">
                        ";
            // line 28
            if ((((array_key_exists("admin", $context) && array_key_exists("action", $context)) && ((isset($context["action"]) ? $context["action"] : $this->getContext($context, "action")) == "list")) && (twig_length_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "listModes", array())) > 1))) {
                // line 29
                echo "                            <div class=\"nav navbar-right btn-group\">
                                ";
                // line 30
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "listModes", array()));
                foreach ($context['_seq'] as $context["mode"] => $context["settings"]) {
                    // line 31
                    echo "                                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "generateUrl", array(0 => "list", 1 => twig_array_merge($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "query", array()), "all", array()), array("_list_mode" => $context["mode"]))), "method"), "html", null, true);
                    echo "\" class=\"btn btn-default navbar-btn btn-sm";
                    if (($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getListMode", array(), "method") == $context["mode"])) {
                        echo " active";
                    }
                    echo "\"><i class=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["settings"], "class", array()), "html", null, true);
                    echo "\"></i></a>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['mode'], $context['settings'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 33
                echo "                            </div>
                        ";
            }
            // line 35
            echo "
                        ";
            // line 36
            if ( !twig_test_empty((isset($context["_list_filters_actions"]) ? $context["_list_filters_actions"] : $this->getContext($context, "_list_filters_actions")))) {
                // line 37
                echo "                            ";
                echo (isset($context["_list_filters_actions"]) ? $context["_list_filters_actions"] : $this->getContext($context, "_list_filters_actions"));
                echo "
                        ";
            }
            // line 39
            echo "                    </div>
                </div>
            </div>
        </div>

        ";
            // line 44
            if (twig_trim_filter((isset($context["_list_filters"]) ? $context["_list_filters"] : $this->getContext($context, "_list_filters")))) {
                // line 45
                echo "            <div class=\"row\">
                ";
                // line 46
                echo (isset($context["_list_filters"]) ? $context["_list_filters"] : $this->getContext($context, "_list_filters"));
                echo "
            </div>
        ";
            }
            // line 49
            echo "
        <div class=\"row\">
            ";
            // line 51
            echo (isset($context["_list_table"]) ? $context["_list_table"] : $this->getContext($context, "_list_table"));
            echo "
        </div>
    </div>
    ";
        }
        
        $__internal_bfb62d255203fe6e815d3747bcd567308b29eb844e49cdf3b677c671092b8e72->leave($__internal_bfb62d255203fe6e815d3747bcd567308b29eb844e49cdf3b677c671092b8e72_prof);

        
        $__internal_d1190e9a5fe077f959c054938080555512c33b99c76f073d69705628d7f3006a->leave($__internal_d1190e9a5fe077f959c054938080555512c33b99c76f073d69705628d7f3006a_prof);

    }

    // line 17
    public function block_preview($context, array $blocks = array())
    {
        $__internal_517a7eeb11320377cf74b6b5a78e06a433067969169e4b9ea8a9d7cf76c947ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_517a7eeb11320377cf74b6b5a78e06a433067969169e4b9ea8a9d7cf76c947ef->enter($__internal_517a7eeb11320377cf74b6b5a78e06a433067969169e4b9ea8a9d7cf76c947ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "preview"));

        $__internal_539f124ee51690d50861c5aec1faeee4f4f271b01194de3ba86f3d9b8f5e8dd2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_539f124ee51690d50861c5aec1faeee4f4f271b01194de3ba86f3d9b8f5e8dd2->enter($__internal_539f124ee51690d50861c5aec1faeee4f4f271b01194de3ba86f3d9b8f5e8dd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "preview"));

        
        $__internal_539f124ee51690d50861c5aec1faeee4f4f271b01194de3ba86f3d9b8f5e8dd2->leave($__internal_539f124ee51690d50861c5aec1faeee4f4f271b01194de3ba86f3d9b8f5e8dd2_prof);

        
        $__internal_517a7eeb11320377cf74b6b5a78e06a433067969169e4b9ea8a9d7cf76c947ef->leave($__internal_517a7eeb11320377cf74b6b5a78e06a433067969169e4b9ea8a9d7cf76c947ef_prof);

    }

    // line 18
    public function block_form($context, array $blocks = array())
    {
        $__internal_b276dff5d3f5097f80336c750c7a5c257f6ac34dae48dce5bdc12edbd2f606b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b276dff5d3f5097f80336c750c7a5c257f6ac34dae48dce5bdc12edbd2f606b6->enter($__internal_b276dff5d3f5097f80336c750c7a5c257f6ac34dae48dce5bdc12edbd2f606b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        $__internal_bd79c8b073d3e2aed77072514a5a067cb25b520d0c62bb88229a4945dc9e6614 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd79c8b073d3e2aed77072514a5a067cb25b520d0c62bb88229a4945dc9e6614->enter($__internal_bd79c8b073d3e2aed77072514a5a067cb25b520d0c62bb88229a4945dc9e6614_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        
        $__internal_bd79c8b073d3e2aed77072514a5a067cb25b520d0c62bb88229a4945dc9e6614->leave($__internal_bd79c8b073d3e2aed77072514a5a067cb25b520d0c62bb88229a4945dc9e6614_prof);

        
        $__internal_b276dff5d3f5097f80336c750c7a5c257f6ac34dae48dce5bdc12edbd2f606b6->leave($__internal_b276dff5d3f5097f80336c750c7a5c257f6ac34dae48dce5bdc12edbd2f606b6_prof);

    }

    // line 19
    public function block_list($context, array $blocks = array())
    {
        $__internal_a25db5beaad8079ba678b5e1f989b3172bd75430d7ba20eccabaf59d9681fbf8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a25db5beaad8079ba678b5e1f989b3172bd75430d7ba20eccabaf59d9681fbf8->enter($__internal_a25db5beaad8079ba678b5e1f989b3172bd75430d7ba20eccabaf59d9681fbf8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "list"));

        $__internal_46aeb794153bd20e16f6327efcb3fc62de2d5afef232f8fdaee81ddb33aabeec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46aeb794153bd20e16f6327efcb3fc62de2d5afef232f8fdaee81ddb33aabeec->enter($__internal_46aeb794153bd20e16f6327efcb3fc62de2d5afef232f8fdaee81ddb33aabeec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "list"));

        
        $__internal_46aeb794153bd20e16f6327efcb3fc62de2d5afef232f8fdaee81ddb33aabeec->leave($__internal_46aeb794153bd20e16f6327efcb3fc62de2d5afef232f8fdaee81ddb33aabeec_prof);

        
        $__internal_a25db5beaad8079ba678b5e1f989b3172bd75430d7ba20eccabaf59d9681fbf8->leave($__internal_a25db5beaad8079ba678b5e1f989b3172bd75430d7ba20eccabaf59d9681fbf8_prof);

    }

    // line 20
    public function block_show($context, array $blocks = array())
    {
        $__internal_79bd652fc67bff13026af348550ba6d0f10f422eef9ad7c3d50c49caa831370a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79bd652fc67bff13026af348550ba6d0f10f422eef9ad7c3d50c49caa831370a->enter($__internal_79bd652fc67bff13026af348550ba6d0f10f422eef9ad7c3d50c49caa831370a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show"));

        $__internal_4c4a592040b0335f326977daed1e96cae5cb063d9334daf79673b7cd94bd7877 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c4a592040b0335f326977daed1e96cae5cb063d9334daf79673b7cd94bd7877->enter($__internal_4c4a592040b0335f326977daed1e96cae5cb063d9334daf79673b7cd94bd7877_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show"));

        
        $__internal_4c4a592040b0335f326977daed1e96cae5cb063d9334daf79673b7cd94bd7877->leave($__internal_4c4a592040b0335f326977daed1e96cae5cb063d9334daf79673b7cd94bd7877_prof);

        
        $__internal_79bd652fc67bff13026af348550ba6d0f10f422eef9ad7c3d50c49caa831370a->leave($__internal_79bd652fc67bff13026af348550ba6d0f10f422eef9ad7c3d50c49caa831370a_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle::ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  213 => 20,  196 => 19,  179 => 18,  162 => 17,  147 => 51,  143 => 49,  137 => 46,  134 => 45,  132 => 44,  125 => 39,  119 => 37,  117 => 36,  114 => 35,  110 => 33,  95 => 31,  91 => 30,  88 => 29,  86 => 28,  79 => 23,  77 => 22,  74 => 21,  71 => 20,  68 => 19,  65 => 18,  63 => 17,  60 => 16,  57 => 15,  54 => 14,  51 => 13,  33 => 12,  30 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% block content %}
    {% set _list_table = block('list_table') is defined ? block('list_table')|trim : null %}
    {% set _list_filters = block('list_filters') is defined ? block('list_filters')|trim : null %}
    {% set _list_filters_actions = block('list_filters_actions') is defined ? block('list_filters_actions')|trim : null %}

    {% block preview %}{% endblock %}
    {% block form %}{% endblock %}
    {% block list %}{% endblock %}
    {% block show %}{% endblock %}

    {% if _list_table is not empty or _list_filters is not empty %}
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"navbar navbar-default sonata-list-table\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-collapse\">
                        {% if admin is defined and action is defined and action == 'list' and admin.listModes|length > 1 %}
                            <div class=\"nav navbar-right btn-group\">
                                {% for mode, settings in admin.listModes %}
                                    <a href=\"{{ admin.generateUrl('list', app.request.query.all|merge({_list_mode: mode})) }}\" class=\"btn btn-default navbar-btn btn-sm{% if admin.getListMode() == mode %} active{% endif %}\"><i class=\"{{ settings.class }}\"></i></a>
                                {% endfor %}
                            </div>
                        {% endif %}

                        {% if _list_filters_actions is not empty %}
                            {{ _list_filters_actions|raw }}
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>

        {% if _list_filters|trim %}
            <div class=\"row\">
                {{ _list_filters|raw }}
            </div>
        {% endif %}

        <div class=\"row\">
            {{ _list_table|raw }}
        </div>
    </div>
    {% endif %}
{% endblock %}
", "SonataAdminBundle::ajax_layout.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/ajax_layout.html.twig");
    }
}
