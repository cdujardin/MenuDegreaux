<?php

/* SonataAdminBundle:CRUD:list_date.html.twig */
class __TwigTemplate_962dd4ae71e576f96033bcee9585309ae5f0ac10c5eb6bf058e1a12bcb21805a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_date.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2dca0022a4ff35fdad8871eb981a78d1ba4d936148d4c4a861c602db0c9eecfa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2dca0022a4ff35fdad8871eb981a78d1ba4d936148d4c4a861c602db0c9eecfa->enter($__internal_2dca0022a4ff35fdad8871eb981a78d1ba4d936148d4c4a861c602db0c9eecfa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_date.html.twig"));

        $__internal_71fcb9c6a5dce37c34c9b9bcb2e3b854df901b6ad9192bd87a4b527ca09b3701 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_71fcb9c6a5dce37c34c9b9bcb2e3b854df901b6ad9192bd87a4b527ca09b3701->enter($__internal_71fcb9c6a5dce37c34c9b9bcb2e3b854df901b6ad9192bd87a4b527ca09b3701_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_date.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2dca0022a4ff35fdad8871eb981a78d1ba4d936148d4c4a861c602db0c9eecfa->leave($__internal_2dca0022a4ff35fdad8871eb981a78d1ba4d936148d4c4a861c602db0c9eecfa_prof);

        
        $__internal_71fcb9c6a5dce37c34c9b9bcb2e3b854df901b6ad9192bd87a4b527ca09b3701->leave($__internal_71fcb9c6a5dce37c34c9b9bcb2e3b854df901b6ad9192bd87a4b527ca09b3701_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_f6d1af69b06bfd0142530944db383eedadbb27784c8689679a9bcc775b2f49c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f6d1af69b06bfd0142530944db383eedadbb27784c8689679a9bcc775b2f49c7->enter($__internal_f6d1af69b06bfd0142530944db383eedadbb27784c8689679a9bcc775b2f49c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_930efb81500302cb36f14a23c8d4c830970e3b3f529114091bfccc5c0b702776 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_930efb81500302cb36f14a23c8d4c830970e3b3f529114091bfccc5c0b702776->enter($__internal_930efb81500302cb36f14a23c8d4c830970e3b3f529114091bfccc5c0b702776_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            // line 16
            echo "&nbsp;";
        } elseif ($this->getAttribute($this->getAttribute(        // line 17
(isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "format", array(), "any", true, true)) {
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "format", array())), "html", null, true);
        } else {
            // line 20
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "F j, Y"), "html", null, true);
        }
        
        $__internal_930efb81500302cb36f14a23c8d4c830970e3b3f529114091bfccc5c0b702776->leave($__internal_930efb81500302cb36f14a23c8d4c830970e3b3f529114091bfccc5c0b702776_prof);

        
        $__internal_f6d1af69b06bfd0142530944db383eedadbb27784c8689679a9bcc775b2f49c7->leave($__internal_f6d1af69b06bfd0142530944db383eedadbb27784c8689679a9bcc775b2f49c7_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_date.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 20,  54 => 18,  52 => 17,  50 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field%}
    {%- if value is empty -%}
        &nbsp;
    {%- elseif field_description.options.format is defined -%}
        {{ value|date(field_description.options.format) }}
    {%- else -%}
        {{ value|date('F j, Y') }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:list_date.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list_date.html.twig");
    }
}
