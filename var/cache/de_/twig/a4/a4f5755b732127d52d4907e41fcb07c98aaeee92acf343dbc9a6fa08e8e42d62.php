<?php

/* KnpMenuBundle::menu.html.twig */
class __TwigTemplate_61353db3bc12d8ac0ba08725d3cef57b4a300cac356a02d3090fadc8c43bf83b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("knp_menu.html.twig", "KnpMenuBundle::menu.html.twig", 1);
        $this->blocks = array(
            'label' => array($this, 'block_label'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "knp_menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64fa59dbe2771a81c8ff733fe6e290bccff36ee8d97922dee0795566965b22a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64fa59dbe2771a81c8ff733fe6e290bccff36ee8d97922dee0795566965b22a3->enter($__internal_64fa59dbe2771a81c8ff733fe6e290bccff36ee8d97922dee0795566965b22a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "KnpMenuBundle::menu.html.twig"));

        $__internal_9b9c92379aca4891f2f1e6e97b5b86bfc8499880df6fce6a6c712b3d2df15e26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b9c92379aca4891f2f1e6e97b5b86bfc8499880df6fce6a6c712b3d2df15e26->enter($__internal_9b9c92379aca4891f2f1e6e97b5b86bfc8499880df6fce6a6c712b3d2df15e26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "KnpMenuBundle::menu.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_64fa59dbe2771a81c8ff733fe6e290bccff36ee8d97922dee0795566965b22a3->leave($__internal_64fa59dbe2771a81c8ff733fe6e290bccff36ee8d97922dee0795566965b22a3_prof);

        
        $__internal_9b9c92379aca4891f2f1e6e97b5b86bfc8499880df6fce6a6c712b3d2df15e26->leave($__internal_9b9c92379aca4891f2f1e6e97b5b86bfc8499880df6fce6a6c712b3d2df15e26_prof);

    }

    // line 3
    public function block_label($context, array $blocks = array())
    {
        $__internal_087cc282ef59dd5bc899fa2f64be939599f8ed0280ab6ccf22c0bab2306b1e8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_087cc282ef59dd5bc899fa2f64be939599f8ed0280ab6ccf22c0bab2306b1e8b->enter($__internal_087cc282ef59dd5bc899fa2f64be939599f8ed0280ab6ccf22c0bab2306b1e8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_3b6908e123d7022a269fc6e9e2b26ad50fef68babb993fd0a8a07778c0c70b02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b6908e123d7022a269fc6e9e2b26ad50fef68babb993fd0a8a07778c0c70b02->enter($__internal_3b6908e123d7022a269fc6e9e2b26ad50fef68babb993fd0a8a07778c0c70b02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        // line 4
        $context["translation_domain"] = $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "translation_domain", 1 => "messages"), "method");
        // line 5
        $context["label"] = $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "label", array());
        // line 6
        if ( !((isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")) === false)) {
            // line 7
            $context["label"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "translation_params", 1 => array()), "method"), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")));
        }
        // line 9
        if (($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "allow_safe_labels", array()) && $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "safe_label", 1 => false), "method"))) {
            echo (isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"));
        } else {
            echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), "html", null, true);
        }
        
        $__internal_3b6908e123d7022a269fc6e9e2b26ad50fef68babb993fd0a8a07778c0c70b02->leave($__internal_3b6908e123d7022a269fc6e9e2b26ad50fef68babb993fd0a8a07778c0c70b02_prof);

        
        $__internal_087cc282ef59dd5bc899fa2f64be939599f8ed0280ab6ccf22c0bab2306b1e8b->leave($__internal_087cc282ef59dd5bc899fa2f64be939599f8ed0280ab6ccf22c0bab2306b1e8b_prof);

    }

    public function getTemplateName()
    {
        return "KnpMenuBundle::menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 9,  55 => 7,  53 => 6,  51 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'knp_menu.html.twig' %}

{% block label %}
    {%- set translation_domain = item.extra('translation_domain', 'messages') -%}
    {%- set label = item.label -%}
    {%- if translation_domain is not same as(false) -%}
        {%- set label = label|trans(item.extra('translation_params', {}), translation_domain) -%}
    {%- endif -%}
    {%- if options.allow_safe_labels and item.extra('safe_label', false) %}{{ label|raw }}{% else %}{{ label }}{% endif -%}
{% endblock %}
", "KnpMenuBundle::menu.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\knplabs\\knp-menu-bundle/Resources/views/menu.html.twig");
    }
}
