<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_d0aeeab83eabaafcab1f17b299dc7ce5409932b1cab62c487d1ce1906e51c95e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_07410c3f21383c3df613d391507a7896e957c653f4431be4d91f45474bb4a087 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_07410c3f21383c3df613d391507a7896e957c653f4431be4d91f45474bb4a087->enter($__internal_07410c3f21383c3df613d391507a7896e957c653f4431be4d91f45474bb4a087_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_3ed3776c1b5bc508689f1a953dd213d78a63f54e304c206dbe41c3f2c6c35726 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ed3776c1b5bc508689f1a953dd213d78a63f54e304c206dbe41c3f2c6c35726->enter($__internal_3ed3776c1b5bc508689f1a953dd213d78a63f54e304c206dbe41c3f2c6c35726_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_07410c3f21383c3df613d391507a7896e957c653f4431be4d91f45474bb4a087->leave($__internal_07410c3f21383c3df613d391507a7896e957c653f4431be4d91f45474bb4a087_prof);

        
        $__internal_3ed3776c1b5bc508689f1a953dd213d78a63f54e304c206dbe41c3f2c6c35726->leave($__internal_3ed3776c1b5bc508689f1a953dd213d78a63f54e304c206dbe41c3f2c6c35726_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\hidden_widget.html.php");
    }
}
