<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_c08923ffbd4db2e65acfc7b4deec4befcaeb5f69602bb889d022c8649de3ea02 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c129f17b81f2c84d62a2838855b82dc4f8afd512d83ec9721424933c1f55564 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c129f17b81f2c84d62a2838855b82dc4f8afd512d83ec9721424933c1f55564->enter($__internal_0c129f17b81f2c84d62a2838855b82dc4f8afd512d83ec9721424933c1f55564_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_82ebb21d7be8a0a94a4081c1c404be3eb947cf20f7b4c521d930066473c7e706 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_82ebb21d7be8a0a94a4081c1c404be3eb947cf20f7b4c521d930066473c7e706->enter($__internal_82ebb21d7be8a0a94a4081c1c404be3eb947cf20f7b4c521d930066473c7e706_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_0c129f17b81f2c84d62a2838855b82dc4f8afd512d83ec9721424933c1f55564->leave($__internal_0c129f17b81f2c84d62a2838855b82dc4f8afd512d83ec9721424933c1f55564_prof);

        
        $__internal_82ebb21d7be8a0a94a4081c1c404be3eb947cf20f7b4c521d930066473c7e706->leave($__internal_82ebb21d7be8a0a94a4081c1c404be3eb947cf20f7b4c521d930066473c7e706_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_row.html.php");
    }
}
