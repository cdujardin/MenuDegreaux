<?php

/* ::layout.html.twig */
class __TwigTemplate_eb808de4fc20b42dba3cf98070b86dc5655227bb35ee3156731a9438fc46e884 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "::layout.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f410f5a8d77f6f9998cc32015a042d2c44f2a2a3b8fcc16bcad7ba100c25918 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f410f5a8d77f6f9998cc32015a042d2c44f2a2a3b8fcc16bcad7ba100c25918->enter($__internal_4f410f5a8d77f6f9998cc32015a042d2c44f2a2a3b8fcc16bcad7ba100c25918_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::layout.html.twig"));

        $__internal_51ceb63f3e41b7951262cc34da4465a522d914aa8a1f8199604562030c217914 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51ceb63f3e41b7951262cc34da4465a522d914aa8a1f8199604562030c217914->enter($__internal_51ceb63f3e41b7951262cc34da4465a522d914aa8a1f8199604562030c217914_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4f410f5a8d77f6f9998cc32015a042d2c44f2a2a3b8fcc16bcad7ba100c25918->leave($__internal_4f410f5a8d77f6f9998cc32015a042d2c44f2a2a3b8fcc16bcad7ba100c25918_prof);

        
        $__internal_51ceb63f3e41b7951262cc34da4465a522d914aa8a1f8199604562030c217914->leave($__internal_51ceb63f3e41b7951262cc34da4465a522d914aa8a1f8199604562030c217914_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_1a077df9468ef2c0cebe35f81a47a2c55bfc4593fc45e885130c28d5c574157b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a077df9468ef2c0cebe35f81a47a2c55bfc4593fc45e885130c28d5c574157b->enter($__internal_1a077df9468ef2c0cebe35f81a47a2c55bfc4593fc45e885130c28d5c574157b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_01e1088d44af2fef81cff894051bd3204adc57f98e942770553a29a88d92833b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01e1088d44af2fef81cff894051bd3204adc57f98e942770553a29a88d92833b->enter($__internal_01e1088d44af2fef81cff894051bd3204adc57f98e942770553a29a88d92833b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_01e1088d44af2fef81cff894051bd3204adc57f98e942770553a29a88d92833b->leave($__internal_01e1088d44af2fef81cff894051bd3204adc57f98e942770553a29a88d92833b_prof);

        
        $__internal_1a077df9468ef2c0cebe35f81a47a2c55bfc4593fc45e885130c28d5c574157b->leave($__internal_1a077df9468ef2c0cebe35f81a47a2c55bfc4593fc45e885130c28d5c574157b_prof);

    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        $__internal_4bb9e1853082b30dc12ca3d5014c9baee68d76bda0137cfa3b6bc10db5be54ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4bb9e1853082b30dc12ca3d5014c9baee68d76bda0137cfa3b6bc10db5be54ac->enter($__internal_4bb9e1853082b30dc12ca3d5014c9baee68d76bda0137cfa3b6bc10db5be54ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_29a4184e65e99e821876e60705101eb90d287e9f3c3366be93758e3965efc2c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29a4184e65e99e821876e60705101eb90d287e9f3c3366be93758e3965efc2c0->enter($__internal_29a4184e65e99e821876e60705101eb90d287e9f3c3366be93758e3965efc2c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "
            ";
        // line 8
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 10
        echo "            
";
        
        $__internal_29a4184e65e99e821876e60705101eb90d287e9f3c3366be93758e3965efc2c0->leave($__internal_29a4184e65e99e821876e60705101eb90d287e9f3c3366be93758e3965efc2c0_prof);

        
        $__internal_4bb9e1853082b30dc12ca3d5014c9baee68d76bda0137cfa3b6bc10db5be54ac->leave($__internal_4bb9e1853082b30dc12ca3d5014c9baee68d76bda0137cfa3b6bc10db5be54ac_prof);

    }

    // line 8
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_fd099edd4e5478998296c18baff09b8f337ee5cd760b3487341be998d47e6319 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd099edd4e5478998296c18baff09b8f337ee5cd760b3487341be998d47e6319->enter($__internal_fd099edd4e5478998296c18baff09b8f337ee5cd760b3487341be998d47e6319_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_6bc17089bdc287e1c8f82997bb256cce612291d0a229fb68e95fda3b3679661b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6bc17089bdc287e1c8f82997bb256cce612291d0a229fb68e95fda3b3679661b->enter($__internal_6bc17089bdc287e1c8f82997bb256cce612291d0a229fb68e95fda3b3679661b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 9
        echo "            ";
        
        $__internal_6bc17089bdc287e1c8f82997bb256cce612291d0a229fb68e95fda3b3679661b->leave($__internal_6bc17089bdc287e1c8f82997bb256cce612291d0a229fb68e95fda3b3679661b_prof);

        
        $__internal_fd099edd4e5478998296c18baff09b8f337ee5cd760b3487341be998d47e6319->leave($__internal_fd099edd4e5478998296c18baff09b8f337ee5cd760b3487341be998d47e6319_prof);

    }

    public function getTemplateName()
    {
        return "::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 9,  84 => 8,  73 => 10,  71 => 8,  68 => 7,  59 => 6,  42 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}


{% block title %}{% endblock %}

{% block content %}

            {% block fos_user_content %}
            {% endblock fos_user_content %}
            
{% endblock %}
", "::layout.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources\\views/layout.html.twig");
    }
}
