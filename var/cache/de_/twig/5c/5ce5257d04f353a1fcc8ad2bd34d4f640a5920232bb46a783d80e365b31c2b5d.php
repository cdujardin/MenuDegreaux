<?php

/* SonataAdminBundle:CRUD:select_subclass.html.twig */
class __TwigTemplate_048c23e5dbcf04ea316c42ef037c5f3b28e685f71b67e588025afabf34b3a57e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 11
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:select_subclass.html.twig", 11);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28f25d3c8249c80f65f74644d7e8920059a8c97524959a6fae7d120ea4ef818a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_28f25d3c8249c80f65f74644d7e8920059a8c97524959a6fae7d120ea4ef818a->enter($__internal_28f25d3c8249c80f65f74644d7e8920059a8c97524959a6fae7d120ea4ef818a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:select_subclass.html.twig"));

        $__internal_bed949ebcb0d87fa52395462995d699ca4eaf22727d83b32d946a048dc8d5d95 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bed949ebcb0d87fa52395462995d699ca4eaf22727d83b32d946a048dc8d5d95->enter($__internal_bed949ebcb0d87fa52395462995d699ca4eaf22727d83b32d946a048dc8d5d95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:select_subclass.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_28f25d3c8249c80f65f74644d7e8920059a8c97524959a6fae7d120ea4ef818a->leave($__internal_28f25d3c8249c80f65f74644d7e8920059a8c97524959a6fae7d120ea4ef818a_prof);

        
        $__internal_bed949ebcb0d87fa52395462995d699ca4eaf22727d83b32d946a048dc8d5d95->leave($__internal_bed949ebcb0d87fa52395462995d699ca4eaf22727d83b32d946a048dc8d5d95_prof);

    }

    // line 13
    public function block_title($context, array $blocks = array())
    {
        $__internal_197beadf107574b51fd755ec6982196e34e88d1888424c91e1e09df042c8859e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_197beadf107574b51fd755ec6982196e34e88d1888424c91e1e09df042c8859e->enter($__internal_197beadf107574b51fd755ec6982196e34e88d1888424c91e1e09df042c8859e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_432bfa91b92edf6ea97d2a602126a1c866003c62f9170e5fcc90be01e4b9dc0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_432bfa91b92edf6ea97d2a602126a1c866003c62f9170e5fcc90be01e4b9dc0a->enter($__internal_432bfa91b92edf6ea97d2a602126a1c866003c62f9170e5fcc90be01e4b9dc0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_select_subclass", array(), "SonataAdminBundle"), "html", null, true);
        
        $__internal_432bfa91b92edf6ea97d2a602126a1c866003c62f9170e5fcc90be01e4b9dc0a->leave($__internal_432bfa91b92edf6ea97d2a602126a1c866003c62f9170e5fcc90be01e4b9dc0a_prof);

        
        $__internal_197beadf107574b51fd755ec6982196e34e88d1888424c91e1e09df042c8859e->leave($__internal_197beadf107574b51fd755ec6982196e34e88d1888424c91e1e09df042c8859e_prof);

    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        $__internal_aa7cdfb76f36f053b4f864c622ed3b740dfe0a4a9a4936cdee0caaf915fa0fb9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa7cdfb76f36f053b4f864c622ed3b740dfe0a4a9a4936cdee0caaf915fa0fb9->enter($__internal_aa7cdfb76f36f053b4f864c622ed3b740dfe0a4a9a4936cdee0caaf915fa0fb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_93324497afc1553fc354a7325a4388601a321d4fd03e24f8b3b12eeffeb90cd5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93324497afc1553fc354a7325a4388601a321d4fd03e24f8b3b12eeffeb90cd5->enter($__internal_93324497afc1553fc354a7325a4388601a321d4fd03e24f8b3b12eeffeb90cd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 16
        echo "    <div class=\"box box-success\">
        <div class=\"box-header\">
            <h3 class=\"box-title\">
                ";
        // line 19
        $this->displayBlock("title", $context, $blocks);
        echo "
            </h3>
        </div>
        <div class=\"box-body\">
            ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "subclasses", array())));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["subclass"]) {
            // line 24
            echo "                <div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">
                    <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "generateUrl", array(0 => (isset($context["action"]) ? $context["action"] : $this->getContext($context, "action")), 1 => array("subclass" => $context["subclass"])), "method"), "html", null, true);
            echo "\"
                       class=\"btn btn-app btn-block\"
                            >
                        <i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i>
                        ";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["subclass"], array(), (($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "translationdomain", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "translationdomain", array()), "SonataAdminBundle")) : ("SonataAdminBundle"))), "html", null, true);
            echo "
                    </a>
                </div>
            ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 33
            echo "                <span class=\"alert alert-info\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("no_subclass_available", array(), "SonataAdminBundle"), "html", null, true);
            echo "</span>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subclass'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "            <div class=\"clearfix\"></div>
        </div>
    </div>
";
        
        $__internal_93324497afc1553fc354a7325a4388601a321d4fd03e24f8b3b12eeffeb90cd5->leave($__internal_93324497afc1553fc354a7325a4388601a321d4fd03e24f8b3b12eeffeb90cd5_prof);

        
        $__internal_aa7cdfb76f36f053b4f864c622ed3b740dfe0a4a9a4936cdee0caaf915fa0fb9->leave($__internal_aa7cdfb76f36f053b4f864c622ed3b740dfe0a4a9a4936cdee0caaf915fa0fb9_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:select_subclass.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 35,  103 => 33,  94 => 29,  87 => 25,  84 => 24,  79 => 23,  72 => 19,  67 => 16,  58 => 15,  40 => 13,  19 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% extends base_template %}

{% block title %}{{ 'title_select_subclass'|trans({}, 'SonataAdminBundle') }}{% endblock %}

{% block content %}
    <div class=\"box box-success\">
        <div class=\"box-header\">
            <h3 class=\"box-title\">
                {{ block('title') }}
            </h3>
        </div>
        <div class=\"box-body\">
            {% for subclass in admin.subclasses|keys %}
                <div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">
                    <a href=\"{{ admin.generateUrl(action, {'subclass': subclass }) }}\"
                       class=\"btn btn-app btn-block\"
                            >
                        <i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i>
                        {{ subclass|trans({}, admin.translationdomain|default('SonataAdminBundle')) }}
                    </a>
                </div>
            {% else %}
                <span class=\"alert alert-info\">{{ 'no_subclass_available'|trans({}, 'SonataAdminBundle') }}</span>
            {% endfor %}
            <div class=\"clearfix\"></div>
        </div>
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:select_subclass.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/select_subclass.html.twig");
    }
}
