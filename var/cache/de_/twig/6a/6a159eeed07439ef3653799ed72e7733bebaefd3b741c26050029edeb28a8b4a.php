<?php

/* SonataAdminBundle:CRUD:edit_boolean.html.twig */
class __TwigTemplate_86ee2c454c21e15eaf9e385899c6cb4f978905594bd074023a779a45202edda3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'field' => array($this, 'block_field'),
            'label' => array($this, 'block_label'),
            'errors' => array($this, 'block_errors'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8e7f4026da9f4b489ec007ed6922ecbd9302828131d62ee69b7ae8e38b4afb1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d8e7f4026da9f4b489ec007ed6922ecbd9302828131d62ee69b7ae8e38b4afb1->enter($__internal_d8e7f4026da9f4b489ec007ed6922ecbd9302828131d62ee69b7ae8e38b4afb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_boolean.html.twig"));

        $__internal_f2d8ef152bdd9f20b1130c109ab439db9665bdd2edc14b1342f42b016bc060ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2d8ef152bdd9f20b1130c109ab439db9665bdd2edc14b1342f42b016bc060ff->enter($__internal_f2d8ef152bdd9f20b1130c109ab439db9665bdd2edc14b1342f42b016bc060ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_boolean.html.twig"));

        // line 11
        echo "
<div>
    <div class=\"sonata-ba-field ";
        // line 13
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), "vars", array()), "errors", array())) > 0)) {
            echo "sonata-ba-field-error";
        }
        echo "\">
        ";
        // line 14
        $this->displayBlock('field', $context, $blocks);
        // line 15
        echo "        ";
        $this->displayBlock('label', $context, $blocks);
        // line 23
        echo "
        <div class=\"sonata-ba-field-error-messages\">
            ";
        // line 25
        $this->displayBlock('errors', $context, $blocks);
        // line 26
        echo "        </div>

    </div>
</div>
";
        
        $__internal_d8e7f4026da9f4b489ec007ed6922ecbd9302828131d62ee69b7ae8e38b4afb1->leave($__internal_d8e7f4026da9f4b489ec007ed6922ecbd9302828131d62ee69b7ae8e38b4afb1_prof);

        
        $__internal_f2d8ef152bdd9f20b1130c109ab439db9665bdd2edc14b1342f42b016bc060ff->leave($__internal_f2d8ef152bdd9f20b1130c109ab439db9665bdd2edc14b1342f42b016bc060ff_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_450e61509f18ac63579c3aef3b832e852495f16ecf10848c658573abc334f006 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_450e61509f18ac63579c3aef3b832e852495f16ecf10848c658573abc334f006->enter($__internal_450e61509f18ac63579c3aef3b832e852495f16ecf10848c658573abc334f006_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_03ec25adc9d2cb0fa8718476d3e429902c799eaa06f12e99a7ea34b160d4e17d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_03ec25adc9d2cb0fa8718476d3e429902c799eaa06f12e99a7ea34b160d4e17d->enter($__internal_03ec25adc9d2cb0fa8718476d3e429902c799eaa06f12e99a7ea34b160d4e17d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'widget');
        
        $__internal_03ec25adc9d2cb0fa8718476d3e429902c799eaa06f12e99a7ea34b160d4e17d->leave($__internal_03ec25adc9d2cb0fa8718476d3e429902c799eaa06f12e99a7ea34b160d4e17d_prof);

        
        $__internal_450e61509f18ac63579c3aef3b832e852495f16ecf10848c658573abc334f006->leave($__internal_450e61509f18ac63579c3aef3b832e852495f16ecf10848c658573abc334f006_prof);

    }

    // line 15
    public function block_label($context, array $blocks = array())
    {
        $__internal_f224b686bb649f16302b29dff5be32e3882923a30152480f8537df6199d18686 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f224b686bb649f16302b29dff5be32e3882923a30152480f8537df6199d18686->enter($__internal_f224b686bb649f16302b29dff5be32e3882923a30152480f8537df6199d18686_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_557f26c0748004f539aabf06e13adb117b845052d1dccc7789dc7610bf77d46b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_557f26c0748004f539aabf06e13adb117b845052d1dccc7789dc7610bf77d46b->enter($__internal_557f26c0748004f539aabf06e13adb117b845052d1dccc7789dc7610bf77d46b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        // line 16
        echo "            ";
        if ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "name", array(), "any", true, true)) {
            // line 17
            echo "                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'label', (twig_test_empty($_label_ = $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "name", array())) ? array() : array("label" => $_label_)));
            echo "
            ";
        } else {
            // line 19
            echo "                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'label');
            echo "
            ";
        }
        // line 21
        echo "            <br>
        ";
        
        $__internal_557f26c0748004f539aabf06e13adb117b845052d1dccc7789dc7610bf77d46b->leave($__internal_557f26c0748004f539aabf06e13adb117b845052d1dccc7789dc7610bf77d46b_prof);

        
        $__internal_f224b686bb649f16302b29dff5be32e3882923a30152480f8537df6199d18686->leave($__internal_f224b686bb649f16302b29dff5be32e3882923a30152480f8537df6199d18686_prof);

    }

    // line 25
    public function block_errors($context, array $blocks = array())
    {
        $__internal_ddf9216aaa942200e9d97675241fde37352ab4abec471971283cacded46342e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ddf9216aaa942200e9d97675241fde37352ab4abec471971283cacded46342e4->enter($__internal_ddf9216aaa942200e9d97675241fde37352ab4abec471971283cacded46342e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "errors"));

        $__internal_186667b4b7cdae2ae43feb0868a1df699dc66c5475fea6b168a67bf291859a83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_186667b4b7cdae2ae43feb0868a1df699dc66c5475fea6b168a67bf291859a83->enter($__internal_186667b4b7cdae2ae43feb0868a1df699dc66c5475fea6b168a67bf291859a83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "errors"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'errors');
        
        $__internal_186667b4b7cdae2ae43feb0868a1df699dc66c5475fea6b168a67bf291859a83->leave($__internal_186667b4b7cdae2ae43feb0868a1df699dc66c5475fea6b168a67bf291859a83_prof);

        
        $__internal_ddf9216aaa942200e9d97675241fde37352ab4abec471971283cacded46342e4->leave($__internal_ddf9216aaa942200e9d97675241fde37352ab4abec471971283cacded46342e4_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_boolean.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 25,  105 => 21,  99 => 19,  93 => 17,  90 => 16,  81 => 15,  63 => 14,  49 => 26,  47 => 25,  43 => 23,  40 => 15,  38 => 14,  32 => 13,  28 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<div>
    <div class=\"sonata-ba-field {% if field_element.vars.errors|length > 0 %}sonata-ba-field-error{% endif %}\">
        {% block field %}{{ form_widget(field_element) }}{% endblock %}
        {% block label %}
            {% if field_description.options.name is defined %}
                {{ form_label(field_element, field_description.options.name) }}
            {% else %}
                {{ form_label(field_element) }}
            {% endif %}
            <br>
        {% endblock %}

        <div class=\"sonata-ba-field-error-messages\">
            {% block errors %}{{ form_errors(field_element) }}{% endblock %}
        </div>

    </div>
</div>
", "SonataAdminBundle:CRUD:edit_boolean.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/edit_boolean.html.twig");
    }
}
