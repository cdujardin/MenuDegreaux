<?php

/* SonataAdminBundle:CRUD:edit_array.html.twig */
class __TwigTemplate_92cbf6bc54ab5e2b61ccae284200924d649f37fba477bc26f351d00115568bdb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:edit_array.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec4a756b185c995186072cb88ea3b29ade81b7c6cf3cf46839d93b331cf9d8f0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec4a756b185c995186072cb88ea3b29ade81b7c6cf3cf46839d93b331cf9d8f0->enter($__internal_ec4a756b185c995186072cb88ea3b29ade81b7c6cf3cf46839d93b331cf9d8f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_array.html.twig"));

        $__internal_91c712534952e5b738bc1caafe14264d8fe1f98cab94b48e5bac28de4ca7e4a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91c712534952e5b738bc1caafe14264d8fe1f98cab94b48e5bac28de4ca7e4a0->enter($__internal_91c712534952e5b738bc1caafe14264d8fe1f98cab94b48e5bac28de4ca7e4a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_array.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ec4a756b185c995186072cb88ea3b29ade81b7c6cf3cf46839d93b331cf9d8f0->leave($__internal_ec4a756b185c995186072cb88ea3b29ade81b7c6cf3cf46839d93b331cf9d8f0_prof);

        
        $__internal_91c712534952e5b738bc1caafe14264d8fe1f98cab94b48e5bac28de4ca7e4a0->leave($__internal_91c712534952e5b738bc1caafe14264d8fe1f98cab94b48e5bac28de4ca7e4a0_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_73e51b6c58767d8ac6e9072bdab0daa19a95facfba31786094821646c7a2b146 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73e51b6c58767d8ac6e9072bdab0daa19a95facfba31786094821646c7a2b146->enter($__internal_73e51b6c58767d8ac6e9072bdab0daa19a95facfba31786094821646c7a2b146_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_7ede06d6f80b79e48636b80bbeaa2b24d64f48ed88323cf2da420f9604a6d064 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ede06d6f80b79e48636b80bbeaa2b24d64f48ed88323cf2da420f9604a6d064->enter($__internal_7ede06d6f80b79e48636b80bbeaa2b24d64f48ed88323cf2da420f9604a6d064_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <span class=\"edit\">
        ";
        // line 16
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'widget', array("attr" => array("class" => "title")));
        echo "
    </span>
";
        
        $__internal_7ede06d6f80b79e48636b80bbeaa2b24d64f48ed88323cf2da420f9604a6d064->leave($__internal_7ede06d6f80b79e48636b80bbeaa2b24d64f48ed88323cf2da420f9604a6d064_prof);

        
        $__internal_73e51b6c58767d8ac6e9072bdab0daa19a95facfba31786094821646c7a2b146->leave($__internal_73e51b6c58767d8ac6e9072bdab0daa19a95facfba31786094821646c7a2b146_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_array.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block field %}
    <span class=\"edit\">
        {{ form_widget(field_element, {'attr': {'class' : 'title'}}) }}
    </span>
{% endblock %}
", "SonataAdminBundle:CRUD:edit_array.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/edit_array.html.twig");
    }
}
