<?php

/* SonataAdminBundle:CRUD:show_boolean.html.twig */
class __TwigTemplate_b64575126004e7d68b755fa3830d1d1a0d1b2f2e7269a67941e693d6e027f405 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_boolean.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_442aec943cedfd3f2cb650ed4ca9a4f1c351226243778d42585d1f5e17ff5166 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_442aec943cedfd3f2cb650ed4ca9a4f1c351226243778d42585d1f5e17ff5166->enter($__internal_442aec943cedfd3f2cb650ed4ca9a4f1c351226243778d42585d1f5e17ff5166_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_boolean.html.twig"));

        $__internal_4be435b1d120e9d7e3969560e99498e07dae8e8e4b46b3a97c150eccc29203db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4be435b1d120e9d7e3969560e99498e07dae8e8e4b46b3a97c150eccc29203db->enter($__internal_4be435b1d120e9d7e3969560e99498e07dae8e8e4b46b3a97c150eccc29203db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_boolean.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_442aec943cedfd3f2cb650ed4ca9a4f1c351226243778d42585d1f5e17ff5166->leave($__internal_442aec943cedfd3f2cb650ed4ca9a4f1c351226243778d42585d1f5e17ff5166_prof);

        
        $__internal_4be435b1d120e9d7e3969560e99498e07dae8e8e4b46b3a97c150eccc29203db->leave($__internal_4be435b1d120e9d7e3969560e99498e07dae8e8e4b46b3a97c150eccc29203db_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_80f7d7ea22cceba59cb32322db3912717ec066ad5d5b02fd98a23c8246bb0dbc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80f7d7ea22cceba59cb32322db3912717ec066ad5d5b02fd98a23c8246bb0dbc->enter($__internal_80f7d7ea22cceba59cb32322db3912717ec066ad5d5b02fd98a23c8246bb0dbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_0cf5422e2e3cb7890059d79f76c88ce9d08559331c41a7d015d1b9969cc426de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0cf5422e2e3cb7890059d79f76c88ce9d08559331c41a7d015d1b9969cc426de->enter($__internal_0cf5422e2e3cb7890059d79f76c88ce9d08559331c41a7d015d1b9969cc426de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:display_boolean.html.twig", "SonataAdminBundle:CRUD:show_boolean.html.twig", 15)->display($context);
        
        $__internal_0cf5422e2e3cb7890059d79f76c88ce9d08559331c41a7d015d1b9969cc426de->leave($__internal_0cf5422e2e3cb7890059d79f76c88ce9d08559331c41a7d015d1b9969cc426de_prof);

        
        $__internal_80f7d7ea22cceba59cb32322db3912717ec066ad5d5b02fd98a23c8246bb0dbc->leave($__internal_80f7d7ea22cceba59cb32322db3912717ec066ad5d5b02fd98a23c8246bb0dbc_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_boolean.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 15,  40 => 14,  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field %}
    {%- include 'SonataAdminBundle:CRUD:display_boolean.html.twig' -%}
{% endblock %}
", "SonataAdminBundle:CRUD:show_boolean.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/show_boolean.html.twig");
    }
}
