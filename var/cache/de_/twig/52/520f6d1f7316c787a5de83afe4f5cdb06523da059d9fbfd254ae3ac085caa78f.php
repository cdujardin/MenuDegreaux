<?php

/* SonataAdminBundle:CRUD:base_edit.html.twig */
class __TwigTemplate_51d7c106e4dd53983de9def4ac42aa990cbbcd4533c2c9726fe5ad8c358c4973 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $_trait_0 = $this->loadTemplate("SonataAdminBundle:CRUD:base_edit_form.html.twig", "SonataAdminBundle:CRUD:base_edit.html.twig", 33);
        // line 33
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."SonataAdminBundle:CRUD:base_edit_form.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        if (!isset($_trait_0_blocks["form"])) {
            throw new Twig_Error_Runtime(sprintf('Block "form" is not defined in trait "SonataAdminBundle:CRUD:base_edit_form.html.twig".'));
        }

        $_trait_0_blocks["parentForm"] = $_trait_0_blocks["form"]; unset($_trait_0_blocks["form"]);

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'title' => array($this, 'block_title'),
                'navbar_title' => array($this, 'block_navbar_title'),
                'actions' => array($this, 'block_actions'),
                'tab_menu' => array($this, 'block_tab_menu'),
                'form' => array($this, 'block_form'),
            )
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:base_edit.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_01271a11ec48f46513deca5d33e605722092fc708c2103004f1161776972eb68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01271a11ec48f46513deca5d33e605722092fc708c2103004f1161776972eb68->enter($__internal_01271a11ec48f46513deca5d33e605722092fc708c2103004f1161776972eb68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_edit.html.twig"));

        $__internal_2defd981480634f331fbf1b24ef1580b5780be7cc55e1e961a422f4403fcbaa7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2defd981480634f331fbf1b24ef1580b5780be7cc55e1e961a422f4403fcbaa7->enter($__internal_2defd981480634f331fbf1b24ef1580b5780be7cc55e1e961a422f4403fcbaa7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_edit.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_01271a11ec48f46513deca5d33e605722092fc708c2103004f1161776972eb68->leave($__internal_01271a11ec48f46513deca5d33e605722092fc708c2103004f1161776972eb68_prof);

        
        $__internal_2defd981480634f331fbf1b24ef1580b5780be7cc55e1e961a422f4403fcbaa7->leave($__internal_2defd981480634f331fbf1b24ef1580b5780be7cc55e1e961a422f4403fcbaa7_prof);

    }

    // line 14
    public function block_title($context, array $blocks = array())
    {
        $__internal_488a36a6fc4ff96aa96b774e8695ebfa601e7d9efaa2010e22530df3f364e5b4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_488a36a6fc4ff96aa96b774e8695ebfa601e7d9efaa2010e22530df3f364e5b4->enter($__internal_488a36a6fc4ff96aa96b774e8695ebfa601e7d9efaa2010e22530df3f364e5b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_31d88a7dba4958763f34441dba46b04a14e6372348f82cbcad82a51f6d86bff7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_31d88a7dba4958763f34441dba46b04a14e6372348f82cbcad82a51f6d86bff7->enter($__internal_31d88a7dba4958763f34441dba46b04a14e6372348f82cbcad82a51f6d86bff7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 15
        echo "    ";
        // line 16
        echo "    ";
        if ( !(null === ((array_key_exists("objectId", $context)) ? (_twig_default_filter((isset($context["objectId"]) ? $context["objectId"] : $this->getContext($context, "objectId")), $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "id", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"))) : ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "id", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"))))) {
            // line 17
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_edit", array("%name%" => twig_truncate_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "toString", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), 15)), "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        } else {
            // line 19
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_create", array(), "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        }
        
        $__internal_31d88a7dba4958763f34441dba46b04a14e6372348f82cbcad82a51f6d86bff7->leave($__internal_31d88a7dba4958763f34441dba46b04a14e6372348f82cbcad82a51f6d86bff7_prof);

        
        $__internal_488a36a6fc4ff96aa96b774e8695ebfa601e7d9efaa2010e22530df3f364e5b4->leave($__internal_488a36a6fc4ff96aa96b774e8695ebfa601e7d9efaa2010e22530df3f364e5b4_prof);

    }

    // line 23
    public function block_navbar_title($context, array $blocks = array())
    {
        $__internal_40265351d1f39111e9d56f62c9c72c11bc0260a6abdf027b5faff6e03163c547 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40265351d1f39111e9d56f62c9c72c11bc0260a6abdf027b5faff6e03163c547->enter($__internal_40265351d1f39111e9d56f62c9c72c11bc0260a6abdf027b5faff6e03163c547_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navbar_title"));

        $__internal_2394cb162045d23a2d54f9fe46e4ee97d0643cd8f7a41645e0808667ade9bcde = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2394cb162045d23a2d54f9fe46e4ee97d0643cd8f7a41645e0808667ade9bcde->enter($__internal_2394cb162045d23a2d54f9fe46e4ee97d0643cd8f7a41645e0808667ade9bcde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navbar_title"));

        // line 24
        echo "    ";
        $this->displayBlock("title", $context, $blocks);
        echo "
";
        
        $__internal_2394cb162045d23a2d54f9fe46e4ee97d0643cd8f7a41645e0808667ade9bcde->leave($__internal_2394cb162045d23a2d54f9fe46e4ee97d0643cd8f7a41645e0808667ade9bcde_prof);

        
        $__internal_40265351d1f39111e9d56f62c9c72c11bc0260a6abdf027b5faff6e03163c547->leave($__internal_40265351d1f39111e9d56f62c9c72c11bc0260a6abdf027b5faff6e03163c547_prof);

    }

    // line 27
    public function block_actions($context, array $blocks = array())
    {
        $__internal_1679d40524aab6ea7bc23fad604f5cabfa1d2da227be66a55b343bb9e48df0f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1679d40524aab6ea7bc23fad604f5cabfa1d2da227be66a55b343bb9e48df0f1->enter($__internal_1679d40524aab6ea7bc23fad604f5cabfa1d2da227be66a55b343bb9e48df0f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        $__internal_0cdaff89b6cb911de081a4acbb5dcac023951f7eab52b4a064dca9217cc29017 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0cdaff89b6cb911de081a4acbb5dcac023951f7eab52b4a064dca9217cc29017->enter($__internal_0cdaff89b6cb911de081a4acbb5dcac023951f7eab52b4a064dca9217cc29017_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 28
        $this->loadTemplate("SonataAdminBundle:CRUD:action_buttons.html.twig", "SonataAdminBundle:CRUD:base_edit.html.twig", 28)->display($context);
        
        $__internal_0cdaff89b6cb911de081a4acbb5dcac023951f7eab52b4a064dca9217cc29017->leave($__internal_0cdaff89b6cb911de081a4acbb5dcac023951f7eab52b4a064dca9217cc29017_prof);

        
        $__internal_1679d40524aab6ea7bc23fad604f5cabfa1d2da227be66a55b343bb9e48df0f1->leave($__internal_1679d40524aab6ea7bc23fad604f5cabfa1d2da227be66a55b343bb9e48df0f1_prof);

    }

    // line 31
    public function block_tab_menu($context, array $blocks = array())
    {
        $__internal_fac965caef22e8594af246fefd98f35d80bd21844d7d3249eba528ecd673d613 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fac965caef22e8594af246fefd98f35d80bd21844d7d3249eba528ecd673d613->enter($__internal_fac965caef22e8594af246fefd98f35d80bd21844d7d3249eba528ecd673d613_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        $__internal_56fbff424c5cfe80f6939d86cecbb3692b886162cb5342c20559e49ffceb766a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56fbff424c5cfe80f6939d86cecbb3692b886162cb5342c20559e49ffceb766a->enter($__internal_56fbff424c5cfe80f6939d86cecbb3692b886162cb5342c20559e49ffceb766a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "sidemenu", array(0 => (isset($context["action"]) ? $context["action"] : $this->getContext($context, "action"))), "method"), array("currentClass" => "active", "template" => $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getTemplate", array(0 => "tab_menu_template"), "method")), "twig");
        
        $__internal_56fbff424c5cfe80f6939d86cecbb3692b886162cb5342c20559e49ffceb766a->leave($__internal_56fbff424c5cfe80f6939d86cecbb3692b886162cb5342c20559e49ffceb766a_prof);

        
        $__internal_fac965caef22e8594af246fefd98f35d80bd21844d7d3249eba528ecd673d613->leave($__internal_fac965caef22e8594af246fefd98f35d80bd21844d7d3249eba528ecd673d613_prof);

    }

    // line 35
    public function block_form($context, array $blocks = array())
    {
        $__internal_6e43263a46243b6838d93349d409a312b71e826d5ed83fe7076ddd3c85b10e69 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6e43263a46243b6838d93349d409a312b71e826d5ed83fe7076ddd3c85b10e69->enter($__internal_6e43263a46243b6838d93349d409a312b71e826d5ed83fe7076ddd3c85b10e69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        $__internal_19a4c7c13bcd3ce03c50e927a60f25a0a0ec210dd865aff07fef51b9e343182e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19a4c7c13bcd3ce03c50e927a60f25a0a0ec210dd865aff07fef51b9e343182e->enter($__internal_19a4c7c13bcd3ce03c50e927a60f25a0a0ec210dd865aff07fef51b9e343182e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 36
        echo "    ";
        $this->displayBlock("parentForm", $context, $blocks);
        echo "
";
        
        $__internal_19a4c7c13bcd3ce03c50e927a60f25a0a0ec210dd865aff07fef51b9e343182e->leave($__internal_19a4c7c13bcd3ce03c50e927a60f25a0a0ec210dd865aff07fef51b9e343182e_prof);

        
        $__internal_6e43263a46243b6838d93349d409a312b71e826d5ed83fe7076ddd3c85b10e69->leave($__internal_6e43263a46243b6838d93349d409a312b71e826d5ed83fe7076ddd3c85b10e69_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 36,  154 => 35,  136 => 31,  126 => 28,  117 => 27,  104 => 24,  95 => 23,  81 => 19,  75 => 17,  72 => 16,  70 => 15,  61 => 14,  40 => 12,  12 => 33,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block title %}
    {# NEXT_MAJOR: remove default filter #}
    {% if objectId|default(admin.id(object)) is not null %}
        {{ \"title_edit\"|trans({'%name%': admin.toString(object)|truncate(15) }, 'SonataAdminBundle') }}
    {% else %}
        {{ \"title_create\"|trans({}, 'SonataAdminBundle') }}
    {% endif %}
{% endblock %}

{% block navbar_title %}
    {{ block('title') }}
{% endblock %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:action_buttons.html.twig' %}
{%- endblock -%}

{% block tab_menu %}{{ knp_menu_render(admin.sidemenu(action), {'currentClass' : 'active', 'template': sonata_admin.adminPool.getTemplate('tab_menu_template')}, 'twig') }}{% endblock %}

{% use 'SonataAdminBundle:CRUD:base_edit_form.html.twig' with form as parentForm %}

{% block form %}
    {{ block('parentForm') }}
{% endblock %}
", "SonataAdminBundle:CRUD:base_edit.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/base_edit.html.twig");
    }
}
