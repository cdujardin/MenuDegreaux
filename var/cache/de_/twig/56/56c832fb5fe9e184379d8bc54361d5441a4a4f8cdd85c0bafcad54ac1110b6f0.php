<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_a48987a9eff7d02690be2094b8764fe0bc71f84925afe4042fcc27f5f66dbb5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ddf0a9a13b0e9af62c391fa8fbc1a8cd8777d354bd22a0802e584e115c646ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ddf0a9a13b0e9af62c391fa8fbc1a8cd8777d354bd22a0802e584e115c646ec->enter($__internal_9ddf0a9a13b0e9af62c391fa8fbc1a8cd8777d354bd22a0802e584e115c646ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_1122cc733ee21c11cdaf76278e2a9a82b62ce508cfc453dd21417f6c9015aca7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1122cc733ee21c11cdaf76278e2a9a82b62ce508cfc453dd21417f6c9015aca7->enter($__internal_1122cc733ee21c11cdaf76278e2a9a82b62ce508cfc453dd21417f6c9015aca7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_9ddf0a9a13b0e9af62c391fa8fbc1a8cd8777d354bd22a0802e584e115c646ec->leave($__internal_9ddf0a9a13b0e9af62c391fa8fbc1a8cd8777d354bd22a0802e584e115c646ec_prof);

        
        $__internal_1122cc733ee21c11cdaf76278e2a9a82b62ce508cfc453dd21417f6c9015aca7->leave($__internal_1122cc733ee21c11cdaf76278e2a9a82b62ce508cfc453dd21417f6c9015aca7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_enctype.html.php");
    }
}
