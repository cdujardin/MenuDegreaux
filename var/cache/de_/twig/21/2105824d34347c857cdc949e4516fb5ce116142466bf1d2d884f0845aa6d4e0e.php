<?php

/* menu/homepage.html.twig */
class __TwigTemplate_3c2d4f0a1bfe6c70cbafe6dc96b04bbc54edf6de0bc1ae2558297da65a0a295e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "menu/homepage.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ced4814f94eccd06c0751354d6500492f6aac3d86719db62699ee99b4b019b07 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ced4814f94eccd06c0751354d6500492f6aac3d86719db62699ee99b4b019b07->enter($__internal_ced4814f94eccd06c0751354d6500492f6aac3d86719db62699ee99b4b019b07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "menu/homepage.html.twig"));

        $__internal_c35be5d2153434e4be898923e5e567b939fe817c11de73dd4754f0d133eb7d55 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c35be5d2153434e4be898923e5e567b939fe817c11de73dd4754f0d133eb7d55->enter($__internal_c35be5d2153434e4be898923e5e567b939fe817c11de73dd4754f0d133eb7d55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "menu/homepage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ced4814f94eccd06c0751354d6500492f6aac3d86719db62699ee99b4b019b07->leave($__internal_ced4814f94eccd06c0751354d6500492f6aac3d86719db62699ee99b4b019b07_prof);

        
        $__internal_c35be5d2153434e4be898923e5e567b939fe817c11de73dd4754f0d133eb7d55->leave($__internal_c35be5d2153434e4be898923e5e567b939fe817c11de73dd4754f0d133eb7d55_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_cd32370a6337cb6071ff619d0be5df91420d4f6c03df01d7da24e3017c3c3186 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd32370a6337cb6071ff619d0be5df91420d4f6c03df01d7da24e3017c3c3186->enter($__internal_cd32370a6337cb6071ff619d0be5df91420d4f6c03df01d7da24e3017c3c3186_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_940169cad685cc090592dda9cab81745b015a0fcbdd7f25dd7a5189e20231965 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_940169cad685cc090592dda9cab81745b015a0fcbdd7f25dd7a5189e20231965->enter($__internal_940169cad685cc090592dda9cab81745b015a0fcbdd7f25dd7a5189e20231965_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "homepage";
        
        $__internal_940169cad685cc090592dda9cab81745b015a0fcbdd7f25dd7a5189e20231965->leave($__internal_940169cad685cc090592dda9cab81745b015a0fcbdd7f25dd7a5189e20231965_prof);

        
        $__internal_cd32370a6337cb6071ff619d0be5df91420d4f6c03df01d7da24e3017c3c3186->leave($__internal_cd32370a6337cb6071ff619d0be5df91420d4f6c03df01d7da24e3017c3c3186_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_95c6650135bde0ff7ce45b9140a647efdb8f048010a74bf9059a72d2dd09e788 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95c6650135bde0ff7ce45b9140a647efdb8f048010a74bf9059a72d2dd09e788->enter($__internal_95c6650135bde0ff7ce45b9140a647efdb8f048010a74bf9059a72d2dd09e788_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_d19ad33211fc3cc29c70dfb820a839567a1773020141254e23df0633667dbac3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d19ad33211fc3cc29c70dfb820a839567a1773020141254e23df0633667dbac3->enter($__internal_d19ad33211fc3cc29c70dfb820a839567a1773020141254e23df0633667dbac3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    <h1> Bienvenue sur le menu Degreaux</h1>
    <!-- choix 3 btn -->
    <div class=\"row categories\">
      <p> Vous êtes : </p>
      <div class=\"col-md-4 col-xs-12\">
          <a href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
            <button id=\"externebtn\" type=\"button\" class=\"btn btn-default\" >
                Externe
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a  href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
            <button id=\"scolairebtn\"  type=\"button\" class=\"btn btn-default \">
                Scolaire
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
            <button id=\"residentbtn\"   type=\"button\" class=\"btn btn-default \">
                Résident
            </button>
        </a>
      </div>
    </div>
";
        
        $__internal_d19ad33211fc3cc29c70dfb820a839567a1773020141254e23df0633667dbac3->leave($__internal_d19ad33211fc3cc29c70dfb820a839567a1773020141254e23df0633667dbac3_prof);

        
        $__internal_95c6650135bde0ff7ce45b9140a647efdb8f048010a74bf9059a72d2dd09e788->leave($__internal_95c6650135bde0ff7ce45b9140a647efdb8f048010a74bf9059a72d2dd09e788_prof);

    }

    public function getTemplateName()
    {
        return "menu/homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 25,  85 => 18,  75 => 11,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'homepage' %}

{% block main %}
    <h1> Bienvenue sur le menu Degreaux</h1>
    <!-- choix 3 btn -->
    <div class=\"row categories\">
      <p> Vous êtes : </p>
      <div class=\"col-md-4 col-xs-12\">
          <a href=\"{{ path('externe') }}\" >
            <button id=\"externebtn\" type=\"button\" class=\"btn btn-default\" >
                Externe
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a  href=\"{{ path('scolaire') }}\">
            <button id=\"scolairebtn\"  type=\"button\" class=\"btn btn-default \">
                Scolaire
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a href=\"{{ path('resident') }}\" >
            <button id=\"residentbtn\"   type=\"button\" class=\"btn btn-default \">
                Résident
            </button>
        </a>
      </div>
    </div>
{% endblock %}
", "menu/homepage.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\menu\\homepage.html.twig");
    }
}
