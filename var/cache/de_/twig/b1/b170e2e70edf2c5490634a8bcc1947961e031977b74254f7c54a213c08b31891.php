<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_2ae06d326fae8cfa051b284c5486baf8fedd119a523e2aec5a452009467e720c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_22d4dcccb8c89d72e683ab81176a27b9a3bcc39fcbca81e4053ed53e2727838f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22d4dcccb8c89d72e683ab81176a27b9a3bcc39fcbca81e4053ed53e2727838f->enter($__internal_22d4dcccb8c89d72e683ab81176a27b9a3bcc39fcbca81e4053ed53e2727838f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $__internal_091639ed118c7ebe48bd3d3f2f7d2abf777b6e67e15aae8ec5a7005006a0fafa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_091639ed118c7ebe48bd3d3f2f7d2abf777b6e67e15aae8ec5a7005006a0fafa->enter($__internal_091639ed118c7ebe48bd3d3f2f7d2abf777b6e67e15aae8ec5a7005006a0fafa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_22d4dcccb8c89d72e683ab81176a27b9a3bcc39fcbca81e4053ed53e2727838f->leave($__internal_22d4dcccb8c89d72e683ab81176a27b9a3bcc39fcbca81e4053ed53e2727838f_prof);

        
        $__internal_091639ed118c7ebe48bd3d3f2f7d2abf777b6e67e15aae8ec5a7005006a0fafa->leave($__internal_091639ed118c7ebe48bd3d3f2f7d2abf777b6e67e15aae8ec5a7005006a0fafa_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_661eeba0c62740e307b996576278105a52474eea15033012d867d78672064750 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_661eeba0c62740e307b996576278105a52474eea15033012d867d78672064750->enter($__internal_661eeba0c62740e307b996576278105a52474eea15033012d867d78672064750_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_6d6701107f7b28bb00930af79629d0373efa60d0b7886a46a2da12a149fc7578 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d6701107f7b28bb00930af79629d0373efa60d0b7886a46a2da12a149fc7578->enter($__internal_6d6701107f7b28bb00930af79629d0373efa60d0b7886a46a2da12a149fc7578_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_6d6701107f7b28bb00930af79629d0373efa60d0b7886a46a2da12a149fc7578->leave($__internal_6d6701107f7b28bb00930af79629d0373efa60d0b7886a46a2da12a149fc7578_prof);

        
        $__internal_661eeba0c62740e307b996576278105a52474eea15033012d867d78672064750->leave($__internal_661eeba0c62740e307b996576278105a52474eea15033012d867d78672064750_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Resetting/request.html.twig");
    }
}
