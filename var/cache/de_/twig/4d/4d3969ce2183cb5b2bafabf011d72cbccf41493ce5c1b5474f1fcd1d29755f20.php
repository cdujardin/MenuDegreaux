<?php

/* TwigBundle:Exception:error404.html.twig */
class __TwigTemplate_9f6b530fefec5bf3d4aa17a58f0cc0099d9b9c425688fd0e73cf2a296457803c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 11
        $this->parent = $this->loadTemplate("base.html.twig", "TwigBundle:Exception:error404.html.twig", 11);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c5f4cbb109261e74b135b4b3b57f6b0ca5d33d37f7e2d8c5fd07f9e6930765e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c5f4cbb109261e74b135b4b3b57f6b0ca5d33d37f7e2d8c5fd07f9e6930765e6->enter($__internal_c5f4cbb109261e74b135b4b3b57f6b0ca5d33d37f7e2d8c5fd07f9e6930765e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error404.html.twig"));

        $__internal_df1c30093fe759100a8c782799c1b88c010c037d0f62dfe4c511e259fd91a2f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_df1c30093fe759100a8c782799c1b88c010c037d0f62dfe4c511e259fd91a2f6->enter($__internal_df1c30093fe759100a8c782799c1b88c010c037d0f62dfe4c511e259fd91a2f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error404.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c5f4cbb109261e74b135b4b3b57f6b0ca5d33d37f7e2d8c5fd07f9e6930765e6->leave($__internal_c5f4cbb109261e74b135b4b3b57f6b0ca5d33d37f7e2d8c5fd07f9e6930765e6_prof);

        
        $__internal_df1c30093fe759100a8c782799c1b88c010c037d0f62dfe4c511e259fd91a2f6->leave($__internal_df1c30093fe759100a8c782799c1b88c010c037d0f62dfe4c511e259fd91a2f6_prof);

    }

    // line 13
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_580b6e676746eebf1af9b55ea6a15674a9e1c4b123765aadf3bda3bf49b62705 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_580b6e676746eebf1af9b55ea6a15674a9e1c4b123765aadf3bda3bf49b62705->enter($__internal_580b6e676746eebf1af9b55ea6a15674a9e1c4b123765aadf3bda3bf49b62705_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_8389639e2ed5973829e813ea25256a0e492f5dfd19001635ab6e39e9bd723aa7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8389639e2ed5973829e813ea25256a0e492f5dfd19001635ab6e39e9bd723aa7->enter($__internal_8389639e2ed5973829e813ea25256a0e492f5dfd19001635ab6e39e9bd723aa7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "error";
        
        $__internal_8389639e2ed5973829e813ea25256a0e492f5dfd19001635ab6e39e9bd723aa7->leave($__internal_8389639e2ed5973829e813ea25256a0e492f5dfd19001635ab6e39e9bd723aa7_prof);

        
        $__internal_580b6e676746eebf1af9b55ea6a15674a9e1c4b123765aadf3bda3bf49b62705->leave($__internal_580b6e676746eebf1af9b55ea6a15674a9e1c4b123765aadf3bda3bf49b62705_prof);

    }

    // line 15
    public function block_main($context, array $blocks = array())
    {
        $__internal_06664a635437dce3e7cf79677b7ac9999054c20b65c311dc485cfdf7cd1c9134 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_06664a635437dce3e7cf79677b7ac9999054c20b65c311dc485cfdf7cd1c9134->enter($__internal_06664a635437dce3e7cf79677b7ac9999054c20b65c311dc485cfdf7cd1c9134_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_6706bc48bef8c6c05d4fb427bb125cc7cf3f6a804cb69a9222bfc3d82570c387 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6706bc48bef8c6c05d4fb427bb125cc7cf3f6a804cb69a9222bfc3d82570c387->enter($__internal_6706bc48bef8c6c05d4fb427bb125cc7cf3f6a804cb69a9222bfc3d82570c387_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 16
        echo "    <h1 class=\"text-danger\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error.name", array("%status_code%" => 404)), "html", null, true);
        echo "</h1>

    <p class=\"lead\">
        ";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error_404.description"), "html", null, true);
        echo "
    </p>
    <p>
        ";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error_404.suggestion", array("%url%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_index")));
        echo "
    </p>
";
        
        $__internal_6706bc48bef8c6c05d4fb427bb125cc7cf3f6a804cb69a9222bfc3d82570c387->leave($__internal_6706bc48bef8c6c05d4fb427bb125cc7cf3f6a804cb69a9222bfc3d82570c387_prof);

        
        $__internal_06664a635437dce3e7cf79677b7ac9999054c20b65c311dc485cfdf7cd1c9134->leave($__internal_06664a635437dce3e7cf79677b7ac9999054c20b65c311dc485cfdf7cd1c9134_prof);

    }

    // line 26
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_87ec8c734cb4ab7578e00ab07b687a18ea44a4889ab6583281437073580ad6ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87ec8c734cb4ab7578e00ab07b687a18ea44a4889ab6583281437073580ad6ad->enter($__internal_87ec8c734cb4ab7578e00ab07b687a18ea44a4889ab6583281437073580ad6ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_d618b8005a222ce2412eb95e89ea13c4fb1b9df1b15a50b465a4497efb6324a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d618b8005a222ce2412eb95e89ea13c4fb1b9df1b15a50b465a4497efb6324a7->enter($__internal_d618b8005a222ce2412eb95e89ea13c4fb1b9df1b15a50b465a4497efb6324a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 27
        echo "    ";
        $this->displayParentBlock("sidebar", $context, $blocks);
        echo "

    ";
        // line 29
        echo $this->env->getExtension('CodeExplorerBundle\Twig\SourceCodeExtension')->showSourceCode($this->env, $this);
        echo "
";
        
        $__internal_d618b8005a222ce2412eb95e89ea13c4fb1b9df1b15a50b465a4497efb6324a7->leave($__internal_d618b8005a222ce2412eb95e89ea13c4fb1b9df1b15a50b465a4497efb6324a7_prof);

        
        $__internal_87ec8c734cb4ab7578e00ab07b687a18ea44a4889ab6583281437073580ad6ad->leave($__internal_87ec8c734cb4ab7578e00ab07b687a18ea44a4889ab6583281437073580ad6ad_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error404.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 29,  104 => 27,  95 => 26,  82 => 22,  76 => 19,  69 => 16,  60 => 15,  42 => 13,  11 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
    This template is used to render errors of type HTTP 404 (Not Found)

    This is the simplest way to customize error pages in Symfony applications.
    In case you need it, you can also hook into the internal exception handling
    made by Symfony. This allows you to perform advanced tasks and even recover
    your application from some errors.
    See http://symfony.com/doc/current/cookbook/controller/error_pages.html
#}

{% extends 'base.html.twig' %}

{% block body_id 'error' %}

{% block main %}
    <h1 class=\"text-danger\">{{ 'http_error.name'|trans({ '%status_code%': 404 }) }}</h1>

    <p class=\"lead\">
        {{ 'http_error_404.description'|trans }}
    </p>
    <p>
        {{ 'http_error_404.suggestion'|trans({ '%url%': path('blog_index') })|raw }}
    </p>
{% endblock %}

{% block sidebar %}
    {{ parent() }}

    {{ show_source_code(_self) }}
{% endblock %}
", "TwigBundle:Exception:error404.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources/TwigBundle/views/Exception/error404.html.twig");
    }
}
