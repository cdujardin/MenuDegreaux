<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_48e19b4a52ae2c62f4dcf2a03ac911c8ce85c7ea1d59e3cc49cbb3d6bcb7edfa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c8bfe98bd3baa806c487c2c78a0d82c4491fb89be158307622e604599be8516 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c8bfe98bd3baa806c487c2c78a0d82c4491fb89be158307622e604599be8516->enter($__internal_0c8bfe98bd3baa806c487c2c78a0d82c4491fb89be158307622e604599be8516_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_5fd61b8ab73fbccce969885b18d231b173373f101b48bde911d36a00ee26b02d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5fd61b8ab73fbccce969885b18d231b173373f101b48bde911d36a00ee26b02d->enter($__internal_5fd61b8ab73fbccce969885b18d231b173373f101b48bde911d36a00ee26b02d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_0c8bfe98bd3baa806c487c2c78a0d82c4491fb89be158307622e604599be8516->leave($__internal_0c8bfe98bd3baa806c487c2c78a0d82c4491fb89be158307622e604599be8516_prof);

        
        $__internal_5fd61b8ab73fbccce969885b18d231b173373f101b48bde911d36a00ee26b02d->leave($__internal_5fd61b8ab73fbccce969885b18d231b173373f101b48bde911d36a00ee26b02d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\hidden_row.html.php");
    }
}
