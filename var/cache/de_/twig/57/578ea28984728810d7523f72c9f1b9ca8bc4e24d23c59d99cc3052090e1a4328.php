<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_e7d0a3c7bbeba9643cab6a3d3896120acba22f895b4d14a85e23c1565a39b257 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_59ccbf4c635ce61f36b327b900f17d866a34646447004b5fac89e37e0d93ed1c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_59ccbf4c635ce61f36b327b900f17d866a34646447004b5fac89e37e0d93ed1c->enter($__internal_59ccbf4c635ce61f36b327b900f17d866a34646447004b5fac89e37e0d93ed1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_fd8a4cef489b2cb1d7a6680b172df47aa0009dd3cdc968d0a21aab5c02abb0bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd8a4cef489b2cb1d7a6680b172df47aa0009dd3cdc968d0a21aab5c02abb0bc->enter($__internal_fd8a4cef489b2cb1d7a6680b172df47aa0009dd3cdc968d0a21aab5c02abb0bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_59ccbf4c635ce61f36b327b900f17d866a34646447004b5fac89e37e0d93ed1c->leave($__internal_59ccbf4c635ce61f36b327b900f17d866a34646447004b5fac89e37e0d93ed1c_prof);

        
        $__internal_fd8a4cef489b2cb1d7a6680b172df47aa0009dd3cdc968d0a21aab5c02abb0bc->leave($__internal_fd8a4cef489b2cb1d7a6680b172df47aa0009dd3cdc968d0a21aab5c02abb0bc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_widget.html.php");
    }
}
