<?php

/* SonataBlockBundle:Block:block_core_text.html.twig */
class __TwigTemplate_0d81f1d79b2e4a4fccab4268f3f40b7807d8a2bbcdb4cd2945ba0187de5c90b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_core_text.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fb011b311442fef3af44b1cba92e16e876444f6cc9ff17fcc190180c440e8afc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb011b311442fef3af44b1cba92e16e876444f6cc9ff17fcc190180c440e8afc->enter($__internal_fb011b311442fef3af44b1cba92e16e876444f6cc9ff17fcc190180c440e8afc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_text.html.twig"));

        $__internal_64b9ec1f2ddd8962f37d4fc420d126bcb58609172d41cc51af687e56d20ac43c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64b9ec1f2ddd8962f37d4fc420d126bcb58609172d41cc51af687e56d20ac43c->enter($__internal_64b9ec1f2ddd8962f37d4fc420d126bcb58609172d41cc51af687e56d20ac43c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_text.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fb011b311442fef3af44b1cba92e16e876444f6cc9ff17fcc190180c440e8afc->leave($__internal_fb011b311442fef3af44b1cba92e16e876444f6cc9ff17fcc190180c440e8afc_prof);

        
        $__internal_64b9ec1f2ddd8962f37d4fc420d126bcb58609172d41cc51af687e56d20ac43c->leave($__internal_64b9ec1f2ddd8962f37d4fc420d126bcb58609172d41cc51af687e56d20ac43c_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_165003ae43a676d6342930ca390fa8fe8dc8c06186996c7657ab2857325e4f5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_165003ae43a676d6342930ca390fa8fe8dc8c06186996c7657ab2857325e4f5e->enter($__internal_165003ae43a676d6342930ca390fa8fe8dc8c06186996c7657ab2857325e4f5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_7a2bc2b5898e6b4471e63fe94314eff46d1fea67b0f3248c69faf85038c124ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a2bc2b5898e6b4471e63fe94314eff46d1fea67b0f3248c69faf85038c124ec->enter($__internal_7a2bc2b5898e6b4471e63fe94314eff46d1fea67b0f3248c69faf85038c124ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        echo $this->getAttribute((isset($context["settings"]) ? $context["settings"] : $this->getContext($context, "settings")), "content", array());
        echo "
";
        
        $__internal_7a2bc2b5898e6b4471e63fe94314eff46d1fea67b0f3248c69faf85038c124ec->leave($__internal_7a2bc2b5898e6b4471e63fe94314eff46d1fea67b0f3248c69faf85038c124ec_prof);

        
        $__internal_165003ae43a676d6342930ca390fa8fe8dc8c06186996c7657ab2857325e4f5e->leave($__internal_165003ae43a676d6342930ca390fa8fe8dc8c06186996c7657ab2857325e4f5e_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_core_text.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    {{ settings.content|raw }}
{% endblock %}
", "SonataBlockBundle:Block:block_core_text.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle/Resources/views/Block/block_core_text.html.twig");
    }
}
