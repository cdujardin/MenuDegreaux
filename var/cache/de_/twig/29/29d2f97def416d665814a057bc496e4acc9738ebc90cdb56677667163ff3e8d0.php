<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_42483e43bf362b62fc5a5b7904831baa7f1d28a34a8bbe69fdaa898ff2376a9b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_af8fc880fcc06ee1893288377d271b8ad35ec3f8ef67901c915d39574342a6e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af8fc880fcc06ee1893288377d271b8ad35ec3f8ef67901c915d39574342a6e6->enter($__internal_af8fc880fcc06ee1893288377d271b8ad35ec3f8ef67901c915d39574342a6e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $__internal_db3e46353c2d7c181777e676e2e470a56fe9e738ab7a269973448d96f0ef303a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db3e46353c2d7c181777e676e2e470a56fe9e738ab7a269973448d96f0ef303a->enter($__internal_db3e46353c2d7c181777e676e2e470a56fe9e738ab7a269973448d96f0ef303a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_af8fc880fcc06ee1893288377d271b8ad35ec3f8ef67901c915d39574342a6e6->leave($__internal_af8fc880fcc06ee1893288377d271b8ad35ec3f8ef67901c915d39574342a6e6_prof);

        
        $__internal_db3e46353c2d7c181777e676e2e470a56fe9e738ab7a269973448d96f0ef303a->leave($__internal_db3e46353c2d7c181777e676e2e470a56fe9e738ab7a269973448d96f0ef303a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_93fd9a51b4422bda9244580ac343e166b3f9d295b50d8a38db9d203aa6f161de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_93fd9a51b4422bda9244580ac343e166b3f9d295b50d8a38db9d203aa6f161de->enter($__internal_93fd9a51b4422bda9244580ac343e166b3f9d295b50d8a38db9d203aa6f161de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_8aa8db8d893da341375a6dc9f051bc150eecee956f56fadbd9bfa14488ea6db1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8aa8db8d893da341375a6dc9f051bc150eecee956f56fadbd9bfa14488ea6db1->enter($__internal_8aa8db8d893da341375a6dc9f051bc150eecee956f56fadbd9bfa14488ea6db1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_8aa8db8d893da341375a6dc9f051bc150eecee956f56fadbd9bfa14488ea6db1->leave($__internal_8aa8db8d893da341375a6dc9f051bc150eecee956f56fadbd9bfa14488ea6db1_prof);

        
        $__internal_93fd9a51b4422bda9244580ac343e166b3f9d295b50d8a38db9d203aa6f161de->leave($__internal_93fd9a51b4422bda9244580ac343e166b3f9d295b50d8a38db9d203aa6f161de_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}
