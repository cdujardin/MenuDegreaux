<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_9bf76e896f81021814c0396f5216382896325045fbbabed33aed94e265f8167a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4aab52127e6767e2d1f65b9850ac45e0fc597dc707c0a5fc5e3f8a429c41a22f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4aab52127e6767e2d1f65b9850ac45e0fc597dc707c0a5fc5e3f8a429c41a22f->enter($__internal_4aab52127e6767e2d1f65b9850ac45e0fc597dc707c0a5fc5e3f8a429c41a22f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_eeb8042bf8e6eb1a86f742af7207446e60f90f81c3bb3e2f6cf3514d4e1eaf2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eeb8042bf8e6eb1a86f742af7207446e60f90f81c3bb3e2f6cf3514d4e1eaf2d->enter($__internal_eeb8042bf8e6eb1a86f742af7207446e60f90f81c3bb3e2f6cf3514d4e1eaf2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_4aab52127e6767e2d1f65b9850ac45e0fc597dc707c0a5fc5e3f8a429c41a22f->leave($__internal_4aab52127e6767e2d1f65b9850ac45e0fc597dc707c0a5fc5e3f8a429c41a22f_prof);

        
        $__internal_eeb8042bf8e6eb1a86f742af7207446e60f90f81c3bb3e2f6cf3514d4e1eaf2d->leave($__internal_eeb8042bf8e6eb1a86f742af7207446e60f90f81c3bb3e2f6cf3514d4e1eaf2d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_rest.html.php");
    }
}
