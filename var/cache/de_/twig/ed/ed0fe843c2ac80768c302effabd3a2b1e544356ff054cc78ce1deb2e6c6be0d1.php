<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_348c9ce917f8836d0adc77db66e17c3db7136e1b00891222008d771431e6ba9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b7c3fe87bef6a3b6307cd1abe3ccc1f0368cc1005102ca71a4786a6a67646b33 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b7c3fe87bef6a3b6307cd1abe3ccc1f0368cc1005102ca71a4786a6a67646b33->enter($__internal_b7c3fe87bef6a3b6307cd1abe3ccc1f0368cc1005102ca71a4786a6a67646b33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        $__internal_71e140e0c406d3c59e0de71f9d518473c8cf1f82af8d29a39283c35f50319ec1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_71e140e0c406d3c59e0de71f9d518473c8cf1f82af8d29a39283c35f50319ec1->enter($__internal_71e140e0c406d3c59e0de71f9d518473c8cf1f82af8d29a39283c35f50319ec1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_b7c3fe87bef6a3b6307cd1abe3ccc1f0368cc1005102ca71a4786a6a67646b33->leave($__internal_b7c3fe87bef6a3b6307cd1abe3ccc1f0368cc1005102ca71a4786a6a67646b33_prof);

        
        $__internal_71e140e0c406d3c59e0de71f9d518473c8cf1f82af8d29a39283c35f50319ec1->leave($__internal_71e140e0c406d3c59e0de71f9d518473c8cf1f82af8d29a39283c35f50319ec1_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.js.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.js.twig");
    }
}
