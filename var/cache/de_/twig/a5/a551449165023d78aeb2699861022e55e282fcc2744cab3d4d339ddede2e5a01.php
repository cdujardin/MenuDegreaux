<?php

/* SonataAdminBundle:CRUD:base_show_field.html.twig */
class __TwigTemplate_d382c4277755d5d68ac08b08d8244508a2abad9132397d9ad692bb0e09dd1411 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'name' => array($this, 'block_name'),
            'field' => array($this, 'block_field'),
            'field_value' => array($this, 'block_field_value'),
            'field_compare' => array($this, 'block_field_compare'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc542bf4fc7d1a2799d75f9db4e112d198af05fa8cb0d0951493f0dd4820491e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc542bf4fc7d1a2799d75f9db4e112d198af05fa8cb0d0951493f0dd4820491e->enter($__internal_cc542bf4fc7d1a2799d75f9db4e112d198af05fa8cb0d0951493f0dd4820491e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_show_field.html.twig"));

        $__internal_d61fdcf8947039faff03c08b8c7566f944ec771b238ce9884df71ba023af99d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d61fdcf8947039faff03c08b8c7566f944ec771b238ce9884df71ba023af99d3->enter($__internal_d61fdcf8947039faff03c08b8c7566f944ec771b238ce9884df71ba023af99d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_show_field.html.twig"));

        // line 11
        echo "
<th";
        // line 12
        if (((array_key_exists("is_diff", $context)) ? (_twig_default_filter((isset($context["is_diff"]) ? $context["is_diff"] : $this->getContext($context, "is_diff")), false)) : (false))) {
            echo " class=\"diff\"";
        }
        echo ">";
        $this->displayBlock('name', $context, $blocks);
        echo "</th>
<td>";
        // line 14
        $this->displayBlock('field', $context, $blocks);
        // line 31
        echo "</td>

";
        // line 33
        $this->displayBlock('field_compare', $context, $blocks);
        
        $__internal_cc542bf4fc7d1a2799d75f9db4e112d198af05fa8cb0d0951493f0dd4820491e->leave($__internal_cc542bf4fc7d1a2799d75f9db4e112d198af05fa8cb0d0951493f0dd4820491e_prof);

        
        $__internal_d61fdcf8947039faff03c08b8c7566f944ec771b238ce9884df71ba023af99d3->leave($__internal_d61fdcf8947039faff03c08b8c7566f944ec771b238ce9884df71ba023af99d3_prof);

    }

    // line 12
    public function block_name($context, array $blocks = array())
    {
        $__internal_59396742f53113f19583b6633787f37e6e54c47d9210a3f7b7bad88ab38f71ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_59396742f53113f19583b6633787f37e6e54c47d9210a3f7b7bad88ab38f71ae->enter($__internal_59396742f53113f19583b6633787f37e6e54c47d9210a3f7b7bad88ab38f71ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "name"));

        $__internal_68b9c788b7dff86eb0558a9a9a6fc2408f652a2d10fdc99dc026e884c4eaea4a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_68b9c788b7dff86eb0558a9a9a6fc2408f652a2d10fdc99dc026e884c4eaea4a->enter($__internal_68b9c788b7dff86eb0558a9a9a6fc2408f652a2d10fdc99dc026e884c4eaea4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "name"));

        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "label", array()), array(), (($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "translationDomain", array())) ? ($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "translationDomain", array())) : ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "translationDomain", array())))), "html", null, true);
        
        $__internal_68b9c788b7dff86eb0558a9a9a6fc2408f652a2d10fdc99dc026e884c4eaea4a->leave($__internal_68b9c788b7dff86eb0558a9a9a6fc2408f652a2d10fdc99dc026e884c4eaea4a_prof);

        
        $__internal_59396742f53113f19583b6633787f37e6e54c47d9210a3f7b7bad88ab38f71ae->leave($__internal_59396742f53113f19583b6633787f37e6e54c47d9210a3f7b7bad88ab38f71ae_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_3a019f501801c03942c94c18843a780ade4a32e97be22881bf99839e259ed1e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a019f501801c03942c94c18843a780ade4a32e97be22881bf99839e259ed1e8->enter($__internal_3a019f501801c03942c94c18843a780ade4a32e97be22881bf99839e259ed1e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_689bf37e25883c170091bbb5752db5ed220768870eeb581f87125cf17497a6da = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_689bf37e25883c170091bbb5752db5ed220768870eeb581f87125cf17497a6da->enter($__internal_689bf37e25883c170091bbb5752db5ed220768870eeb581f87125cf17497a6da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        ob_start();
        // line 16
        echo "            ";
        $context["collapse"] = (($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "collapse", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "collapse", array()), false)) : (false));
        // line 17
        echo "            ";
        if ((isset($context["collapse"]) ? $context["collapse"] : $this->getContext($context, "collapse"))) {
            // line 18
            echo "                <div class=\"sonata-readmore\"
                      data-readmore-height=\"";
            // line 19
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["collapse"]) ? $context["collapse"] : null), "height", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["collapse"]) ? $context["collapse"] : null), "height", array()), 40)) : (40)), "html", null, true);
            echo "\"
                      data-readmore-more=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((($this->getAttribute((isset($context["collapse"]) ? $context["collapse"] : null), "more", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["collapse"]) ? $context["collapse"] : null), "more", array()), "read_more")) : ("read_more")), array(), "SonataAdminBundle"), "html", null, true);
            echo "\"
                      data-readmore-less=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((($this->getAttribute((isset($context["collapse"]) ? $context["collapse"] : null), "less", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["collapse"]) ? $context["collapse"] : null), "less", array()), "read_less")) : ("read_less")), array(), "SonataAdminBundle"), "html", null, true);
            echo "\">
                    ";
            // line 22
            $this->displayBlock('field_value', $context, $blocks);
            // line 25
            echo "                </div>
            ";
        } else {
            // line 27
            echo "                ";
            $this->displayBlock("field_value", $context, $blocks);
            echo "
            ";
        }
        // line 29
        echo "        ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_689bf37e25883c170091bbb5752db5ed220768870eeb581f87125cf17497a6da->leave($__internal_689bf37e25883c170091bbb5752db5ed220768870eeb581f87125cf17497a6da_prof);

        
        $__internal_3a019f501801c03942c94c18843a780ade4a32e97be22881bf99839e259ed1e8->leave($__internal_3a019f501801c03942c94c18843a780ade4a32e97be22881bf99839e259ed1e8_prof);

    }

    // line 22
    public function block_field_value($context, array $blocks = array())
    {
        $__internal_c0436698abc5b25e6391ebc1d3bbcea44392accd2ce575f2a45f32947a17efbc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c0436698abc5b25e6391ebc1d3bbcea44392accd2ce575f2a45f32947a17efbc->enter($__internal_c0436698abc5b25e6391ebc1d3bbcea44392accd2ce575f2a45f32947a17efbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_value"));

        $__internal_5285605981c022d14cb52aaae79c7f7999d1590c5acf64aa39976e92bfdb0634 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5285605981c022d14cb52aaae79c7f7999d1590c5acf64aa39976e92bfdb0634->enter($__internal_5285605981c022d14cb52aaae79c7f7999d1590c5acf64aa39976e92bfdb0634_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_value"));

        // line 23
        echo "                        ";
        if ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "safe", array())) {
            echo (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"));
        } else {
            echo nl2br(twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true));
        }
        // line 24
        echo "                    ";
        
        $__internal_5285605981c022d14cb52aaae79c7f7999d1590c5acf64aa39976e92bfdb0634->leave($__internal_5285605981c022d14cb52aaae79c7f7999d1590c5acf64aa39976e92bfdb0634_prof);

        
        $__internal_c0436698abc5b25e6391ebc1d3bbcea44392accd2ce575f2a45f32947a17efbc->leave($__internal_c0436698abc5b25e6391ebc1d3bbcea44392accd2ce575f2a45f32947a17efbc_prof);

    }

    // line 33
    public function block_field_compare($context, array $blocks = array())
    {
        $__internal_a7468d3b539eda999ead35ffeb9a7cd70cde8483b5d792fef47d53ad7d327b8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a7468d3b539eda999ead35ffeb9a7cd70cde8483b5d792fef47d53ad7d327b8c->enter($__internal_a7468d3b539eda999ead35ffeb9a7cd70cde8483b5d792fef47d53ad7d327b8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_compare"));

        $__internal_b742b95bd46f5ccf62de7935040c89b4a5c13eb44d896bbce1a0180eb7f718f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b742b95bd46f5ccf62de7935040c89b4a5c13eb44d896bbce1a0180eb7f718f0->enter($__internal_b742b95bd46f5ccf62de7935040c89b4a5c13eb44d896bbce1a0180eb7f718f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_compare"));

        // line 34
        echo "    ";
        if (array_key_exists("value_compare", $context)) {
            // line 35
            echo "        <td>
            ";
            // line 36
            $context["value"] = (isset($context["value_compare"]) ? $context["value_compare"] : $this->getContext($context, "value_compare"));
            // line 37
            echo "            ";
            $this->displayBlock("field", $context, $blocks);
            echo "
        </td>
    ";
        }
        
        $__internal_b742b95bd46f5ccf62de7935040c89b4a5c13eb44d896bbce1a0180eb7f718f0->leave($__internal_b742b95bd46f5ccf62de7935040c89b4a5c13eb44d896bbce1a0180eb7f718f0_prof);

        
        $__internal_a7468d3b539eda999ead35ffeb9a7cd70cde8483b5d792fef47d53ad7d327b8c->leave($__internal_a7468d3b539eda999ead35ffeb9a7cd70cde8483b5d792fef47d53ad7d327b8c_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 37,  170 => 36,  167 => 35,  164 => 34,  155 => 33,  145 => 24,  138 => 23,  129 => 22,  118 => 29,  112 => 27,  108 => 25,  106 => 22,  102 => 21,  98 => 20,  94 => 19,  91 => 18,  88 => 17,  85 => 16,  83 => 15,  74 => 14,  56 => 12,  46 => 33,  42 => 31,  40 => 14,  32 => 12,  29 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<th{% if(is_diff|default(false)) %} class=\"diff\"{% endif %}>{% block name %}{{ field_description.label|trans({}, field_description.translationDomain ?: admin.translationDomain) }}{% endblock %}</th>
<td>
    {%- block field -%}
        {% spaceless %}
            {% set collapse = field_description.options.collapse|default(false) %}
            {% if collapse %}
                <div class=\"sonata-readmore\"
                      data-readmore-height=\"{{ collapse.height|default(40) }}\"
                      data-readmore-more=\"{{ collapse.more|default('read_more')|trans({}, 'SonataAdminBundle') }}\"
                      data-readmore-less=\"{{ collapse.less|default('read_less')|trans({}, 'SonataAdminBundle') }}\">
                    {% block field_value %}
                        {% if field_description.options.safe %}{{ value|raw }}{% else %}{{ value|nl2br }}{% endif %}
                    {% endblock %}
                </div>
            {% else %}
                {{ block('field_value') }}
            {% endif %}
        {% endspaceless %}
    {%- endblock -%}
</td>

{% block field_compare %}
    {% if(value_compare is defined) %}
        <td>
            {% set value = value_compare %}
            {{ block('field') }}
        </td>
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:base_show_field.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/base_show_field.html.twig");
    }
}
