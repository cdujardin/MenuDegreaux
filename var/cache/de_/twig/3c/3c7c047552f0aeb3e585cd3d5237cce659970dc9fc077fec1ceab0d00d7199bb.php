<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_08413f6e0e4addd06eef16b228e8142a0d46ee4435c64756298cb57a8dfe4251 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04a43ba85624ae8ae9b7e6b219519399d80af3f48ab4af623f8f0f42dd7fb8aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_04a43ba85624ae8ae9b7e6b219519399d80af3f48ab4af623f8f0f42dd7fb8aa->enter($__internal_04a43ba85624ae8ae9b7e6b219519399d80af3f48ab4af623f8f0f42dd7fb8aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_37bc107f08776858839cbf54e074cc53e8f7c8a24ab4a4b24fd73d4edac2c2ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37bc107f08776858839cbf54e074cc53e8f7c8a24ab4a4b24fd73d4edac2c2ea->enter($__internal_37bc107f08776858839cbf54e074cc53e8f7c8a24ab4a4b24fd73d4edac2c2ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_04a43ba85624ae8ae9b7e6b219519399d80af3f48ab4af623f8f0f42dd7fb8aa->leave($__internal_04a43ba85624ae8ae9b7e6b219519399d80af3f48ab4af623f8f0f42dd7fb8aa_prof);

        
        $__internal_37bc107f08776858839cbf54e074cc53e8f7c8a24ab4a4b24fd73d4edac2c2ea->leave($__internal_37bc107f08776858839cbf54e074cc53e8f7c8a24ab4a4b24fd73d4edac2c2ea_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_cd4578a353f5908b5e1401e3764d21b95fb30852e1c766fdd1f950642b678e30 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd4578a353f5908b5e1401e3764d21b95fb30852e1c766fdd1f950642b678e30->enter($__internal_cd4578a353f5908b5e1401e3764d21b95fb30852e1c766fdd1f950642b678e30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_5d73844b299c0aa050dff9bc89c466d4ffad906438c58b3099f5fb9fe07d13f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d73844b299c0aa050dff9bc89c466d4ffad906438c58b3099f5fb9fe07d13f0->enter($__internal_5d73844b299c0aa050dff9bc89c466d4ffad906438c58b3099f5fb9fe07d13f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_5d73844b299c0aa050dff9bc89c466d4ffad906438c58b3099f5fb9fe07d13f0->leave($__internal_5d73844b299c0aa050dff9bc89c466d4ffad906438c58b3099f5fb9fe07d13f0_prof);

        
        $__internal_cd4578a353f5908b5e1401e3764d21b95fb30852e1c766fdd1f950642b678e30->leave($__internal_cd4578a353f5908b5e1401e3764d21b95fb30852e1c766fdd1f950642b678e30_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_101997ca0f66f6c912ee993e3c0339ca793c83b4347d34d3fc350f1eb082b864 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_101997ca0f66f6c912ee993e3c0339ca793c83b4347d34d3fc350f1eb082b864->enter($__internal_101997ca0f66f6c912ee993e3c0339ca793c83b4347d34d3fc350f1eb082b864_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3790cfaa0ead897a90fe83b4551c5d662484a27fe4e8e5758ad2c95078b84ff9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3790cfaa0ead897a90fe83b4551c5d662484a27fe4e8e5758ad2c95078b84ff9->enter($__internal_3790cfaa0ead897a90fe83b4551c5d662484a27fe4e8e5758ad2c95078b84ff9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["file"]) ? $context["file"] : $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, (isset($context["line"]) ? $context["line"] : $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) ? $context["filename"] : $this->getContext($context, "filename")), (isset($context["line"]) ? $context["line"] : $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_3790cfaa0ead897a90fe83b4551c5d662484a27fe4e8e5758ad2c95078b84ff9->leave($__internal_3790cfaa0ead897a90fe83b4551c5d662484a27fe4e8e5758ad2c95078b84ff9_prof);

        
        $__internal_101997ca0f66f6c912ee993e3c0339ca793c83b4347d34d3fc350f1eb082b864->leave($__internal_101997ca0f66f6c912ee993e3c0339ca793c83b4347d34d3fc350f1eb082b864_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
