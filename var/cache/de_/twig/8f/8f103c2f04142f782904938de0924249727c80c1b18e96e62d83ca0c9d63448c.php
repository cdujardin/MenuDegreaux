<?php

/* SonataAdminBundle:CRUD/Association:list_one_to_many.html.twig */
class __TwigTemplate_951da502a7e99820ec3cfabc93e6ad5f77fd8b38f241c2d2c46ebd19777243b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
            'relation_link' => array($this, 'block_relation_link'),
            'relation_value' => array($this, 'block_relation_value'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD/Association:list_one_to_many.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_70fd1788035bee31f4833354eb83d827a3127b99e721069443af514c61ae886e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_70fd1788035bee31f4833354eb83d827a3127b99e721069443af514c61ae886e->enter($__internal_70fd1788035bee31f4833354eb83d827a3127b99e721069443af514c61ae886e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:list_one_to_many.html.twig"));

        $__internal_5c672aef0c61cedb1084297bec6c75b86a9ca8286c76398d630339afdd9d1de7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c672aef0c61cedb1084297bec6c75b86a9ca8286c76398d630339afdd9d1de7->enter($__internal_5c672aef0c61cedb1084297bec6c75b86a9ca8286c76398d630339afdd9d1de7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:list_one_to_many.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_70fd1788035bee31f4833354eb83d827a3127b99e721069443af514c61ae886e->leave($__internal_70fd1788035bee31f4833354eb83d827a3127b99e721069443af514c61ae886e_prof);

        
        $__internal_5c672aef0c61cedb1084297bec6c75b86a9ca8286c76398d630339afdd9d1de7->leave($__internal_5c672aef0c61cedb1084297bec6c75b86a9ca8286c76398d630339afdd9d1de7_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_c39469901b69f79e1ed7bd4972efa3f74da5bff7a0c74cf4927e634d4cacf453 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c39469901b69f79e1ed7bd4972efa3f74da5bff7a0c74cf4927e634d4cacf453->enter($__internal_c39469901b69f79e1ed7bd4972efa3f74da5bff7a0c74cf4927e634d4cacf453_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_80f00f17c3c877f252bf721fed492769eda4010c76ac1e18d24754f00152c77e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80f00f17c3c877f252bf721fed492769eda4010c76ac1e18d24754f00152c77e->enter($__internal_80f00f17c3c877f252bf721fed492769eda4010c76ac1e18d24754f00152c77e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["route_name"] = $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "route", array()), "name", array());
        // line 16
        echo "    ";
        if (($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "hasassociationadmin", array()) && $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "associationadmin", array()), "hasRoute", array(0 => (isset($context["route_name"]) ? $context["route_name"] : $this->getContext($context, "route_name"))), "method"))) {
            // line 17
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 18
                if ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "associationadmin", array()), "hasAccess", array(0 => (isset($context["route_name"]) ? $context["route_name"] : $this->getContext($context, "route_name")), 1 => $context["element"]), "method")) {
                    // line 19
                    $this->displayBlock("relation_link", $context, $blocks);
                } else {
                    // line 21
                    $this->displayBlock("relation_value", $context, $blocks);
                }
                // line 23
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 24
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "    ";
        } else {
            // line 26
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 27
                echo "            ";
                $this->displayBlock("relation_value", $context, $blocks);
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 28
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "    ";
        }
        
        $__internal_80f00f17c3c877f252bf721fed492769eda4010c76ac1e18d24754f00152c77e->leave($__internal_80f00f17c3c877f252bf721fed492769eda4010c76ac1e18d24754f00152c77e_prof);

        
        $__internal_c39469901b69f79e1ed7bd4972efa3f74da5bff7a0c74cf4927e634d4cacf453->leave($__internal_c39469901b69f79e1ed7bd4972efa3f74da5bff7a0c74cf4927e634d4cacf453_prof);

    }

    // line 32
    public function block_relation_link($context, array $blocks = array())
    {
        $__internal_ee38df2dceb5db3f36a6d83f73eb71dfeec21c453516e5aa1d0e9052863779ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee38df2dceb5db3f36a6d83f73eb71dfeec21c453516e5aa1d0e9052863779ce->enter($__internal_ee38df2dceb5db3f36a6d83f73eb71dfeec21c453516e5aa1d0e9052863779ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "relation_link"));

        $__internal_356eb5f6adcf60331f0670dc8ff691d4deee8d3b33c15a9bf055bdc824a5304f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_356eb5f6adcf60331f0670dc8ff691d4deee8d3b33c15a9bf055bdc824a5304f->enter($__internal_356eb5f6adcf60331f0670dc8ff691d4deee8d3b33c15a9bf055bdc824a5304f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "relation_link"));

        // line 33
        echo "<a href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["route_name"]) ? $context["route_name"] : $this->getContext($context, "route_name")), 1 => (isset($context["element"]) ? $context["element"] : $this->getContext($context, "element")), 2 => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
        echo "\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) ? $context["element"] : $this->getContext($context, "element")), (isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description"))), "html", null, true);
        // line 35
        echo "</a>";
        
        $__internal_356eb5f6adcf60331f0670dc8ff691d4deee8d3b33c15a9bf055bdc824a5304f->leave($__internal_356eb5f6adcf60331f0670dc8ff691d4deee8d3b33c15a9bf055bdc824a5304f_prof);

        
        $__internal_ee38df2dceb5db3f36a6d83f73eb71dfeec21c453516e5aa1d0e9052863779ce->leave($__internal_ee38df2dceb5db3f36a6d83f73eb71dfeec21c453516e5aa1d0e9052863779ce_prof);

    }

    // line 38
    public function block_relation_value($context, array $blocks = array())
    {
        $__internal_ff022ff58c3b93c9a187a67e902148211a84d87adaf0f0e44dc5315c1ad778f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff022ff58c3b93c9a187a67e902148211a84d87adaf0f0e44dc5315c1ad778f4->enter($__internal_ff022ff58c3b93c9a187a67e902148211a84d87adaf0f0e44dc5315c1ad778f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "relation_value"));

        $__internal_faa03aa6113aee19f34911b4e30d1ade836a17178e8e0f97285a745e97d8df4a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_faa03aa6113aee19f34911b4e30d1ade836a17178e8e0f97285a745e97d8df4a->enter($__internal_faa03aa6113aee19f34911b4e30d1ade836a17178e8e0f97285a745e97d8df4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "relation_value"));

        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) ? $context["element"] : $this->getContext($context, "element")), (isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description"))), "html", null, true);
        
        $__internal_faa03aa6113aee19f34911b4e30d1ade836a17178e8e0f97285a745e97d8df4a->leave($__internal_faa03aa6113aee19f34911b4e30d1ade836a17178e8e0f97285a745e97d8df4a_prof);

        
        $__internal_ff022ff58c3b93c9a187a67e902148211a84d87adaf0f0e44dc5315c1ad778f4->leave($__internal_ff022ff58c3b93c9a187a67e902148211a84d87adaf0f0e44dc5315c1ad778f4_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:list_one_to_many.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 39,  177 => 38,  167 => 35,  165 => 34,  161 => 33,  152 => 32,  141 => 29,  127 => 28,  121 => 27,  103 => 26,  100 => 25,  86 => 24,  82 => 23,  79 => 21,  76 => 19,  74 => 18,  56 => 17,  53 => 16,  50 => 15,  41 => 14,  20 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {% set route_name = field_description.options.route.name %}
    {% if field_description.hasassociationadmin and field_description.associationadmin.hasRoute(route_name) %}
        {% for element in value %}
            {%- if field_description.associationadmin.hasAccess(route_name, element) -%}
                {{ block('relation_link') }}
            {%- else -%}
                {{ block('relation_value') }}
            {%- endif -%}
            {% if not loop.last %}, {% endif %}
        {% endfor %}
    {% else %}
        {% for element in value %}
            {{ block('relation_value') }}{% if not loop.last %}, {% endif %}
        {% endfor %}
    {% endif %}
{% endblock %}

{%- block relation_link -%}
    <a href=\"{{ field_description.associationadmin.generateObjectUrl(route_name, element, field_description.options.route.parameters) }}\">
        {{- element|render_relation_element(field_description) -}}
    </a>
{%- endblock -%}

{%- block relation_value -%}
    {{- element|render_relation_element(field_description) -}}
{%- endblock -%}
", "SonataAdminBundle:CRUD/Association:list_one_to_many.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/Association/list_one_to_many.html.twig");
    }
}
