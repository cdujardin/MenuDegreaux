<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_20682997647b1759eba0dda02c830f1af1bd04b57608163943c1e5aeb01ee732 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_78caab92f96206d621df0395db10aabf31b660c35dc05e271e2388315945f052 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_78caab92f96206d621df0395db10aabf31b660c35dc05e271e2388315945f052->enter($__internal_78caab92f96206d621df0395db10aabf31b660c35dc05e271e2388315945f052_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $__internal_09463178c4b84173417cf80c69e21d697977f06a479999ee3f34016bd26a4b4b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09463178c4b84173417cf80c69e21d697977f06a479999ee3f34016bd26a4b4b->enter($__internal_09463178c4b84173417cf80c69e21d697977f06a479999ee3f34016bd26a4b4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_78caab92f96206d621df0395db10aabf31b660c35dc05e271e2388315945f052->leave($__internal_78caab92f96206d621df0395db10aabf31b660c35dc05e271e2388315945f052_prof);

        
        $__internal_09463178c4b84173417cf80c69e21d697977f06a479999ee3f34016bd26a4b4b->leave($__internal_09463178c4b84173417cf80c69e21d697977f06a479999ee3f34016bd26a4b4b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a477c786cf098454b41b728d04332e1c768a6b2a14e876a88e1d56f093897bb5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a477c786cf098454b41b728d04332e1c768a6b2a14e876a88e1d56f093897bb5->enter($__internal_a477c786cf098454b41b728d04332e1c768a6b2a14e876a88e1d56f093897bb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_9477945a19bf2c90cf9b195dbe72074ea083a7ea2f088ca222de51a987363108 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9477945a19bf2c90cf9b195dbe72074ea083a7ea2f088ca222de51a987363108->enter($__internal_9477945a19bf2c90cf9b195dbe72074ea083a7ea2f088ca222de51a987363108_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_9477945a19bf2c90cf9b195dbe72074ea083a7ea2f088ca222de51a987363108->leave($__internal_9477945a19bf2c90cf9b195dbe72074ea083a7ea2f088ca222de51a987363108_prof);

        
        $__internal_a477c786cf098454b41b728d04332e1c768a6b2a14e876a88e1d56f093897bb5->leave($__internal_a477c786cf098454b41b728d04332e1c768a6b2a14e876a88e1d56f093897bb5_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
