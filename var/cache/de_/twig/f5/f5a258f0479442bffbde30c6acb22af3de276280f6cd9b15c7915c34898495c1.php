<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_fb053481dad57ba6fdd38a536f91fad55210559a619b4ffc3c187907dc395e3c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ecf2f5321119c3ff98c6249dc47397c700829833ec7c4d9b648bc9c3d06147be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ecf2f5321119c3ff98c6249dc47397c700829833ec7c4d9b648bc9c3d06147be->enter($__internal_ecf2f5321119c3ff98c6249dc47397c700829833ec7c4d9b648bc9c3d06147be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $__internal_ca3e2ae7b1e87422b6044c0a0f336d8f342e2fa1293e0652338847af8d99ef7b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca3e2ae7b1e87422b6044c0a0f336d8f342e2fa1293e0652338847af8d99ef7b->enter($__internal_ca3e2ae7b1e87422b6044c0a0f336d8f342e2fa1293e0652338847af8d99ef7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ecf2f5321119c3ff98c6249dc47397c700829833ec7c4d9b648bc9c3d06147be->leave($__internal_ecf2f5321119c3ff98c6249dc47397c700829833ec7c4d9b648bc9c3d06147be_prof);

        
        $__internal_ca3e2ae7b1e87422b6044c0a0f336d8f342e2fa1293e0652338847af8d99ef7b->leave($__internal_ca3e2ae7b1e87422b6044c0a0f336d8f342e2fa1293e0652338847af8d99ef7b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_722b4b92485e8660d0417700d1b80af1ea8038160b4d2006ca79ffe8bb91495f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_722b4b92485e8660d0417700d1b80af1ea8038160b4d2006ca79ffe8bb91495f->enter($__internal_722b4b92485e8660d0417700d1b80af1ea8038160b4d2006ca79ffe8bb91495f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_31df151553d7ba50bc6407903bb9a8ada8c91f86d5ebbd0fcb1f8aa1c83ed48c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_31df151553d7ba50bc6407903bb9a8ada8c91f86d5ebbd0fcb1f8aa1c83ed48c->enter($__internal_31df151553d7ba50bc6407903bb9a8ada8c91f86d5ebbd0fcb1f8aa1c83ed48c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_31df151553d7ba50bc6407903bb9a8ada8c91f86d5ebbd0fcb1f8aa1c83ed48c->leave($__internal_31df151553d7ba50bc6407903bb9a8ada8c91f86d5ebbd0fcb1f8aa1c83ed48c_prof);

        
        $__internal_722b4b92485e8660d0417700d1b80af1ea8038160b4d2006ca79ffe8bb91495f->leave($__internal_722b4b92485e8660d0417700d1b80af1ea8038160b4d2006ca79ffe8bb91495f_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_f7898b4e33f15afef756bd9f13fb8d1dd4ace3c28edc9002bf066ff412ced52a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f7898b4e33f15afef756bd9f13fb8d1dd4ace3c28edc9002bf066ff412ced52a->enter($__internal_f7898b4e33f15afef756bd9f13fb8d1dd4ace3c28edc9002bf066ff412ced52a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_15b01c1d053821c186d512028a95c035a74a4bc7deae474edcb0e8486e578298 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15b01c1d053821c186d512028a95c035a74a4bc7deae474edcb0e8486e578298->enter($__internal_15b01c1d053821c186d512028a95c035a74a4bc7deae474edcb0e8486e578298_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_15b01c1d053821c186d512028a95c035a74a4bc7deae474edcb0e8486e578298->leave($__internal_15b01c1d053821c186d512028a95c035a74a4bc7deae474edcb0e8486e578298_prof);

        
        $__internal_f7898b4e33f15afef756bd9f13fb8d1dd4ace3c28edc9002bf066ff412ced52a->leave($__internal_f7898b4e33f15afef756bd9f13fb8d1dd4ace3c28edc9002bf066ff412ced52a_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_bc5ff51a7c8514dddeb5ab7529f20166c2e070c00b9eb2ad19700e67a90c45f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc5ff51a7c8514dddeb5ab7529f20166c2e070c00b9eb2ad19700e67a90c45f5->enter($__internal_bc5ff51a7c8514dddeb5ab7529f20166c2e070c00b9eb2ad19700e67a90c45f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_ad4604c70f51c687261cc45a627ec07e3137b9c3505cfe80ea534c9a829d8c95 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad4604c70f51c687261cc45a627ec07e3137b9c3505cfe80ea534c9a829d8c95->enter($__internal_ad4604c70f51c687261cc45a627ec07e3137b9c3505cfe80ea534c9a829d8c95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_ad4604c70f51c687261cc45a627ec07e3137b9c3505cfe80ea534c9a829d8c95->leave($__internal_ad4604c70f51c687261cc45a627ec07e3137b9c3505cfe80ea534c9a829d8c95_prof);

        
        $__internal_bc5ff51a7c8514dddeb5ab7529f20166c2e070c00b9eb2ad19700e67a90c45f5->leave($__internal_bc5ff51a7c8514dddeb5ab7529f20166c2e070c00b9eb2ad19700e67a90c45f5_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
