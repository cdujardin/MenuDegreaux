<?php

/* SonataAdminBundle:CRUD:show_array.html.twig */
class __TwigTemplate_ea8223ff5e9bfb2c40cee3916f71852c5b9fb03c4edde3264e85eed9744cf809 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 13
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_array.html.twig", 13);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e5647f243aca99151788029859998bef08ca2ce86c46692b2c8279280fd89621 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e5647f243aca99151788029859998bef08ca2ce86c46692b2c8279280fd89621->enter($__internal_e5647f243aca99151788029859998bef08ca2ce86c46692b2c8279280fd89621_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_array.html.twig"));

        $__internal_08c6240d17539345f6410ebd8f2798e678ea0436355aa8eb7f69d9af21b70c03 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08c6240d17539345f6410ebd8f2798e678ea0436355aa8eb7f69d9af21b70c03->enter($__internal_08c6240d17539345f6410ebd8f2798e678ea0436355aa8eb7f69d9af21b70c03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_array.html.twig"));

        // line 11
        $context["show"] = $this->loadTemplate("SonataAdminBundle:CRUD:base_array_macro.html.twig", "SonataAdminBundle:CRUD:show_array.html.twig", 11);
        // line 13
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e5647f243aca99151788029859998bef08ca2ce86c46692b2c8279280fd89621->leave($__internal_e5647f243aca99151788029859998bef08ca2ce86c46692b2c8279280fd89621_prof);

        
        $__internal_08c6240d17539345f6410ebd8f2798e678ea0436355aa8eb7f69d9af21b70c03->leave($__internal_08c6240d17539345f6410ebd8f2798e678ea0436355aa8eb7f69d9af21b70c03_prof);

    }

    // line 15
    public function block_field($context, array $blocks = array())
    {
        $__internal_e4d779b9bfadb3a808488ddbad4bbdd9231f292bf4abe3ba90fd61676186b591 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e4d779b9bfadb3a808488ddbad4bbdd9231f292bf4abe3ba90fd61676186b591->enter($__internal_e4d779b9bfadb3a808488ddbad4bbdd9231f292bf4abe3ba90fd61676186b591_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_e1dd622bf4b06569811ad2f39d74d2d9028d3499df2090de5b0e905fc1849a85 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1dd622bf4b06569811ad2f39d74d2d9028d3499df2090de5b0e905fc1849a85->enter($__internal_e1dd622bf4b06569811ad2f39d74d2d9028d3499df2090de5b0e905fc1849a85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 16
        echo "    ";
        echo $context["show"]->getrender_array((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), (($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "inline", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "inline", array()), false)) : (false)));
        echo "
";
        
        $__internal_e1dd622bf4b06569811ad2f39d74d2d9028d3499df2090de5b0e905fc1849a85->leave($__internal_e1dd622bf4b06569811ad2f39d74d2d9028d3499df2090de5b0e905fc1849a85_prof);

        
        $__internal_e4d779b9bfadb3a808488ddbad4bbdd9231f292bf4abe3ba90fd61676186b591->leave($__internal_e4d779b9bfadb3a808488ddbad4bbdd9231f292bf4abe3ba90fd61676186b591_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_array.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 16,  43 => 15,  33 => 13,  31 => 11,  11 => 13,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% import 'SonataAdminBundle:CRUD:base_array_macro.html.twig' as show %}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field%}
    {{ show.render_array(value, field_description.options.inline|default(false)) }}
{% endblock %}
", "SonataAdminBundle:CRUD:show_array.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/show_array.html.twig");
    }
}
