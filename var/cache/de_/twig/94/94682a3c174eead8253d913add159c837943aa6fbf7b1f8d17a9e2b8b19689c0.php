<?php

/* @Twig/Exception/error.txt.twig */
class __TwigTemplate_b3776c214fbe9604bf05ded501347f06dee67406837720cc7cac3e0eb86bcdf0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d2a254e45d79508bf31a6195504a1fb69b32ad2d083c151b999be8bbcd23e9af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d2a254e45d79508bf31a6195504a1fb69b32ad2d083c151b999be8bbcd23e9af->enter($__internal_d2a254e45d79508bf31a6195504a1fb69b32ad2d083c151b999be8bbcd23e9af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.txt.twig"));

        $__internal_809d6b72d03211ad43620c434c7361e7725beb720a482654b27ced2a4431d90c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_809d6b72d03211ad43620c434c7361e7725beb720a482654b27ced2a4431d90c->enter($__internal_809d6b72d03211ad43620c434c7361e7725beb720a482654b27ced2a4431d90c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_d2a254e45d79508bf31a6195504a1fb69b32ad2d083c151b999be8bbcd23e9af->leave($__internal_d2a254e45d79508bf31a6195504a1fb69b32ad2d083c151b999be8bbcd23e9af_prof);

        
        $__internal_809d6b72d03211ad43620c434c7361e7725beb720a482654b27ced2a4431d90c->leave($__internal_809d6b72d03211ad43620c434c7361e7725beb720a482654b27ced2a4431d90c_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("Oops! An Error Occurred
=======================

The server returned a \"{{ status_code }} {{ status_text }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
", "@Twig/Exception/error.txt.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.txt.twig");
    }
}
