<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_23cf80dd58ef8f3fd2d2b1648216c2121680ec84880b8e03dbc9e70404a505ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a26603f6a1b20799a3c0a5e1526cca541f1fd644fdd8c0203e2c70c5877636b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a26603f6a1b20799a3c0a5e1526cca541f1fd644fdd8c0203e2c70c5877636b0->enter($__internal_a26603f6a1b20799a3c0a5e1526cca541f1fd644fdd8c0203e2c70c5877636b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_bf768a7ac0a0a34f2ad58d6595fc1629aa1f7f397eedb63d6db62988648c041c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf768a7ac0a0a34f2ad58d6595fc1629aa1f7f397eedb63d6db62988648c041c->enter($__internal_bf768a7ac0a0a34f2ad58d6595fc1629aa1f7f397eedb63d6db62988648c041c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_a26603f6a1b20799a3c0a5e1526cca541f1fd644fdd8c0203e2c70c5877636b0->leave($__internal_a26603f6a1b20799a3c0a5e1526cca541f1fd644fdd8c0203e2c70c5877636b0_prof);

        
        $__internal_bf768a7ac0a0a34f2ad58d6595fc1629aa1f7f397eedb63d6db62988648c041c->leave($__internal_bf768a7ac0a0a34f2ad58d6595fc1629aa1f7f397eedb63d6db62988648c041c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
", "@Framework/Form/money_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\money_widget.html.php");
    }
}
