<?php

/* SonataAdminBundle:Core:user_block.html.twig */
class __TwigTemplate_e2afa47560509f18876352be1e8c5036026c5929d63038f5e9eb181a2c8c906f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'user_block' => array($this, 'block_user_block'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_40f01bc8d5e39d413bdc709cc57ee67c1ea9705fbf4068ddc0dad36c727fa42e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40f01bc8d5e39d413bdc709cc57ee67c1ea9705fbf4068ddc0dad36c727fa42e->enter($__internal_40f01bc8d5e39d413bdc709cc57ee67c1ea9705fbf4068ddc0dad36c727fa42e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Core:user_block.html.twig"));

        $__internal_6df0ea2f495603dede103ccb4e2635f2fd95b32dbdd5577c6e5213882b5a8c69 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6df0ea2f495603dede103ccb4e2635f2fd95b32dbdd5577c6e5213882b5a8c69->enter($__internal_6df0ea2f495603dede103ccb4e2635f2fd95b32dbdd5577c6e5213882b5a8c69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Core:user_block.html.twig"));

        // line 1
        $this->displayBlock('user_block', $context, $blocks);
        
        $__internal_40f01bc8d5e39d413bdc709cc57ee67c1ea9705fbf4068ddc0dad36c727fa42e->leave($__internal_40f01bc8d5e39d413bdc709cc57ee67c1ea9705fbf4068ddc0dad36c727fa42e_prof);

        
        $__internal_6df0ea2f495603dede103ccb4e2635f2fd95b32dbdd5577c6e5213882b5a8c69->leave($__internal_6df0ea2f495603dede103ccb4e2635f2fd95b32dbdd5577c6e5213882b5a8c69_prof);

    }

    public function block_user_block($context, array $blocks = array())
    {
        $__internal_fe77fe0879636fca82ecce45bf622a7fe984f4aad19c63a7e3ff36e1d9cafa05 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fe77fe0879636fca82ecce45bf622a7fe984f4aad19c63a7e3ff36e1d9cafa05->enter($__internal_fe77fe0879636fca82ecce45bf622a7fe984f4aad19c63a7e3ff36e1d9cafa05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_block"));

        $__internal_bc6abbfe6f5928f84c6c94dac5886d274030a9f54182e12643d08ab03eec4a18 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc6abbfe6f5928f84c6c94dac5886d274030a9f54182e12643d08ab03eec4a18->enter($__internal_bc6abbfe6f5928f84c6c94dac5886d274030a9f54182e12643d08ab03eec4a18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_block"));

        
        $__internal_bc6abbfe6f5928f84c6c94dac5886d274030a9f54182e12643d08ab03eec4a18->leave($__internal_bc6abbfe6f5928f84c6c94dac5886d274030a9f54182e12643d08ab03eec4a18_prof);

        
        $__internal_fe77fe0879636fca82ecce45bf622a7fe984f4aad19c63a7e3ff36e1d9cafa05->leave($__internal_fe77fe0879636fca82ecce45bf622a7fe984f4aad19c63a7e3ff36e1d9cafa05_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Core:user_block.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block user_block %}{# Customize this value #}{% endblock %}
", "SonataAdminBundle:Core:user_block.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/Core/user_block.html.twig");
    }
}
