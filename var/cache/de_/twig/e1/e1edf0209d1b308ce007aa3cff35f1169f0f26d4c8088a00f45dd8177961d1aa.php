<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_aabd94aa0c869bbe02a302b6b4373a5aac037b61087f4422f332419627bd4c9d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fbb5cc77ff02e93cee0c91901de840af890e45544fcc9c6e02371ef23ddbbbe7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fbb5cc77ff02e93cee0c91901de840af890e45544fcc9c6e02371ef23ddbbbe7->enter($__internal_fbb5cc77ff02e93cee0c91901de840af890e45544fcc9c6e02371ef23ddbbbe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_0e11cf76620e6041a53f4e833235386d6a446675a58fd773b98c21fe8f898cc0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e11cf76620e6041a53f4e833235386d6a446675a58fd773b98c21fe8f898cc0->enter($__internal_0e11cf76620e6041a53f4e833235386d6a446675a58fd773b98c21fe8f898cc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_fbb5cc77ff02e93cee0c91901de840af890e45544fcc9c6e02371ef23ddbbbe7->leave($__internal_fbb5cc77ff02e93cee0c91901de840af890e45544fcc9c6e02371ef23ddbbbe7_prof);

        
        $__internal_0e11cf76620e6041a53f4e833235386d6a446675a58fd773b98c21fe8f898cc0->leave($__internal_0e11cf76620e6041a53f4e833235386d6a446675a58fd773b98c21fe8f898cc0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\reset_widget.html.php");
    }
}
