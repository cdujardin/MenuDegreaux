<?php

/* SonataAdminBundle:CRUD:show_time.html.twig */
class __TwigTemplate_29c1e8453a03f3250c3ddec33c8b344f756995c881350a0d11625995b2cade24 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_time.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1ef0bbc42a76bb7be8bb198bc53296e4e6a3ebcd844dd865184bd197f65e30f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ef0bbc42a76bb7be8bb198bc53296e4e6a3ebcd844dd865184bd197f65e30f9->enter($__internal_1ef0bbc42a76bb7be8bb198bc53296e4e6a3ebcd844dd865184bd197f65e30f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_time.html.twig"));

        $__internal_95b170353bc3d2dd4e95307eea7e5965ae3cacf88bb6a0569a21feb7d9964604 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_95b170353bc3d2dd4e95307eea7e5965ae3cacf88bb6a0569a21feb7d9964604->enter($__internal_95b170353bc3d2dd4e95307eea7e5965ae3cacf88bb6a0569a21feb7d9964604_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_time.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1ef0bbc42a76bb7be8bb198bc53296e4e6a3ebcd844dd865184bd197f65e30f9->leave($__internal_1ef0bbc42a76bb7be8bb198bc53296e4e6a3ebcd844dd865184bd197f65e30f9_prof);

        
        $__internal_95b170353bc3d2dd4e95307eea7e5965ae3cacf88bb6a0569a21feb7d9964604->leave($__internal_95b170353bc3d2dd4e95307eea7e5965ae3cacf88bb6a0569a21feb7d9964604_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_44fc4d6a0c766220b49a76785d40a01ea616e73f754058681a1f85b06f1b3e74 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44fc4d6a0c766220b49a76785d40a01ea616e73f754058681a1f85b06f1b3e74->enter($__internal_44fc4d6a0c766220b49a76785d40a01ea616e73f754058681a1f85b06f1b3e74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_8f1cebfdcc9199e5c33e2acdfb4d6de6e60d3a5239c4295935c0113c4eaff6bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8f1cebfdcc9199e5c33e2acdfb4d6de6e60d3a5239c4295935c0113c4eaff6bd->enter($__internal_8f1cebfdcc9199e5c33e2acdfb4d6de6e60d3a5239c4295935c0113c4eaff6bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            // line 16
            echo "&nbsp;";
        } else {
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "H:i:s"), "html", null, true);
        }
        
        $__internal_8f1cebfdcc9199e5c33e2acdfb4d6de6e60d3a5239c4295935c0113c4eaff6bd->leave($__internal_8f1cebfdcc9199e5c33e2acdfb4d6de6e60d3a5239c4295935c0113c4eaff6bd_prof);

        
        $__internal_44fc4d6a0c766220b49a76785d40a01ea616e73f754058681a1f85b06f1b3e74->leave($__internal_44fc4d6a0c766220b49a76785d40a01ea616e73f754058681a1f85b06f1b3e74_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 18,  51 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field %}
    {%- if value is empty -%}
        &nbsp;
    {%- else -%}
        {{ value|date('H:i:s') }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:show_time.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/show_time.html.twig");
    }
}
