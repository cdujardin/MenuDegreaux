<?php

/* SonataAdminBundle:CRUD:list_inner_row.html.twig */
class __TwigTemplate_5597d166a62b30613ebca9c4b7af60bf8fa3b14c3e547b198de09ace30bd3bc5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_list_inner_row.html.twig", "SonataAdminBundle:CRUD:list_inner_row.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_list_inner_row.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4c1214017fa039cbb90a880efcc203860610a3b24b9f8ee9fce827e17d1638f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c1214017fa039cbb90a880efcc203860610a3b24b9f8ee9fce827e17d1638f6->enter($__internal_4c1214017fa039cbb90a880efcc203860610a3b24b9f8ee9fce827e17d1638f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_inner_row.html.twig"));

        $__internal_d914a94f7661f1249de71c7052d02bd97ff1498e8358e2c0bc06442827fba8c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d914a94f7661f1249de71c7052d02bd97ff1498e8358e2c0bc06442827fba8c3->enter($__internal_d914a94f7661f1249de71c7052d02bd97ff1498e8358e2c0bc06442827fba8c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_inner_row.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4c1214017fa039cbb90a880efcc203860610a3b24b9f8ee9fce827e17d1638f6->leave($__internal_4c1214017fa039cbb90a880efcc203860610a3b24b9f8ee9fce827e17d1638f6_prof);

        
        $__internal_d914a94f7661f1249de71c7052d02bd97ff1498e8358e2c0bc06442827fba8c3->leave($__internal_d914a94f7661f1249de71c7052d02bd97ff1498e8358e2c0bc06442827fba8c3_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_inner_row.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_list_inner_row.html.twig' %}
", "SonataAdminBundle:CRUD:list_inner_row.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list_inner_row.html.twig");
    }
}
