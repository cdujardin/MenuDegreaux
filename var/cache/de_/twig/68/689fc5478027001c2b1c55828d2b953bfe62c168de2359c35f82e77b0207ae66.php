<?php

/* SonataAdminBundle:CRUD:show_date.html.twig */
class __TwigTemplate_4691d9ffb9f3a8d26dadda8ee396d02d334d24013b1b3258386ce2b440652d03 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_date.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ce9751d52d47e56c4df8426466b01203b722a8fb9f9f0537267bd0f54c12d3ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce9751d52d47e56c4df8426466b01203b722a8fb9f9f0537267bd0f54c12d3ee->enter($__internal_ce9751d52d47e56c4df8426466b01203b722a8fb9f9f0537267bd0f54c12d3ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_date.html.twig"));

        $__internal_1d84858bfcec175f9da3c0e0694a9542653be485cc720f4e76224bdfb97ccd8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d84858bfcec175f9da3c0e0694a9542653be485cc720f4e76224bdfb97ccd8b->enter($__internal_1d84858bfcec175f9da3c0e0694a9542653be485cc720f4e76224bdfb97ccd8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_date.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ce9751d52d47e56c4df8426466b01203b722a8fb9f9f0537267bd0f54c12d3ee->leave($__internal_ce9751d52d47e56c4df8426466b01203b722a8fb9f9f0537267bd0f54c12d3ee_prof);

        
        $__internal_1d84858bfcec175f9da3c0e0694a9542653be485cc720f4e76224bdfb97ccd8b->leave($__internal_1d84858bfcec175f9da3c0e0694a9542653be485cc720f4e76224bdfb97ccd8b_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_a6ac226614f616dac61c33fe38ec3b4b2b928fbf79e23b0d2ac7d57d6f43d1f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6ac226614f616dac61c33fe38ec3b4b2b928fbf79e23b0d2ac7d57d6f43d1f7->enter($__internal_a6ac226614f616dac61c33fe38ec3b4b2b928fbf79e23b0d2ac7d57d6f43d1f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_80c17f60a1380f0cab030d5fe1c416e5aeb8c7fdc124281acb9a97d33f785eac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80c17f60a1380f0cab030d5fe1c416e5aeb8c7fdc124281acb9a97d33f785eac->enter($__internal_80c17f60a1380f0cab030d5fe1c416e5aeb8c7fdc124281acb9a97d33f785eac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            // line 16
            echo "&nbsp;";
        } elseif ($this->getAttribute($this->getAttribute(        // line 17
(isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "format", array(), "any", true, true)) {
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "format", array())), "html", null, true);
        } else {
            // line 20
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "F j, Y"), "html", null, true);
        }
        
        $__internal_80c17f60a1380f0cab030d5fe1c416e5aeb8c7fdc124281acb9a97d33f785eac->leave($__internal_80c17f60a1380f0cab030d5fe1c416e5aeb8c7fdc124281acb9a97d33f785eac_prof);

        
        $__internal_a6ac226614f616dac61c33fe38ec3b4b2b928fbf79e23b0d2ac7d57d6f43d1f7->leave($__internal_a6ac226614f616dac61c33fe38ec3b4b2b928fbf79e23b0d2ac7d57d6f43d1f7_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_date.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 20,  55 => 18,  53 => 17,  51 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field%}
    {%- if value is empty -%}
        &nbsp;
    {%- elseif field_description.options.format is defined -%}
        {{ value|date(field_description.options.format) }}
    {%- else -%}
        {{ value|date('F j, Y') }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:show_date.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/show_date.html.twig");
    }
}
