<?php

/* SonataAdminBundle:Menu:sonata_menu.html.twig */
class __TwigTemplate_1ead56c3d1b32db44954c67b3c74364de2e190801bb07c3e3a798afa7257cab3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("knp_menu.html.twig", "SonataAdminBundle:Menu:sonata_menu.html.twig", 1);
        $this->blocks = array(
            'root' => array($this, 'block_root'),
            'item' => array($this, 'block_item'),
            'linkElement' => array($this, 'block_linkElement'),
            'spanElement' => array($this, 'block_spanElement'),
            'label' => array($this, 'block_label'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "knp_menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2950ef5065e41b15bb8fc44d2965af42c06a9663ce70f03b4394566ae6fc6b9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2950ef5065e41b15bb8fc44d2965af42c06a9663ce70f03b4394566ae6fc6b9d->enter($__internal_2950ef5065e41b15bb8fc44d2965af42c06a9663ce70f03b4394566ae6fc6b9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Menu:sonata_menu.html.twig"));

        $__internal_70a5dc006630ad24a34c9a56174ef3723cc8b2d2325b305c3a2959f1f29046cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70a5dc006630ad24a34c9a56174ef3723cc8b2d2325b305c3a2959f1f29046cb->enter($__internal_70a5dc006630ad24a34c9a56174ef3723cc8b2d2325b305c3a2959f1f29046cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Menu:sonata_menu.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2950ef5065e41b15bb8fc44d2965af42c06a9663ce70f03b4394566ae6fc6b9d->leave($__internal_2950ef5065e41b15bb8fc44d2965af42c06a9663ce70f03b4394566ae6fc6b9d_prof);

        
        $__internal_70a5dc006630ad24a34c9a56174ef3723cc8b2d2325b305c3a2959f1f29046cb->leave($__internal_70a5dc006630ad24a34c9a56174ef3723cc8b2d2325b305c3a2959f1f29046cb_prof);

    }

    // line 3
    public function block_root($context, array $blocks = array())
    {
        $__internal_29c58601794ef2549593d018dde1e7e1e0b7e49bc4f134986696e59bd94948fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29c58601794ef2549593d018dde1e7e1e0b7e49bc4f134986696e59bd94948fd->enter($__internal_29c58601794ef2549593d018dde1e7e1e0b7e49bc4f134986696e59bd94948fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "root"));

        $__internal_adca298c6687a95a1a2863dcb0af3adb67cb1e7015e0d7d3d4dfd792e1ea5eb8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_adca298c6687a95a1a2863dcb0af3adb67cb1e7015e0d7d3d4dfd792e1ea5eb8->enter($__internal_adca298c6687a95a1a2863dcb0af3adb67cb1e7015e0d7d3d4dfd792e1ea5eb8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "root"));

        // line 4
        $context["listAttributes"] = twig_array_merge($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "childrenAttributes", array()), array("class" => "sidebar-menu"));
        // line 5
        $context["request"] = (($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "request"), "method")) ? ($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "request"), "method")) : ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array())));
        // line 6
        echo "    ";
        $this->displayBlock("list", $context, $blocks);
        
        $__internal_adca298c6687a95a1a2863dcb0af3adb67cb1e7015e0d7d3d4dfd792e1ea5eb8->leave($__internal_adca298c6687a95a1a2863dcb0af3adb67cb1e7015e0d7d3d4dfd792e1ea5eb8_prof);

        
        $__internal_29c58601794ef2549593d018dde1e7e1e0b7e49bc4f134986696e59bd94948fd->leave($__internal_29c58601794ef2549593d018dde1e7e1e0b7e49bc4f134986696e59bd94948fd_prof);

    }

    // line 9
    public function block_item($context, array $blocks = array())
    {
        $__internal_0c36bac32e7f5b7b1ff1d182464379ed3e84769b03537a521d0d2f952ecfea48 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c36bac32e7f5b7b1ff1d182464379ed3e84769b03537a521d0d2f952ecfea48->enter($__internal_0c36bac32e7f5b7b1ff1d182464379ed3e84769b03537a521d0d2f952ecfea48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "item"));

        $__internal_3406e671db157a2d3f01a7ea0a65b135266cce6bc0f442e8c9b181847af78475 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3406e671db157a2d3f01a7ea0a65b135266cce6bc0f442e8c9b181847af78475->enter($__internal_3406e671db157a2d3f01a7ea0a65b135266cce6bc0f442e8c9b181847af78475_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "item"));

        // line 10
        if ($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "displayed", array())) {
            // line 12
            $context["display"] = (twig_test_empty($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "roles"), "method")) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_SUPER_ADMIN"));
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "roles"), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                if ( !(isset($context["display"]) ? $context["display"] : $this->getContext($context, "display"))) {
                    // line 14
                    $context["display"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted($context["role"]);
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 18
        if (($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "displayed", array()) && ((array_key_exists("display", $context)) ? (_twig_default_filter((isset($context["display"]) ? $context["display"] : $this->getContext($context, "display")))) : ("")))) {
            // line 19
            echo "        ";
            $context["options"] = twig_array_merge((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), array("branch_class" => "treeview", "currentClass" => "active"));
            // line 20
            $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "setChildrenAttribute", array(0 => "class", 1 => twig_trim_filter(($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "childrenAttribute", array(0 => "class"), "method") . " active"))), "method");
            // line 21
            $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "setChildrenAttribute", array(0 => "class", 1 => twig_trim_filter(($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "childrenAttribute", array(0 => "class"), "method") . " treeview-menu"))), "method");
            // line 22
            echo "        ";
            $this->displayParentBlock("item", $context, $blocks);
            echo "
    ";
        }
        
        $__internal_3406e671db157a2d3f01a7ea0a65b135266cce6bc0f442e8c9b181847af78475->leave($__internal_3406e671db157a2d3f01a7ea0a65b135266cce6bc0f442e8c9b181847af78475_prof);

        
        $__internal_0c36bac32e7f5b7b1ff1d182464379ed3e84769b03537a521d0d2f952ecfea48->leave($__internal_0c36bac32e7f5b7b1ff1d182464379ed3e84769b03537a521d0d2f952ecfea48_prof);

    }

    // line 26
    public function block_linkElement($context, array $blocks = array())
    {
        $__internal_bb5a618ed3fbc7c58209d4adcd6075d0a8f5bb7fa99bf1565dd782c10ce6846a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb5a618ed3fbc7c58209d4adcd6075d0a8f5bb7fa99bf1565dd782c10ce6846a->enter($__internal_bb5a618ed3fbc7c58209d4adcd6075d0a8f5bb7fa99bf1565dd782c10ce6846a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "linkElement"));

        $__internal_e7a9094012a89aafbdbc8ee5a2baa24bc700666fc45d086d209d38cdce4cc2af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7a9094012a89aafbdbc8ee5a2baa24bc700666fc45d086d209d38cdce4cc2af->enter($__internal_e7a9094012a89aafbdbc8ee5a2baa24bc700666fc45d086d209d38cdce4cc2af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "linkElement"));

        // line 27
        echo "    ";
        ob_start();
        // line 28
        echo "        ";
        $context["translation_domain"] = $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "label_catalogue", 1 => "messages"), "method");
        // line 29
        echo "        ";
        if (($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "extra", array(0 => "on_top"), "method", true, true) &&  !$this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "on_top"), "method"))) {
            // line 30
            echo "            ";
            $context["icon"] = (($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "extra", array(0 => "icon"), "method", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "extra", array(0 => "icon"), "method"), ((($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "level", array()) > 1)) ? ("<i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>") : ("")))) : (((($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "level", array()) > 1)) ? ("<i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>") : (""))));
            // line 31
            echo "        ";
        } else {
            // line 32
            echo "            ";
            $context["icon"] = $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "icon"), "method");
            // line 33
            echo "        ";
        }
        // line 34
        echo "        ";
        $context["is_link"] = true;
        // line 35
        echo "        ";
        $this->displayParentBlock("linkElement", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_e7a9094012a89aafbdbc8ee5a2baa24bc700666fc45d086d209d38cdce4cc2af->leave($__internal_e7a9094012a89aafbdbc8ee5a2baa24bc700666fc45d086d209d38cdce4cc2af_prof);

        
        $__internal_bb5a618ed3fbc7c58209d4adcd6075d0a8f5bb7fa99bf1565dd782c10ce6846a->leave($__internal_bb5a618ed3fbc7c58209d4adcd6075d0a8f5bb7fa99bf1565dd782c10ce6846a_prof);

    }

    // line 39
    public function block_spanElement($context, array $blocks = array())
    {
        $__internal_5ab7a310a5a106a39e1193fd9891ecf1203bc0f5870dc72da88a166d123e9bef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ab7a310a5a106a39e1193fd9891ecf1203bc0f5870dc72da88a166d123e9bef->enter($__internal_5ab7a310a5a106a39e1193fd9891ecf1203bc0f5870dc72da88a166d123e9bef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "spanElement"));

        $__internal_783009ffbb70aa7aa511dd8df55486e742e4d5830a4cadf954e3d11fd0220842 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_783009ffbb70aa7aa511dd8df55486e742e4d5830a4cadf954e3d11fd0220842->enter($__internal_783009ffbb70aa7aa511dd8df55486e742e4d5830a4cadf954e3d11fd0220842_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "spanElement"));

        // line 40
        echo "    ";
        ob_start();
        // line 41
        echo "        <a href=\"#\">
            ";
        // line 42
        $context["translation_domain"] = $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "label_catalogue"), "method");
        // line 43
        echo "            ";
        $context["icon"] = (($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "extra", array(0 => "icon"), "method", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "extra", array(0 => "icon"), "method"), "")) : (""));
        // line 44
        echo "            ";
        echo (isset($context["icon"]) ? $context["icon"] : $this->getContext($context, "icon"));
        echo "
            ";
        // line 45
        $this->displayParentBlock("spanElement", $context, $blocks);
        // line 46
        if (( !$this->getAttribute((isset($context["item"]) ? $context["item"] : null), "extra", array(0 => "keep_open"), "method", true, true) ||  !$this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "keep_open"), "method"))) {
            // line 47
            echo "<span class=\"pull-right-container\"><i class=\"fa pull-right fa-angle-left\"></i></span>";
        }
        // line 49
        echo "</a>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_783009ffbb70aa7aa511dd8df55486e742e4d5830a4cadf954e3d11fd0220842->leave($__internal_783009ffbb70aa7aa511dd8df55486e742e4d5830a4cadf954e3d11fd0220842_prof);

        
        $__internal_5ab7a310a5a106a39e1193fd9891ecf1203bc0f5870dc72da88a166d123e9bef->leave($__internal_5ab7a310a5a106a39e1193fd9891ecf1203bc0f5870dc72da88a166d123e9bef_prof);

    }

    // line 53
    public function block_label($context, array $blocks = array())
    {
        $__internal_8cbd133e120dae9c702ff2c1e8d14997ec4b80cbaee2c82100d1d9e6ee1abf30 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8cbd133e120dae9c702ff2c1e8d14997ec4b80cbaee2c82100d1d9e6ee1abf30->enter($__internal_8cbd133e120dae9c702ff2c1e8d14997ec4b80cbaee2c82100d1d9e6ee1abf30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_4d752e38c045f30f1cd63b0b1fe0faf09a95383d7ff2203b681a788ddcf89f15 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d752e38c045f30f1cd63b0b1fe0faf09a95383d7ff2203b681a788ddcf89f15->enter($__internal_4d752e38c045f30f1cd63b0b1fe0faf09a95383d7ff2203b681a788ddcf89f15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        if ((array_key_exists("is_link", $context) && (isset($context["is_link"]) ? $context["is_link"] : $this->getContext($context, "is_link")))) {
            echo ((array_key_exists("icon", $context)) ? (_twig_default_filter((isset($context["icon"]) ? $context["icon"] : $this->getContext($context, "icon")))) : (""));
        }
        if (($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "allow_safe_labels", array()) && $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "safe_label", 1 => false), "method"))) {
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "label", array());
        } else {
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "label", array()), array(), ((array_key_exists("translation_domain", $context)) ? (_twig_default_filter((isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")), "messages")) : ("messages"))), "html", null, true);
        }
        
        $__internal_4d752e38c045f30f1cd63b0b1fe0faf09a95383d7ff2203b681a788ddcf89f15->leave($__internal_4d752e38c045f30f1cd63b0b1fe0faf09a95383d7ff2203b681a788ddcf89f15_prof);

        
        $__internal_8cbd133e120dae9c702ff2c1e8d14997ec4b80cbaee2c82100d1d9e6ee1abf30->leave($__internal_8cbd133e120dae9c702ff2c1e8d14997ec4b80cbaee2c82100d1d9e6ee1abf30_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Menu:sonata_menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 53,  196 => 49,  193 => 47,  191 => 46,  189 => 45,  184 => 44,  181 => 43,  179 => 42,  176 => 41,  173 => 40,  164 => 39,  150 => 35,  147 => 34,  144 => 33,  141 => 32,  138 => 31,  135 => 30,  132 => 29,  129 => 28,  126 => 27,  117 => 26,  103 => 22,  101 => 21,  99 => 20,  96 => 19,  94 => 18,  86 => 14,  81 => 13,  79 => 12,  77 => 10,  68 => 9,  57 => 6,  55 => 5,  53 => 4,  44 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'knp_menu.html.twig' %}

{% block root %}
    {%- set listAttributes = item.childrenAttributes|merge({'class': 'sidebar-menu'}) %}
    {%- set request        = item.extra('request') ?: app.request %}
    {{ block('list') -}}
{% endblock %}

{% block item %}
    {%- if item.displayed %}
        {#- check role of the group #}
        {%- set display = (item.extra('roles') is empty or is_granted('ROLE_SUPER_ADMIN') ) %}
        {%- for role in item.extra('roles') if not display %}
            {%- set display = is_granted(role) %}
        {%- endfor %}
    {%- endif %}

    {%- if item.displayed and display|default %}
        {% set options = options|merge({branch_class: 'treeview', currentClass: \"active\"}) %}
        {%- do item.setChildrenAttribute('class', (item.childrenAttribute('class')~' active')|trim) %}
        {%- do item.setChildrenAttribute('class', (item.childrenAttribute('class')~' treeview-menu')|trim) %}
        {{ parent() }}
    {% endif %}
{% endblock %}

{% block linkElement %}
    {% spaceless %}
        {% set translation_domain = item.extra('label_catalogue', 'messages') %}
        {% if item.extra('on_top') is defined and not item.extra('on_top') %}
            {% set icon = item.extra('icon')|default(item.level > 1 ? '<i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>' : '') %}
        {% else %}
            {% set icon = item.extra('icon') %}
        {% endif %}
        {% set is_link = true %}
        {{ parent() }}
    {% endspaceless %}
{% endblock %}

{% block spanElement %}
    {% spaceless %}
        <a href=\"#\">
            {% set translation_domain = item.extra('label_catalogue') %}
            {% set icon = item.extra('icon')|default('') %}
            {{ icon|raw }}
            {{ parent() }}
            {%- if item.extra('keep_open') is not defined or not item.extra('keep_open') -%}
                <span class=\"pull-right-container\"><i class=\"fa pull-right fa-angle-left\"></i></span>
            {%- endif -%}
        </a>
    {% endspaceless %}
{% endblock %}

{% block label %}{% if is_link is defined and is_link %}{{ icon|default|raw }}{% endif %}{% if options.allow_safe_labels and item.extra('safe_label', false) %}{{ item.label|raw }}{% else %}{{ item.label|trans({}, translation_domain|default('messages')) }}{% endif %}{% endblock %}
", "SonataAdminBundle:Menu:sonata_menu.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/Menu/sonata_menu.html.twig");
    }
}
