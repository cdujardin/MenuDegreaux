<?php

/* SonataAdminBundle:CRUD:base_filter_field.html.twig */
class __TwigTemplate_cfca2a58fe71f4151a69902abae730b7df79d7fcb7d059405f69761c0d5e0a0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'label' => array($this, 'block_label'),
            'field' => array($this, 'block_field'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1bfbe3ab3e063871139b5e91aa85ccb073bbb65b11544e6ee05c7f23af911b52 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1bfbe3ab3e063871139b5e91aa85ccb073bbb65b11544e6ee05c7f23af911b52->enter($__internal_1bfbe3ab3e063871139b5e91aa85ccb073bbb65b11544e6ee05c7f23af911b52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_filter_field.html.twig"));

        $__internal_e1ab69f6c80bc9956edacc2616de26feb86785c316088f7019361c87585c998f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1ab69f6c80bc9956edacc2616de26feb86785c316088f7019361c87585c998f->enter($__internal_e1ab69f6c80bc9956edacc2616de26feb86785c316088f7019361c87585c998f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_filter_field.html.twig"));

        // line 11
        echo "

<div>
    ";
        // line 14
        $this->displayBlock('label', $context, $blocks);
        // line 22
        echo "
    <div class=\"sonata-ba-field";
        // line 23
        if ($this->getAttribute($this->getAttribute((isset($context["filter_form"]) ? $context["filter_form"] : $this->getContext($context, "filter_form")), "vars", array()), "errors", array())) {
            echo " sonata-ba-field-error";
        }
        echo "\">
        ";
        // line 24
        $this->displayBlock('field', $context, $blocks);
        // line 25
        echo "    </div>
</div>
";
        
        $__internal_1bfbe3ab3e063871139b5e91aa85ccb073bbb65b11544e6ee05c7f23af911b52->leave($__internal_1bfbe3ab3e063871139b5e91aa85ccb073bbb65b11544e6ee05c7f23af911b52_prof);

        
        $__internal_e1ab69f6c80bc9956edacc2616de26feb86785c316088f7019361c87585c998f->leave($__internal_e1ab69f6c80bc9956edacc2616de26feb86785c316088f7019361c87585c998f_prof);

    }

    // line 14
    public function block_label($context, array $blocks = array())
    {
        $__internal_73427066c7cf57ee2436c430197249414476ba8fb191b348b7be9002b4d76f16 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73427066c7cf57ee2436c430197249414476ba8fb191b348b7be9002b4d76f16->enter($__internal_73427066c7cf57ee2436c430197249414476ba8fb191b348b7be9002b4d76f16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_3362120fec46c171d079c13ee54e39734a3a162036406ae20fb76a06a8c469ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3362120fec46c171d079c13ee54e39734a3a162036406ae20fb76a06a8c469ef->enter($__internal_3362120fec46c171d079c13ee54e39734a3a162036406ae20fb76a06a8c469ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        // line 15
        echo "        ";
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["filter"]) ? $context["filter"] : null), "fielddescription", array(), "any", false, true), "options", array(), "any", false, true), "name", array(), "any", true, true)) {
            // line 16
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["filter_form"]) ? $context["filter_form"] : $this->getContext($context, "filter_form")), 'label', (twig_test_empty($_label_ = $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["filter"]) ? $context["filter"] : $this->getContext($context, "filter")), "fielddescription", array()), "options", array()), "name", array())) ? array() : array("label" => $_label_)));
            echo "
        ";
        } else {
            // line 18
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["filter_form"]) ? $context["filter_form"] : $this->getContext($context, "filter_form")), 'label');
            echo "
        ";
        }
        // line 20
        echo "        <br>
    ";
        
        $__internal_3362120fec46c171d079c13ee54e39734a3a162036406ae20fb76a06a8c469ef->leave($__internal_3362120fec46c171d079c13ee54e39734a3a162036406ae20fb76a06a8c469ef_prof);

        
        $__internal_73427066c7cf57ee2436c430197249414476ba8fb191b348b7be9002b4d76f16->leave($__internal_73427066c7cf57ee2436c430197249414476ba8fb191b348b7be9002b4d76f16_prof);

    }

    // line 24
    public function block_field($context, array $blocks = array())
    {
        $__internal_6dc5fbf573e1b0a03eaab7859d3b116ee37d86a1bc5268dc61efa18bf82be336 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6dc5fbf573e1b0a03eaab7859d3b116ee37d86a1bc5268dc61efa18bf82be336->enter($__internal_6dc5fbf573e1b0a03eaab7859d3b116ee37d86a1bc5268dc61efa18bf82be336_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_f07f9baa9ad63d9e26a8dd27df2c9dad30f7903aae7c75dfaef64d91f12c73c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f07f9baa9ad63d9e26a8dd27df2c9dad30f7903aae7c75dfaef64d91f12c73c5->enter($__internal_f07f9baa9ad63d9e26a8dd27df2c9dad30f7903aae7c75dfaef64d91f12c73c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["filter_form"]) ? $context["filter_form"] : $this->getContext($context, "filter_form")), 'widget');
        
        $__internal_f07f9baa9ad63d9e26a8dd27df2c9dad30f7903aae7c75dfaef64d91f12c73c5->leave($__internal_f07f9baa9ad63d9e26a8dd27df2c9dad30f7903aae7c75dfaef64d91f12c73c5_prof);

        
        $__internal_6dc5fbf573e1b0a03eaab7859d3b116ee37d86a1bc5268dc61efa18bf82be336->leave($__internal_6dc5fbf573e1b0a03eaab7859d3b116ee37d86a1bc5268dc61efa18bf82be336_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_filter_field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 24,  81 => 20,  75 => 18,  69 => 16,  66 => 15,  57 => 14,  45 => 25,  43 => 24,  37 => 23,  34 => 22,  32 => 14,  27 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}


<div>
    {% block label %}
        {% if filter.fielddescription.options.name is defined %}
            {{ form_label(filter_form, filter.fielddescription.options.name) }}
        {% else %}
            {{ form_label(filter_form) }}
        {% endif %}
        <br>
    {% endblock %}

    <div class=\"sonata-ba-field{% if filter_form.vars.errors %} sonata-ba-field-error{% endif %}\">
        {% block field %}{{ form_widget(filter_form) }}{% endblock %}
    </div>
</div>
", "SonataAdminBundle:CRUD:base_filter_field.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/base_filter_field.html.twig");
    }
}
