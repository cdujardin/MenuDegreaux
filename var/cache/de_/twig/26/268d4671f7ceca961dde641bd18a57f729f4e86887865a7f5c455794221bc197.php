<?php

/* SonataAdminBundle:Pager:results.html.twig */
class __TwigTemplate_9ecc4d50a26809caa321687fce85844d46504901b78b97f534bf376e5419a562 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:Pager:base_results.html.twig", "SonataAdminBundle:Pager:results.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Pager:base_results.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_56f3700c3fe0f62222f9a07b30b137282edb76557995839525bacb1547550000 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56f3700c3fe0f62222f9a07b30b137282edb76557995839525bacb1547550000->enter($__internal_56f3700c3fe0f62222f9a07b30b137282edb76557995839525bacb1547550000_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:results.html.twig"));

        $__internal_ff314da22d2d6646769e84e15758d18306d763db231cad4acd2b65e0d1f00e02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff314da22d2d6646769e84e15758d18306d763db231cad4acd2b65e0d1f00e02->enter($__internal_ff314da22d2d6646769e84e15758d18306d763db231cad4acd2b65e0d1f00e02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:results.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_56f3700c3fe0f62222f9a07b30b137282edb76557995839525bacb1547550000->leave($__internal_56f3700c3fe0f62222f9a07b30b137282edb76557995839525bacb1547550000_prof);

        
        $__internal_ff314da22d2d6646769e84e15758d18306d763db231cad4acd2b65e0d1f00e02->leave($__internal_ff314da22d2d6646769e84e15758d18306d763db231cad4acd2b65e0d1f00e02_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Pager:results.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:Pager:base_results.html.twig' %}
", "SonataAdminBundle:Pager:results.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/Pager/results.html.twig");
    }
}
