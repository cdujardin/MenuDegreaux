<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_fd771bfd17c8235358447dd07894251ed7d78cb42333d4663d8ce2cc496c41fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c2e8dd9d43688fbf15aa93c6d17a29effa23ec429d96ae44a589a2b32777367 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8c2e8dd9d43688fbf15aa93c6d17a29effa23ec429d96ae44a589a2b32777367->enter($__internal_8c2e8dd9d43688fbf15aa93c6d17a29effa23ec429d96ae44a589a2b32777367_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_3355ab64a78f391da38dbf2ee5fc8528fe527775087eea6cdb6a9eaafe4d0cb6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3355ab64a78f391da38dbf2ee5fc8528fe527775087eea6cdb6a9eaafe4d0cb6->enter($__internal_3355ab64a78f391da38dbf2ee5fc8528fe527775087eea6cdb6a9eaafe4d0cb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_8c2e8dd9d43688fbf15aa93c6d17a29effa23ec429d96ae44a589a2b32777367->leave($__internal_8c2e8dd9d43688fbf15aa93c6d17a29effa23ec429d96ae44a589a2b32777367_prof);

        
        $__internal_3355ab64a78f391da38dbf2ee5fc8528fe527775087eea6cdb6a9eaafe4d0cb6->leave($__internal_3355ab64a78f391da38dbf2ee5fc8528fe527775087eea6cdb6a9eaafe4d0cb6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_end.html.php");
    }
}
