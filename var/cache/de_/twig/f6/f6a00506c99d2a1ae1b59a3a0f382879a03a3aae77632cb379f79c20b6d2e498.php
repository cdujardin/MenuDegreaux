<?php

/* @Twig/Exception/exception.css.twig */
class __TwigTemplate_014dc3189f1eec3cdf072979546bbc38f43dec1e545b591452aa61b6f9a3ef2d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bbf8ea40f1a952235f08323e1e71c0d1138e72f28a08d5d43c9e0cca9c2f3b78 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bbf8ea40f1a952235f08323e1e71c0d1138e72f28a08d5d43c9e0cca9c2f3b78->enter($__internal_bbf8ea40f1a952235f08323e1e71c0d1138e72f28a08d5d43c9e0cca9c2f3b78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        $__internal_dcf143846971e971b954a34eace4e14e3c4bef89c27130e6a04b7826ec26dd2f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dcf143846971e971b954a34eace4e14e3c4bef89c27130e6a04b7826ec26dd2f->enter($__internal_dcf143846971e971b954a34eace4e14e3c4bef89c27130e6a04b7826ec26dd2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_bbf8ea40f1a952235f08323e1e71c0d1138e72f28a08d5d43c9e0cca9c2f3b78->leave($__internal_bbf8ea40f1a952235f08323e1e71c0d1138e72f28a08d5d43c9e0cca9c2f3b78_prof);

        
        $__internal_dcf143846971e971b954a34eace4e14e3c4bef89c27130e6a04b7826ec26dd2f->leave($__internal_dcf143846971e971b954a34eace4e14e3c4bef89c27130e6a04b7826ec26dd2f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "@Twig/Exception/exception.css.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.css.twig");
    }
}
