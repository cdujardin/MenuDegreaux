<?php

/* buttons/delete_button.html.twig */
class __TwigTemplate_9e6cf0672d839863475c93125549b914c0c933b400a4480696885b878343e3fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_17c0a3afe26709f23929c671818d0160a9ef885d87ee2f5b1e17afcda8169ee9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17c0a3afe26709f23929c671818d0160a9ef885d87ee2f5b1e17afcda8169ee9->enter($__internal_17c0a3afe26709f23929c671818d0160a9ef885d87ee2f5b1e17afcda8169ee9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "buttons/delete_button.html.twig"));

        $__internal_687c2f39837a02c5a4bd4e1b396be6a04cc9cb5fbd3b24093270ddb5a0c85059 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_687c2f39837a02c5a4bd4e1b396be6a04cc9cb5fbd3b24093270ddb5a0c85059->enter($__internal_687c2f39837a02c5a4bd4e1b396be6a04cc9cb5fbd3b24093270ddb5a0c85059_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "buttons/delete_button.html.twig"));

        // line 1
        if (($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "hasAccess", array(0 => "delete", 1 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method") && $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "hasRoute", array(0 => "delete"), "method"))) {
            // line 2
            echo "
    <a href=\"";
            // line 3
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "generateObjectUrl", array(0 => "delete", 1 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), "html", null, true);
            echo "\" class=\"btn btn-sm btn-danger delete_link\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action_delete", array(), "SonataAdminBundle"), "html", null, true);
            echo "\">

        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>

        Supprimer

    </a>

";
        }
        
        $__internal_17c0a3afe26709f23929c671818d0160a9ef885d87ee2f5b1e17afcda8169ee9->leave($__internal_17c0a3afe26709f23929c671818d0160a9ef885d87ee2f5b1e17afcda8169ee9_prof);

        
        $__internal_687c2f39837a02c5a4bd4e1b396be6a04cc9cb5fbd3b24093270ddb5a0c85059->leave($__internal_687c2f39837a02c5a4bd4e1b396be6a04cc9cb5fbd3b24093270ddb5a0c85059_prof);

    }

    public function getTemplateName()
    {
        return "buttons/delete_button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if admin.hasAccess('delete', object) and admin.hasRoute('delete') %}

    <a href=\"{{ admin.generateObjectUrl('delete', object) }}\" class=\"btn btn-sm btn-danger delete_link\" title=\"{{ 'action_delete'|trans({}, 'SonataAdminBundle') }}\">

        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>

        Supprimer

    </a>

{% endif %}
", "buttons/delete_button.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\buttons\\delete_button.html.twig");
    }
}
