<?php

/* SonataAdminBundle:CRUD:history_revision_timestamp.html.twig */
class __TwigTemplate_c1f99e31873ef8cd3ac721cf597d17ea04a4e6666e48de3e2ec6964142531122 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1b96062b668cc5a5a682b436150d7d77fc7682d6604570e6c1dd745d8aab282e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1b96062b668cc5a5a682b436150d7d77fc7682d6604570e6c1dd745d8aab282e->enter($__internal_1b96062b668cc5a5a682b436150d7d77fc7682d6604570e6c1dd745d8aab282e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:history_revision_timestamp.html.twig"));

        $__internal_0a86d0bf775c7f8e918e66b06b4aa240dda5b9fa98978ea32ee3f5e6fd7e5fa4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0a86d0bf775c7f8e918e66b06b4aa240dda5b9fa98978ea32ee3f5e6fd7e5fa4->enter($__internal_0a86d0bf775c7f8e918e66b06b4aa240dda5b9fa98978ea32ee3f5e6fd7e5fa4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:history_revision_timestamp.html.twig"));

        // line 11
        echo "
";
        // line 12
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["revision"]) ? $context["revision"] : $this->getContext($context, "revision")), "timestamp", array())), "html", null, true);
        echo "
";
        
        $__internal_1b96062b668cc5a5a682b436150d7d77fc7682d6604570e6c1dd745d8aab282e->leave($__internal_1b96062b668cc5a5a682b436150d7d77fc7682d6604570e6c1dd745d8aab282e_prof);

        
        $__internal_0a86d0bf775c7f8e918e66b06b4aa240dda5b9fa98978ea32ee3f5e6fd7e5fa4->leave($__internal_0a86d0bf775c7f8e918e66b06b4aa240dda5b9fa98978ea32ee3f5e6fd7e5fa4_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:history_revision_timestamp.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 12,  25 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{{ revision.timestamp|date }}
", "SonataAdminBundle:CRUD:history_revision_timestamp.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/history_revision_timestamp.html.twig");
    }
}
