<?php

/* @Twig/Exception/error.atom.twig */
class __TwigTemplate_8769f343800064d7d3a6c8953e5d6ef2e7ae75f1a72f452a26508cdef3c57121 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f96e74d249839339bf0263704762dd19d177808d5fbb0398429fc23df17da39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f96e74d249839339bf0263704762dd19d177808d5fbb0398429fc23df17da39->enter($__internal_6f96e74d249839339bf0263704762dd19d177808d5fbb0398429fc23df17da39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.atom.twig"));

        $__internal_0640e5a0842878f66facde734cf4ce29033f60494edc52898062f3ee0a0dfdf0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0640e5a0842878f66facde734cf4ce29033f60494edc52898062f3ee0a0dfdf0->enter($__internal_0640e5a0842878f66facde734cf4ce29033f60494edc52898062f3ee0a0dfdf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_6f96e74d249839339bf0263704762dd19d177808d5fbb0398429fc23df17da39->leave($__internal_6f96e74d249839339bf0263704762dd19d177808d5fbb0398429fc23df17da39_prof);

        
        $__internal_0640e5a0842878f66facde734cf4ce29033f60494edc52898062f3ee0a0dfdf0->leave($__internal_0640e5a0842878f66facde734cf4ce29033f60494edc52898062f3ee0a0dfdf0_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "@Twig/Exception/error.atom.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.atom.twig");
    }
}
