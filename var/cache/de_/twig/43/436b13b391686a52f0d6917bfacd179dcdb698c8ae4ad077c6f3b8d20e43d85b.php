<?php

/* SonataAdminBundle:CRUD:base_inline_edit_field.html.twig */
class __TwigTemplate_2e3e56c4b504777bab042732f2b87d1c5b6ef3db6448af06b609a456f516b256 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'label' => array($this, 'block_label'),
            'field' => array($this, 'block_field'),
            'errors' => array($this, 'block_errors'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca5eb8e8bd866a856beef1584942644dc45d163c5772e2831dbdfcb8d5c6ef57 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ca5eb8e8bd866a856beef1584942644dc45d163c5772e2831dbdfcb8d5c6ef57->enter($__internal_ca5eb8e8bd866a856beef1584942644dc45d163c5772e2831dbdfcb8d5c6ef57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_inline_edit_field.html.twig"));

        $__internal_ba2f12be3dcf8cb0c78a3b1700d4e0d82f94697cf9a4d1a84cdcfee9756878ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba2f12be3dcf8cb0c78a3b1700d4e0d82f94697cf9a4d1a84cdcfee9756878ac->enter($__internal_ba2f12be3dcf8cb0c78a3b1700d4e0d82f94697cf9a4d1a84cdcfee9756878ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_inline_edit_field.html.twig"));

        // line 11
        echo "
<div id=\"sonata-ba-field-container-";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), "vars", array()), "id", array()), "html", null, true);
        echo "\" class=\"sonata-ba-field sonata-ba-field-";
        echo twig_escape_filter($this->env, (isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, (isset($context["inline"]) ? $context["inline"] : $this->getContext($context, "inline")), "html", null, true);
        echo " ";
        if (twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), "vars", array()), "errors", array()))) {
            echo "sonata-ba-field-error";
        }
        echo "\">

    ";
        // line 14
        $this->displayBlock('label', $context, $blocks);
        // line 24
        echo "
    ";
        // line 25
        $this->displayBlock('field', $context, $blocks);
        // line 26
        echo "
    <div class=\"sonata-ba-field-error-messages\">
        ";
        // line 28
        $this->displayBlock('errors', $context, $blocks);
        // line 29
        echo "    </div>
</div>
";
        
        $__internal_ca5eb8e8bd866a856beef1584942644dc45d163c5772e2831dbdfcb8d5c6ef57->leave($__internal_ca5eb8e8bd866a856beef1584942644dc45d163c5772e2831dbdfcb8d5c6ef57_prof);

        
        $__internal_ba2f12be3dcf8cb0c78a3b1700d4e0d82f94697cf9a4d1a84cdcfee9756878ac->leave($__internal_ba2f12be3dcf8cb0c78a3b1700d4e0d82f94697cf9a4d1a84cdcfee9756878ac_prof);

    }

    // line 14
    public function block_label($context, array $blocks = array())
    {
        $__internal_ee6afc81a63dc398e090a298d14f142672a8db25fed93413ca2bc7f4a4fc7472 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee6afc81a63dc398e090a298d14f142672a8db25fed93413ca2bc7f4a4fc7472->enter($__internal_ee6afc81a63dc398e090a298d14f142672a8db25fed93413ca2bc7f4a4fc7472_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_63bb8627022556cf000b7a09ad5c8247f3ac8bf2f9c2864b1a71affb9677da6d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63bb8627022556cf000b7a09ad5c8247f3ac8bf2f9c2864b1a71affb9677da6d->enter($__internal_63bb8627022556cf000b7a09ad5c8247f3ac8bf2f9c2864b1a71affb9677da6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        // line 15
        echo "        ";
        if (((isset($context["inline"]) ? $context["inline"] : $this->getContext($context, "inline")) == "natural")) {
            // line 16
            echo "            ";
            if ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "name", array(), "any", true, true)) {
                // line 17
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'label', (twig_test_empty($_label_ = $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "name", array())) ? array() : array("label" => $_label_)));
                echo "
            ";
            } else {
                // line 19
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'label');
                echo "
            ";
            }
            // line 21
            echo "            <br>
        ";
        }
        // line 23
        echo "    ";
        
        $__internal_63bb8627022556cf000b7a09ad5c8247f3ac8bf2f9c2864b1a71affb9677da6d->leave($__internal_63bb8627022556cf000b7a09ad5c8247f3ac8bf2f9c2864b1a71affb9677da6d_prof);

        
        $__internal_ee6afc81a63dc398e090a298d14f142672a8db25fed93413ca2bc7f4a4fc7472->leave($__internal_ee6afc81a63dc398e090a298d14f142672a8db25fed93413ca2bc7f4a4fc7472_prof);

    }

    // line 25
    public function block_field($context, array $blocks = array())
    {
        $__internal_54a04003fb761e64e046cd542d6630e75ed2c94e591afb0218c8b110b8bb5176 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_54a04003fb761e64e046cd542d6630e75ed2c94e591afb0218c8b110b8bb5176->enter($__internal_54a04003fb761e64e046cd542d6630e75ed2c94e591afb0218c8b110b8bb5176_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_d1a8270935cb25738c4261db7992a79c9585a6ca226e07a2c70ab233bebc19ca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1a8270935cb25738c4261db7992a79c9585a6ca226e07a2c70ab233bebc19ca->enter($__internal_d1a8270935cb25738c4261db7992a79c9585a6ca226e07a2c70ab233bebc19ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'widget');
        
        $__internal_d1a8270935cb25738c4261db7992a79c9585a6ca226e07a2c70ab233bebc19ca->leave($__internal_d1a8270935cb25738c4261db7992a79c9585a6ca226e07a2c70ab233bebc19ca_prof);

        
        $__internal_54a04003fb761e64e046cd542d6630e75ed2c94e591afb0218c8b110b8bb5176->leave($__internal_54a04003fb761e64e046cd542d6630e75ed2c94e591afb0218c8b110b8bb5176_prof);

    }

    // line 28
    public function block_errors($context, array $blocks = array())
    {
        $__internal_2e761e8f1ae294a3423cdc13b123a1a90071de012d4b3c67f2bb21f29ebb37ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e761e8f1ae294a3423cdc13b123a1a90071de012d4b3c67f2bb21f29ebb37ec->enter($__internal_2e761e8f1ae294a3423cdc13b123a1a90071de012d4b3c67f2bb21f29ebb37ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "errors"));

        $__internal_0806c5cbc1271cfd4102c3cb3760422f10b36b786bf79f85e90f6865471c5809 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0806c5cbc1271cfd4102c3cb3760422f10b36b786bf79f85e90f6865471c5809->enter($__internal_0806c5cbc1271cfd4102c3cb3760422f10b36b786bf79f85e90f6865471c5809_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "errors"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'errors');
        
        $__internal_0806c5cbc1271cfd4102c3cb3760422f10b36b786bf79f85e90f6865471c5809->leave($__internal_0806c5cbc1271cfd4102c3cb3760422f10b36b786bf79f85e90f6865471c5809_prof);

        
        $__internal_2e761e8f1ae294a3423cdc13b123a1a90071de012d4b3c67f2bb21f29ebb37ec->leave($__internal_2e761e8f1ae294a3423cdc13b123a1a90071de012d4b3c67f2bb21f29ebb37ec_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_inline_edit_field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 28,  110 => 25,  100 => 23,  96 => 21,  90 => 19,  84 => 17,  81 => 16,  78 => 15,  69 => 14,  57 => 29,  55 => 28,  51 => 26,  49 => 25,  46 => 24,  44 => 14,  31 => 12,  28 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<div id=\"sonata-ba-field-container-{{ field_element.vars.id }}\" class=\"sonata-ba-field sonata-ba-field-{{ edit }}-{{ inline }} {% if field_element.vars.errors|length %}sonata-ba-field-error{% endif %}\">

    {% block label %}
        {% if inline == 'natural' %}
            {% if field_description.options.name is defined %}
                {{ form_label(field_element, field_description.options.name) }}
            {% else %}
                {{ form_label(field_element) }}
            {% endif %}
            <br>
        {% endif %}
    {% endblock %}

    {% block field %}{{ form_widget(field_element) }}{% endblock %}

    <div class=\"sonata-ba-field-error-messages\">
        {% block errors %}{{ form_errors(field_element) }}{% endblock %}
    </div>
</div>
", "SonataAdminBundle:CRUD:base_inline_edit_field.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/base_inline_edit_field.html.twig");
    }
}
