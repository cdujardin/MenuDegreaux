<?php

/* SonataAdminBundle:CRUD:action.html.twig */
class __TwigTemplate_8cd9d53f1383a533f4bc0358bc9371fbe3fffd3e4120652c2a8a1369c1fb924b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'actions' => array($this, 'block_actions'),
            'tab_menu' => array($this, 'block_tab_menu'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:action.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb9ebea57e9a6d759765e949f49b9676c0b6ee5d322bbe49e062dea5fa033018 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cb9ebea57e9a6d759765e949f49b9676c0b6ee5d322bbe49e062dea5fa033018->enter($__internal_cb9ebea57e9a6d759765e949f49b9676c0b6ee5d322bbe49e062dea5fa033018_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:action.html.twig"));

        $__internal_2eec4b21c42746b38cd2e8e5221b6fa21091a17e04e4bbdf36113ca875011caa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2eec4b21c42746b38cd2e8e5221b6fa21091a17e04e4bbdf36113ca875011caa->enter($__internal_2eec4b21c42746b38cd2e8e5221b6fa21091a17e04e4bbdf36113ca875011caa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:action.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cb9ebea57e9a6d759765e949f49b9676c0b6ee5d322bbe49e062dea5fa033018->leave($__internal_cb9ebea57e9a6d759765e949f49b9676c0b6ee5d322bbe49e062dea5fa033018_prof);

        
        $__internal_2eec4b21c42746b38cd2e8e5221b6fa21091a17e04e4bbdf36113ca875011caa->leave($__internal_2eec4b21c42746b38cd2e8e5221b6fa21091a17e04e4bbdf36113ca875011caa_prof);

    }

    // line 14
    public function block_actions($context, array $blocks = array())
    {
        $__internal_0e505545a92d142bc0584cd75c47468d5b2bd010f3c9df74ae8fb28a723409de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0e505545a92d142bc0584cd75c47468d5b2bd010f3c9df74ae8fb28a723409de->enter($__internal_0e505545a92d142bc0584cd75c47468d5b2bd010f3c9df74ae8fb28a723409de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        $__internal_f1c38047bbab8c7400296636193c85e101692d79a63961502ace3a617d7a8a40 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f1c38047bbab8c7400296636193c85e101692d79a63961502ace3a617d7a8a40->enter($__internal_f1c38047bbab8c7400296636193c85e101692d79a63961502ace3a617d7a8a40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:action_buttons.html.twig", "SonataAdminBundle:CRUD:action.html.twig", 15)->display($context);
        
        $__internal_f1c38047bbab8c7400296636193c85e101692d79a63961502ace3a617d7a8a40->leave($__internal_f1c38047bbab8c7400296636193c85e101692d79a63961502ace3a617d7a8a40_prof);

        
        $__internal_0e505545a92d142bc0584cd75c47468d5b2bd010f3c9df74ae8fb28a723409de->leave($__internal_0e505545a92d142bc0584cd75c47468d5b2bd010f3c9df74ae8fb28a723409de_prof);

    }

    // line 18
    public function block_tab_menu($context, array $blocks = array())
    {
        $__internal_17af9496bb55d9faa747e5af6107b33e02e38302db57c5307036919cc70c62b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17af9496bb55d9faa747e5af6107b33e02e38302db57c5307036919cc70c62b1->enter($__internal_17af9496bb55d9faa747e5af6107b33e02e38302db57c5307036919cc70c62b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        $__internal_f71e65292b6598d89d4891387ddef90202d7dbabdb27cfec816688a25f195733 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f71e65292b6598d89d4891387ddef90202d7dbabdb27cfec816688a25f195733->enter($__internal_f71e65292b6598d89d4891387ddef90202d7dbabdb27cfec816688a25f195733_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        // line 19
        echo "    ";
        if (array_key_exists("action", $context)) {
            // line 20
            echo "        ";
            echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "sidemenu", array(0 => (isset($context["action"]) ? $context["action"] : $this->getContext($context, "action"))), "method"), array("currentClass" => "active", "template" => $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getTemplate", array(0 => "tab_menu_template"), "method")), "twig");
            echo "
    ";
        }
        
        $__internal_f71e65292b6598d89d4891387ddef90202d7dbabdb27cfec816688a25f195733->leave($__internal_f71e65292b6598d89d4891387ddef90202d7dbabdb27cfec816688a25f195733_prof);

        
        $__internal_17af9496bb55d9faa747e5af6107b33e02e38302db57c5307036919cc70c62b1->leave($__internal_17af9496bb55d9faa747e5af6107b33e02e38302db57c5307036919cc70c62b1_prof);

    }

    // line 24
    public function block_content($context, array $blocks = array())
    {
        $__internal_13071b61ecda1e68842419affb402788f07837ce10eb975307fa72e2a73fd5f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13071b61ecda1e68842419affb402788f07837ce10eb975307fa72e2a73fd5f8->enter($__internal_13071b61ecda1e68842419affb402788f07837ce10eb975307fa72e2a73fd5f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_f78682d55d0ad94a6d00f324028a9310d27fe264fd665f1cbdd5dc252d7f1478 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f78682d55d0ad94a6d00f324028a9310d27fe264fd665f1cbdd5dc252d7f1478->enter($__internal_f78682d55d0ad94a6d00f324028a9310d27fe264fd665f1cbdd5dc252d7f1478_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 25
        echo "
    Redefine the content block in your action template

";
        
        $__internal_f78682d55d0ad94a6d00f324028a9310d27fe264fd665f1cbdd5dc252d7f1478->leave($__internal_f78682d55d0ad94a6d00f324028a9310d27fe264fd665f1cbdd5dc252d7f1478_prof);

        
        $__internal_13071b61ecda1e68842419affb402788f07837ce10eb975307fa72e2a73fd5f8->leave($__internal_13071b61ecda1e68842419affb402788f07837ce10eb975307fa72e2a73fd5f8_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 25,  86 => 24,  72 => 20,  69 => 19,  60 => 18,  50 => 15,  41 => 14,  20 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:action_buttons.html.twig' %}
{%- endblock -%}

{% block tab_menu %}
    {% if action is defined %}
        {{ knp_menu_render(admin.sidemenu(action), {'currentClass' : 'active', 'template': sonata_admin.adminPool.getTemplate('tab_menu_template')}, 'twig') }}
    {% endif %}
{% endblock %}

{% block content %}

    Redefine the content block in your action template

{% endblock %}
", "SonataAdminBundle:CRUD:action.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/action.html.twig");
    }
}
