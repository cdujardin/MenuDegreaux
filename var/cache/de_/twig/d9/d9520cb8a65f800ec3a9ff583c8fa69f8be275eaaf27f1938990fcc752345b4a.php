<?php

/* SonataAdminBundle:CRUD:base_show.html.twig */
class __TwigTemplate_3add47f811a8130f8d85c44881ba6057d8e187e8389a3fa2bae5ab1f1f80c66e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'navbar_title' => array($this, 'block_navbar_title'),
            'actions' => array($this, 'block_actions'),
            'tab_menu' => array($this, 'block_tab_menu'),
            'show' => array($this, 'block_show'),
            'show_groups' => array($this, 'block_show_groups'),
            'field_row' => array($this, 'block_field_row'),
            'show_title' => array($this, 'block_show_title'),
            'show_field' => array($this, 'block_show_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:base_show.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_233fa638b687057846ab0a373067037b15ba2187fb5623c3c8c337c08952c134 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_233fa638b687057846ab0a373067037b15ba2187fb5623c3c8c337c08952c134->enter($__internal_233fa638b687057846ab0a373067037b15ba2187fb5623c3c8c337c08952c134_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_show.html.twig"));

        $__internal_44e7f726835f66f606f138de69d6545307cb54cc5abe6cf9886d82bd878e27e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_44e7f726835f66f606f138de69d6545307cb54cc5abe6cf9886d82bd878e27e1->enter($__internal_44e7f726835f66f606f138de69d6545307cb54cc5abe6cf9886d82bd878e27e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_show.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_233fa638b687057846ab0a373067037b15ba2187fb5623c3c8c337c08952c134->leave($__internal_233fa638b687057846ab0a373067037b15ba2187fb5623c3c8c337c08952c134_prof);

        
        $__internal_44e7f726835f66f606f138de69d6545307cb54cc5abe6cf9886d82bd878e27e1->leave($__internal_44e7f726835f66f606f138de69d6545307cb54cc5abe6cf9886d82bd878e27e1_prof);

    }

    // line 14
    public function block_title($context, array $blocks = array())
    {
        $__internal_22cd672eb1085d69f34989d2b8123de2ee8d91996ddda5d0463681dd80ab0a65 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22cd672eb1085d69f34989d2b8123de2ee8d91996ddda5d0463681dd80ab0a65->enter($__internal_22cd672eb1085d69f34989d2b8123de2ee8d91996ddda5d0463681dd80ab0a65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_19119c92fd3dae5b2ac12a1f8b5fd3308ad4b38b5b25c28d4fae7759a068d60f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19119c92fd3dae5b2ac12a1f8b5fd3308ad4b38b5b25c28d4fae7759a068d60f->enter($__internal_19119c92fd3dae5b2ac12a1f8b5fd3308ad4b38b5b25c28d4fae7759a068d60f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 15
        echo "    ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_show", array("%name%" => twig_truncate_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "toString", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), 15)), "SonataAdminBundle"), "html", null, true);
        echo "
";
        
        $__internal_19119c92fd3dae5b2ac12a1f8b5fd3308ad4b38b5b25c28d4fae7759a068d60f->leave($__internal_19119c92fd3dae5b2ac12a1f8b5fd3308ad4b38b5b25c28d4fae7759a068d60f_prof);

        
        $__internal_22cd672eb1085d69f34989d2b8123de2ee8d91996ddda5d0463681dd80ab0a65->leave($__internal_22cd672eb1085d69f34989d2b8123de2ee8d91996ddda5d0463681dd80ab0a65_prof);

    }

    // line 18
    public function block_navbar_title($context, array $blocks = array())
    {
        $__internal_5295e2facfe166f5d71fc1637719ed3146022cbba3d9bc56f1046ea59a0050a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5295e2facfe166f5d71fc1637719ed3146022cbba3d9bc56f1046ea59a0050a6->enter($__internal_5295e2facfe166f5d71fc1637719ed3146022cbba3d9bc56f1046ea59a0050a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navbar_title"));

        $__internal_b240c51969ba068b3e482e4de5e739186f81740868ddfd34de1b20cb5f7ed3a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b240c51969ba068b3e482e4de5e739186f81740868ddfd34de1b20cb5f7ed3a5->enter($__internal_b240c51969ba068b3e482e4de5e739186f81740868ddfd34de1b20cb5f7ed3a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navbar_title"));

        // line 19
        echo "    ";
        $this->displayBlock("title", $context, $blocks);
        echo "
";
        
        $__internal_b240c51969ba068b3e482e4de5e739186f81740868ddfd34de1b20cb5f7ed3a5->leave($__internal_b240c51969ba068b3e482e4de5e739186f81740868ddfd34de1b20cb5f7ed3a5_prof);

        
        $__internal_5295e2facfe166f5d71fc1637719ed3146022cbba3d9bc56f1046ea59a0050a6->leave($__internal_5295e2facfe166f5d71fc1637719ed3146022cbba3d9bc56f1046ea59a0050a6_prof);

    }

    // line 22
    public function block_actions($context, array $blocks = array())
    {
        $__internal_cc78ac6705999094ff84e6212768460e7135267447fdcf4ab8982167fff0e904 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc78ac6705999094ff84e6212768460e7135267447fdcf4ab8982167fff0e904->enter($__internal_cc78ac6705999094ff84e6212768460e7135267447fdcf4ab8982167fff0e904_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        $__internal_d139aa177287a15bed6652e30176a888163b632a7f7fcced09c0a6c75965e642 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d139aa177287a15bed6652e30176a888163b632a7f7fcced09c0a6c75965e642->enter($__internal_d139aa177287a15bed6652e30176a888163b632a7f7fcced09c0a6c75965e642_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 23
        $this->loadTemplate("SonataAdminBundle:CRUD:action_buttons.html.twig", "SonataAdminBundle:CRUD:base_show.html.twig", 23)->display($context);
        
        $__internal_d139aa177287a15bed6652e30176a888163b632a7f7fcced09c0a6c75965e642->leave($__internal_d139aa177287a15bed6652e30176a888163b632a7f7fcced09c0a6c75965e642_prof);

        
        $__internal_cc78ac6705999094ff84e6212768460e7135267447fdcf4ab8982167fff0e904->leave($__internal_cc78ac6705999094ff84e6212768460e7135267447fdcf4ab8982167fff0e904_prof);

    }

    // line 26
    public function block_tab_menu($context, array $blocks = array())
    {
        $__internal_00752dded39dda855cbd725e255ca8676a6b79782838b5f10ee29ebcd60a9ef7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_00752dded39dda855cbd725e255ca8676a6b79782838b5f10ee29ebcd60a9ef7->enter($__internal_00752dded39dda855cbd725e255ca8676a6b79782838b5f10ee29ebcd60a9ef7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        $__internal_986d310d932b92893b3024e6000c4e33800a4a2a742830dd0c811920a4411df2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_986d310d932b92893b3024e6000c4e33800a4a2a742830dd0c811920a4411df2->enter($__internal_986d310d932b92893b3024e6000c4e33800a4a2a742830dd0c811920a4411df2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        // line 27
        echo "    ";
        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "sidemenu", array(0 => (isset($context["action"]) ? $context["action"] : $this->getContext($context, "action"))), "method"), array("currentClass" => "active", "template" => $this->getAttribute($this->getAttribute(        // line 29
(isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getTemplate", array(0 => "tab_menu_template"), "method")), "twig");
        // line 30
        echo "
";
        
        $__internal_986d310d932b92893b3024e6000c4e33800a4a2a742830dd0c811920a4411df2->leave($__internal_986d310d932b92893b3024e6000c4e33800a4a2a742830dd0c811920a4411df2_prof);

        
        $__internal_00752dded39dda855cbd725e255ca8676a6b79782838b5f10ee29ebcd60a9ef7->leave($__internal_00752dded39dda855cbd725e255ca8676a6b79782838b5f10ee29ebcd60a9ef7_prof);

    }

    // line 33
    public function block_show($context, array $blocks = array())
    {
        $__internal_139d36d3d696cf4b128606e438d4390eea65df573f332506636f842a8f374412 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_139d36d3d696cf4b128606e438d4390eea65df573f332506636f842a8f374412->enter($__internal_139d36d3d696cf4b128606e438d4390eea65df573f332506636f842a8f374412_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show"));

        $__internal_32d7cb3fb70f83b2d3a90b83e6a59684458990e3c8e7775d24d2f3b4fb6d8a78 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32d7cb3fb70f83b2d3a90b83e6a59684458990e3c8e7775d24d2f3b4fb6d8a78->enter($__internal_32d7cb3fb70f83b2d3a90b83e6a59684458990e3c8e7775d24d2f3b4fb6d8a78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show"));

        // line 34
        echo "    <div class=\"sonata-ba-view\">

        ";
        // line 36
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), array("sonata.admin.show.top", array("admin" => (isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "object" => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object")))));
        echo "

        ";
        // line 38
        $context["has_tab"] = (((twig_length_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "showtabs", array())) == 1) && ($this->getAttribute(twig_get_array_keys_filter($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "showtabs", array())), 0, array(), "array") != "default")) || (twig_length_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "showtabs", array())) > 1));
        // line 39
        echo "
        ";
        // line 40
        if ((isset($context["has_tab"]) ? $context["has_tab"] : $this->getContext($context, "has_tab"))) {
            // line 41
            echo "            <div class=\"nav-tabs-custom\">
                <ul class=\"nav nav-tabs\" role=\"tablist\">
                    ";
            // line 43
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "showtabs", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["name"] => $context["show_tab"]) {
                // line 44
                echo "                        <li";
                if ($this->getAttribute($context["loop"], "first", array())) {
                    echo " class=\"active\"";
                }
                echo ">
                            <a href=\"#tab_";
                // line 45
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "uniqid", array()), "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                echo "\" data-toggle=\"tab\">
                                <i class=\"fa fa-exclamation-circle has-errors hide\" aria-hidden=\"true\"></i>
                                ";
                // line 47
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($context["show_tab"], "label", array()), array(), (($this->getAttribute($context["show_tab"], "translation_domain", array())) ? ($this->getAttribute($context["show_tab"], "translation_domain", array())) : ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "translationDomain", array())))), "html", null, true);
                echo "
                            </a>
                        </li>
                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['name'], $context['show_tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "                </ul>

                <div class=\"tab-content\">
                    ";
            // line 54
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "showtabs", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["code"] => $context["show_tab"]) {
                // line 55
                echo "                        <div
                                class=\"tab-pane fade";
                // line 56
                if ($this->getAttribute($context["loop"], "first", array())) {
                    echo " in active";
                }
                echo "\"
                                id=\"tab_";
                // line 57
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "uniqid", array()), "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                echo "\"
                        >
                            <div class=\"box-body  container-fluid\">
                                <div class=\"sonata-ba-collapsed-fields\">
                                    ";
                // line 61
                if (($this->getAttribute($context["show_tab"], "description", array()) != false)) {
                    // line 62
                    echo "                                        <p>";
                    echo $this->getAttribute($context["show_tab"], "description", array());
                    echo "</p>
                                    ";
                }
                // line 64
                echo "
                                    ";
                // line 65
                $context["groups"] = $this->getAttribute($context["show_tab"], "groups", array());
                // line 66
                echo "                                    ";
                $this->displayBlock("show_groups", $context, $blocks);
                echo "
                                </div>
                            </div>
                        </div>
                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['code'], $context['show_tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 71
            echo "                </div>
            </div>
        ";
        } elseif (twig_test_iterable($this->getAttribute(        // line 73
(isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "showtabs", array()))) {
            // line 74
            echo "            ";
            $context["groups"] = $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "showtabs", array()), "default", array()), "groups", array());
            // line 75
            echo "            ";
            $this->displayBlock("show_groups", $context, $blocks);
            echo "
        ";
        }
        // line 77
        echo "
    </div>

    ";
        // line 80
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), array("sonata.admin.show.bottom", array("admin" => (isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "object" => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object")))));
        echo "
";
        
        $__internal_32d7cb3fb70f83b2d3a90b83e6a59684458990e3c8e7775d24d2f3b4fb6d8a78->leave($__internal_32d7cb3fb70f83b2d3a90b83e6a59684458990e3c8e7775d24d2f3b4fb6d8a78_prof);

        
        $__internal_139d36d3d696cf4b128606e438d4390eea65df573f332506636f842a8f374412->leave($__internal_139d36d3d696cf4b128606e438d4390eea65df573f332506636f842a8f374412_prof);

    }

    // line 83
    public function block_show_groups($context, array $blocks = array())
    {
        $__internal_96d25c8d1638dea7f322fca32f7574ae8b04b7c7b3f07ac9847f1299ba108076 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96d25c8d1638dea7f322fca32f7574ae8b04b7c7b3f07ac9847f1299ba108076->enter($__internal_96d25c8d1638dea7f322fca32f7574ae8b04b7c7b3f07ac9847f1299ba108076_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_groups"));

        $__internal_1d91c8460ceaf7abc1fc11a0c689aad8da34757cd2b0ce78253100687cc66fbe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d91c8460ceaf7abc1fc11a0c689aad8da34757cd2b0ce78253100687cc66fbe->enter($__internal_1d91c8460ceaf7abc1fc11a0c689aad8da34757cd2b0ce78253100687cc66fbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_groups"));

        // line 84
        echo "    <div class=\"row\">
        ";
        // line 85
        $this->displayBlock('field_row', $context, $blocks);
        // line 117
        echo "
    </div>
";
        
        $__internal_1d91c8460ceaf7abc1fc11a0c689aad8da34757cd2b0ce78253100687cc66fbe->leave($__internal_1d91c8460ceaf7abc1fc11a0c689aad8da34757cd2b0ce78253100687cc66fbe_prof);

        
        $__internal_96d25c8d1638dea7f322fca32f7574ae8b04b7c7b3f07ac9847f1299ba108076->leave($__internal_96d25c8d1638dea7f322fca32f7574ae8b04b7c7b3f07ac9847f1299ba108076_prof);

    }

    // line 85
    public function block_field_row($context, array $blocks = array())
    {
        $__internal_40f478bb2ed7e9de0ebc73a5e7c298c0538a76df0a9eecc1efd701b9c0da18e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40f478bb2ed7e9de0ebc73a5e7c298c0538a76df0a9eecc1efd701b9c0da18e2->enter($__internal_40f478bb2ed7e9de0ebc73a5e7c298c0538a76df0a9eecc1efd701b9c0da18e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_row"));

        $__internal_5c5cc553a60dee506dc66bd9bddd810f865f0287cfa145728cc1f91375c28697 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c5cc553a60dee506dc66bd9bddd810f865f0287cfa145728cc1f91375c28697->enter($__internal_5c5cc553a60dee506dc66bd9bddd810f865f0287cfa145728cc1f91375c28697_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_row"));

        // line 86
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["groups"]) ? $context["groups"] : $this->getContext($context, "groups")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["code"]) {
            // line 87
            echo "                ";
            $context["show_group"] = $this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "showgroups", array()), $context["code"], array(), "array");
            // line 88
            echo "
                <div class=\"";
            // line 89
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : null), "class", array()), "col-md-12")) : ("col-md-12")), "html", null, true);
            echo " ";
            echo ((((array_key_exists("no_padding", $context)) ? (_twig_default_filter((isset($context["no_padding"]) ? $context["no_padding"] : $this->getContext($context, "no_padding")), false)) : (false))) ? ("nopadding") : (""));
            echo "\">
                    <div class=\"";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : $this->getContext($context, "show_group")), "box_class", array()), "html", null, true);
            echo "\">
                        <div class=\"box-header\">
                            <h4 class=\"box-title\">
                                ";
            // line 93
            $this->displayBlock('show_title', $context, $blocks);
            // line 96
            echo "                            </h4>
                        </div>
                        <div class=\"box-body table-responsive no-padding\">
                            <table class=\"table\">
                                <tbody>
                                ";
            // line 101
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : $this->getContext($context, "show_group")), "fields", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["field_name"]) {
                // line 102
                echo "                                    ";
                $this->displayBlock('show_field', $context, $blocks);
                // line 109
                echo "                                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field_name'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 110
            echo "                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['code'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 116
        echo "        ";
        
        $__internal_5c5cc553a60dee506dc66bd9bddd810f865f0287cfa145728cc1f91375c28697->leave($__internal_5c5cc553a60dee506dc66bd9bddd810f865f0287cfa145728cc1f91375c28697_prof);

        
        $__internal_40f478bb2ed7e9de0ebc73a5e7c298c0538a76df0a9eecc1efd701b9c0da18e2->leave($__internal_40f478bb2ed7e9de0ebc73a5e7c298c0538a76df0a9eecc1efd701b9c0da18e2_prof);

    }

    // line 93
    public function block_show_title($context, array $blocks = array())
    {
        $__internal_50ca84dcd9322457f5c763a75a30976e396a8f73af0b7d75713cf378a174e33a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50ca84dcd9322457f5c763a75a30976e396a8f73af0b7d75713cf378a174e33a->enter($__internal_50ca84dcd9322457f5c763a75a30976e396a8f73af0b7d75713cf378a174e33a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_title"));

        $__internal_2c71f875190250019c8effdd8aaf49f0424e8ffb6d815638620b478f09fff6f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c71f875190250019c8effdd8aaf49f0424e8ffb6d815638620b478f09fff6f3->enter($__internal_2c71f875190250019c8effdd8aaf49f0424e8ffb6d815638620b478f09fff6f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_title"));

        // line 94
        echo "                                    ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : $this->getContext($context, "show_group")), "label", array()), array(), (($this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : null), "translation_domain", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : null), "translation_domain", array()), $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "translationDomain", array()))) : ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "translationDomain", array())))), "html", null, true);
        echo "
                                ";
        
        $__internal_2c71f875190250019c8effdd8aaf49f0424e8ffb6d815638620b478f09fff6f3->leave($__internal_2c71f875190250019c8effdd8aaf49f0424e8ffb6d815638620b478f09fff6f3_prof);

        
        $__internal_50ca84dcd9322457f5c763a75a30976e396a8f73af0b7d75713cf378a174e33a->leave($__internal_50ca84dcd9322457f5c763a75a30976e396a8f73af0b7d75713cf378a174e33a_prof);

    }

    // line 102
    public function block_show_field($context, array $blocks = array())
    {
        $__internal_77787b893dbe273d1f52737c77579cb2d06b1ed9bac595256fd80204728ab58a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_77787b893dbe273d1f52737c77579cb2d06b1ed9bac595256fd80204728ab58a->enter($__internal_77787b893dbe273d1f52737c77579cb2d06b1ed9bac595256fd80204728ab58a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_field"));

        $__internal_165c875eceaea53614aea2d885435601b839c8c69383eb560923c406b61bb142 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_165c875eceaea53614aea2d885435601b839c8c69383eb560923c406b61bb142->enter($__internal_165c875eceaea53614aea2d885435601b839c8c69383eb560923c406b61bb142_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_field"));

        // line 103
        echo "                                        <tr class=\"sonata-ba-view-container\">
                                            ";
        // line 104
        if ($this->getAttribute((isset($context["elements"]) ? $context["elements"] : null), (isset($context["field_name"]) ? $context["field_name"] : $this->getContext($context, "field_name")), array(), "array", true, true)) {
            // line 105
            echo "                                                ";
            echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderViewElement($this->env, $this->getAttribute((isset($context["elements"]) ? $context["elements"] : $this->getContext($context, "elements")), (isset($context["field_name"]) ? $context["field_name"] : $this->getContext($context, "field_name")), array(), "array"), (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object")));
            echo "
                                            ";
        }
        // line 107
        echo "                                        </tr>
                                    ";
        
        $__internal_165c875eceaea53614aea2d885435601b839c8c69383eb560923c406b61bb142->leave($__internal_165c875eceaea53614aea2d885435601b839c8c69383eb560923c406b61bb142_prof);

        
        $__internal_77787b893dbe273d1f52737c77579cb2d06b1ed9bac595256fd80204728ab58a->leave($__internal_77787b893dbe273d1f52737c77579cb2d06b1ed9bac595256fd80204728ab58a_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  502 => 107,  496 => 105,  494 => 104,  491 => 103,  482 => 102,  469 => 94,  460 => 93,  450 => 116,  431 => 110,  417 => 109,  414 => 102,  397 => 101,  390 => 96,  388 => 93,  382 => 90,  376 => 89,  373 => 88,  370 => 87,  352 => 86,  343 => 85,  331 => 117,  329 => 85,  326 => 84,  317 => 83,  305 => 80,  300 => 77,  294 => 75,  291 => 74,  289 => 73,  285 => 71,  265 => 66,  263 => 65,  260 => 64,  254 => 62,  252 => 61,  243 => 57,  237 => 56,  234 => 55,  217 => 54,  212 => 51,  194 => 47,  187 => 45,  180 => 44,  163 => 43,  159 => 41,  157 => 40,  154 => 39,  152 => 38,  147 => 36,  143 => 34,  134 => 33,  123 => 30,  121 => 29,  119 => 27,  110 => 26,  100 => 23,  91 => 22,  78 => 19,  69 => 18,  56 => 15,  47 => 14,  26 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block title %}
    {{ \"title_show\"|trans({'%name%': admin.toString(object)|truncate(15) }, 'SonataAdminBundle') }}
{% endblock %}

{% block navbar_title %}
    {{ block('title') }}
{% endblock %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:action_buttons.html.twig' %}
{%- endblock -%}

{% block tab_menu %}
    {{ knp_menu_render(admin.sidemenu(action), {
        'currentClass' : 'active',
        'template': sonata_admin.adminPool.getTemplate('tab_menu_template')
    }, 'twig') }}
{% endblock %}

{% block show %}
    <div class=\"sonata-ba-view\">

        {{ sonata_block_render_event('sonata.admin.show.top', { 'admin': admin, 'object': object }) }}

        {% set has_tab = (admin.showtabs|length == 1 and admin.showtabs|keys[0] != 'default') or admin.showtabs|length > 1 %}

        {% if has_tab %}
            <div class=\"nav-tabs-custom\">
                <ul class=\"nav nav-tabs\" role=\"tablist\">
                    {% for name, show_tab in admin.showtabs %}
                        <li{% if loop.first %} class=\"active\"{% endif %}>
                            <a href=\"#tab_{{ admin.uniqid }}_{{ loop.index }}\" data-toggle=\"tab\">
                                <i class=\"fa fa-exclamation-circle has-errors hide\" aria-hidden=\"true\"></i>
                                {{ show_tab.label|trans({}, show_tab.translation_domain ?: admin.translationDomain) }}
                            </a>
                        </li>
                    {% endfor %}
                </ul>

                <div class=\"tab-content\">
                    {% for code, show_tab in admin.showtabs %}
                        <div
                                class=\"tab-pane fade{% if loop.first %} in active{% endif %}\"
                                id=\"tab_{{ admin.uniqid }}_{{ loop.index }}\"
                        >
                            <div class=\"box-body  container-fluid\">
                                <div class=\"sonata-ba-collapsed-fields\">
                                    {% if show_tab.description != false %}
                                        <p>{{ show_tab.description|raw }}</p>
                                    {% endif %}

                                    {% set groups = show_tab.groups %}
                                    {{ block('show_groups') }}
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                </div>
            </div>
        {% elseif admin.showtabs is iterable %}
            {% set groups = admin.showtabs.default.groups %}
            {{ block('show_groups') }}
        {% endif %}

    </div>

    {{ sonata_block_render_event('sonata.admin.show.bottom', { 'admin': admin, 'object': object }) }}
{% endblock %}

{% block show_groups %}
    <div class=\"row\">
        {% block field_row %}
            {% for code in groups %}
                {% set show_group = admin.showgroups[code] %}

                <div class=\"{{ show_group.class|default('col-md-12') }} {{ no_padding|default(false) ? 'nopadding' }}\">
                    <div class=\"{{ show_group.box_class }}\">
                        <div class=\"box-header\">
                            <h4 class=\"box-title\">
                                {% block show_title %}
                                    {{ show_group.label|trans({}, show_group.translation_domain|default(admin.translationDomain)) }}
                                {% endblock %}
                            </h4>
                        </div>
                        <div class=\"box-body table-responsive no-padding\">
                            <table class=\"table\">
                                <tbody>
                                {% for field_name in show_group.fields %}
                                    {% block show_field %}
                                        <tr class=\"sonata-ba-view-container\">
                                            {% if elements[field_name] is defined %}
                                                {{ elements[field_name]|render_view_element(object)}}
                                            {% endif %}
                                        </tr>
                                    {% endblock %}
                                {% endfor %}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            {% endfor %}
        {% endblock %}

    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:base_show.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/base_show.html.twig");
    }
}
