<?php

/* SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig */
class __TwigTemplate_5e1020207a55f109134a7fcd6bce11e1a587759e59c7a99bb796fad8b1ad28fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:Form:filter_admin_fields.html.twig", "SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Form:filter_admin_fields.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_569eea8baf4b3e973640701ad7868deebfd237155e8bfee9da9471965dbe4a7f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_569eea8baf4b3e973640701ad7868deebfd237155e8bfee9da9471965dbe4a7f->enter($__internal_569eea8baf4b3e973640701ad7868deebfd237155e8bfee9da9471965dbe4a7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig"));

        $__internal_fc5d6d5711cc3fc001e22b4a2bdafc617b408518e5615ca6162c5e151451ff1e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc5d6d5711cc3fc001e22b4a2bdafc617b408518e5615ca6162c5e151451ff1e->enter($__internal_fc5d6d5711cc3fc001e22b4a2bdafc617b408518e5615ca6162c5e151451ff1e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_569eea8baf4b3e973640701ad7868deebfd237155e8bfee9da9471965dbe4a7f->leave($__internal_569eea8baf4b3e973640701ad7868deebfd237155e8bfee9da9471965dbe4a7f_prof);

        
        $__internal_fc5d6d5711cc3fc001e22b4a2bdafc617b408518e5615ca6162c5e151451ff1e->leave($__internal_fc5d6d5711cc3fc001e22b4a2bdafc617b408518e5615ca6162c5e151451ff1e_prof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:Form:filter_admin_fields.html.twig' %}
", "SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\doctrine-orm-admin-bundle/Resources/views/Form/filter_admin_fields.html.twig");
    }
}
