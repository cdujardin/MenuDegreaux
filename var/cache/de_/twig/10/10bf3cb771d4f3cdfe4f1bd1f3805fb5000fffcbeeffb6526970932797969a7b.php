<?php

/* menu/scolaire.html.twig */
class __TwigTemplate_0125f67be1d8463db6c0e9a2528f1e5431ee69bec201bb804eabff014c6a72b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "menu/scolaire.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_44e9f78eec3055ce4f328d908e20284933d8b703767fa8a0d2c966b694af2dbc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44e9f78eec3055ce4f328d908e20284933d8b703767fa8a0d2c966b694af2dbc->enter($__internal_44e9f78eec3055ce4f328d908e20284933d8b703767fa8a0d2c966b694af2dbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "menu/scolaire.html.twig"));

        $__internal_a7ff083615e66a8f6df9cda070e56b4bcefed8e6145713dbb1d669f3b071d632 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7ff083615e66a8f6df9cda070e56b4bcefed8e6145713dbb1d669f3b071d632->enter($__internal_a7ff083615e66a8f6df9cda070e56b4bcefed8e6145713dbb1d669f3b071d632_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "menu/scolaire.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_44e9f78eec3055ce4f328d908e20284933d8b703767fa8a0d2c966b694af2dbc->leave($__internal_44e9f78eec3055ce4f328d908e20284933d8b703767fa8a0d2c966b694af2dbc_prof);

        
        $__internal_a7ff083615e66a8f6df9cda070e56b4bcefed8e6145713dbb1d669f3b071d632->leave($__internal_a7ff083615e66a8f6df9cda070e56b4bcefed8e6145713dbb1d669f3b071d632_prof);

    }

    // line 5
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_75bc797a8e77a398b8540425dc710b44630a2da10cf3e0dff3edc8797da99df6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75bc797a8e77a398b8540425dc710b44630a2da10cf3e0dff3edc8797da99df6->enter($__internal_75bc797a8e77a398b8540425dc710b44630a2da10cf3e0dff3edc8797da99df6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_9ad8ceba6733f875383f7564e158c95daf6bc476be5c828c90871a8c3bcbed4a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ad8ceba6733f875383f7564e158c95daf6bc476be5c828c90871a8c3bcbed4a->enter($__internal_9ad8ceba6733f875383f7564e158c95daf6bc476be5c828c90871a8c3bcbed4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "menu_scolaire";
        
        $__internal_9ad8ceba6733f875383f7564e158c95daf6bc476be5c828c90871a8c3bcbed4a->leave($__internal_9ad8ceba6733f875383f7564e158c95daf6bc476be5c828c90871a8c3bcbed4a_prof);

        
        $__internal_75bc797a8e77a398b8540425dc710b44630a2da10cf3e0dff3edc8797da99df6->leave($__internal_75bc797a8e77a398b8540425dc710b44630a2da10cf3e0dff3edc8797da99df6_prof);

    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        $__internal_18f0fb3b86549a33366a91cbf9e8cc65594b3bbd994d4f343a9146b9dd71611c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_18f0fb3b86549a33366a91cbf9e8cc65594b3bbd994d4f343a9146b9dd71611c->enter($__internal_18f0fb3b86549a33366a91cbf9e8cc65594b3bbd994d4f343a9146b9dd71611c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_b57d0e88c284560e94826a5bd4fab71e71bda8dfbf642e26f6871a1ae081494f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b57d0e88c284560e94826a5bd4fab71e71bda8dfbf642e26f6871a1ae081494f->enter($__internal_b57d0e88c284560e94826a5bd4fab71e71bda8dfbf642e26f6871a1ae081494f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 8
        echo "<h1> Menu Scolaire</h1>
<p class=\"subtitle\"> Menu du midi uniquement</p>

            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Lundi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Mardi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Mercredi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Jeudi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>


            </div>



";
        
        $__internal_b57d0e88c284560e94826a5bd4fab71e71bda8dfbf642e26f6871a1ae081494f->leave($__internal_b57d0e88c284560e94826a5bd4fab71e71bda8dfbf642e26f6871a1ae081494f_prof);

        
        $__internal_18f0fb3b86549a33366a91cbf9e8cc65594b3bbd994d4f343a9146b9dd71611c->leave($__internal_18f0fb3b86549a33366a91cbf9e8cc65594b3bbd994d4f343a9146b9dd71611c_prof);

    }

    // line 93
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_09f3f0b521f56b331326036ad4d38576270309202fe81e017450f992f5b83d98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09f3f0b521f56b331326036ad4d38576270309202fe81e017450f992f5b83d98->enter($__internal_09f3f0b521f56b331326036ad4d38576270309202fe81e017450f992f5b83d98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_d9bbc05ec8e042e4d1e40b311004691c1d8d48e130b266f4c8eb2abfda16fbd3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9bbc05ec8e042e4d1e40b311004691c1d8d48e130b266f4c8eb2abfda16fbd3->enter($__internal_d9bbc05ec8e042e4d1e40b311004691c1d8d48e130b266f4c8eb2abfda16fbd3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 94
        echo "<p class=\"titre\">
    Voir le menu :
</p>
<div>
    <a href=\"";
        // line 98
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
        Externe
    </a>
</div>
<div>
    <a  href=\"";
        // line 103
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
        Scolaire
    </a>
</div>
<div>
    <a href=\"";
        // line 108
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
        Résident
    </a>
</div>



";
        
        $__internal_d9bbc05ec8e042e4d1e40b311004691c1d8d48e130b266f4c8eb2abfda16fbd3->leave($__internal_d9bbc05ec8e042e4d1e40b311004691c1d8d48e130b266f4c8eb2abfda16fbd3_prof);

        
        $__internal_09f3f0b521f56b331326036ad4d38576270309202fe81e017450f992f5b83d98->leave($__internal_09f3f0b521f56b331326036ad4d38576270309202fe81e017450f992f5b83d98_prof);

    }

    public function getTemplateName()
    {
        return "menu/scolaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  253 => 108,  245 => 103,  237 => 98,  231 => 94,  222 => 93,  201 => 81,  197 => 80,  193 => 79,  189 => 78,  175 => 67,  171 => 66,  167 => 65,  163 => 64,  146 => 50,  142 => 49,  138 => 48,  134 => 47,  120 => 36,  116 => 35,  112 => 34,  108 => 33,  94 => 22,  90 => 21,  86 => 20,  82 => 19,  69 => 8,  60 => 7,  42 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}



{% block body_id 'menu_scolaire' %}

{% block main %}
<h1> Menu Scolaire</h1>
<p class=\"subtitle\"> Menu du midi uniquement</p>

            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Lundi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.lundiEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.lundiPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.lundiAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.lundiDessert }}</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Mardi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.mardiEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.mardiPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.mardiAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.mardiDessert }}</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Mercredi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.mercrediEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.mercrediPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.mercrediAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.mercrediDessert }}</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Jeudi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.jeudiEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.jeudiPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.jeudiAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.jeudiDessert }}</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.vendrediEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.vendrediPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.vendrediAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.vendrediDessert }}</div>
                        </div>
                    </div>
                </div>


            </div>



{% endblock %}

{% block sidebar %}
<p class=\"titre\">
    Voir le menu :
</p>
<div>
    <a href=\"{{ path('externe') }}\" >
        Externe
    </a>
</div>
<div>
    <a  href=\"{{ path('scolaire') }}\">
        Scolaire
    </a>
</div>
<div>
    <a href=\"{{ path('resident') }}\" >
        Résident
    </a>
</div>



{% endblock %}
", "menu/scolaire.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\menu\\scolaire.html.twig");
    }
}
