<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_7704cf80f4a2d9fbd0c9900ebb485c0c6ccfc65ecb164bd475819f68afb19026 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_33a35999a3fba42dae6f661a9d035fa37dd7ec4d467401034d2187d944fc81bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_33a35999a3fba42dae6f661a9d035fa37dd7ec4d467401034d2187d944fc81bc->enter($__internal_33a35999a3fba42dae6f661a9d035fa37dd7ec4d467401034d2187d944fc81bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_e09daeecf1a291fc0f1acc1379ef24cc60afd0432a19e8ebf7536dc5ed66e766 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e09daeecf1a291fc0f1acc1379ef24cc60afd0432a19e8ebf7536dc5ed66e766->enter($__internal_e09daeecf1a291fc0f1acc1379ef24cc60afd0432a19e8ebf7536dc5ed66e766_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_33a35999a3fba42dae6f661a9d035fa37dd7ec4d467401034d2187d944fc81bc->leave($__internal_33a35999a3fba42dae6f661a9d035fa37dd7ec4d467401034d2187d944fc81bc_prof);

        
        $__internal_e09daeecf1a291fc0f1acc1379ef24cc60afd0432a19e8ebf7536dc5ed66e766->leave($__internal_e09daeecf1a291fc0f1acc1379ef24cc60afd0432a19e8ebf7536dc5ed66e766_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_e3e6cca9315f025a6be12bb6e5b8edf11e1dd93e692921c010d5875b511b5afc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e3e6cca9315f025a6be12bb6e5b8edf11e1dd93e692921c010d5875b511b5afc->enter($__internal_e3e6cca9315f025a6be12bb6e5b8edf11e1dd93e692921c010d5875b511b5afc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_8837af4fc6c14fbc20e41e4944dae3bef14fccdf9275908763d6fada7836715f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8837af4fc6c14fbc20e41e4944dae3bef14fccdf9275908763d6fada7836715f->enter($__internal_8837af4fc6c14fbc20e41e4944dae3bef14fccdf9275908763d6fada7836715f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_8837af4fc6c14fbc20e41e4944dae3bef14fccdf9275908763d6fada7836715f->leave($__internal_8837af4fc6c14fbc20e41e4944dae3bef14fccdf9275908763d6fada7836715f_prof);

        
        $__internal_e3e6cca9315f025a6be12bb6e5b8edf11e1dd93e692921c010d5875b511b5afc->leave($__internal_e3e6cca9315f025a6be12bb6e5b8edf11e1dd93e692921c010d5875b511b5afc_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
