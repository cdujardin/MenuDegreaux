<?php

/* SonataBlockBundle:Block:block_exception.html.twig */
class __TwigTemplate_aa1a344548a5a33bda6212758f0ecc6c0df92a7bfb63394da8bd8e93be546165 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_exception.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_98a2377c4631cc653e579f76d5f8d779305fc08fe2ac357f0c3860ba389deaf3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_98a2377c4631cc653e579f76d5f8d779305fc08fe2ac357f0c3860ba389deaf3->enter($__internal_98a2377c4631cc653e579f76d5f8d779305fc08fe2ac357f0c3860ba389deaf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_exception.html.twig"));

        $__internal_1f4a2c9cbfd18e7f4151f5f2215085eb34e1996798050dd6d3b688d8fec85a60 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f4a2c9cbfd18e7f4151f5f2215085eb34e1996798050dd6d3b688d8fec85a60->enter($__internal_1f4a2c9cbfd18e7f4151f5f2215085eb34e1996798050dd6d3b688d8fec85a60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_exception.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_98a2377c4631cc653e579f76d5f8d779305fc08fe2ac357f0c3860ba389deaf3->leave($__internal_98a2377c4631cc653e579f76d5f8d779305fc08fe2ac357f0c3860ba389deaf3_prof);

        
        $__internal_1f4a2c9cbfd18e7f4151f5f2215085eb34e1996798050dd6d3b688d8fec85a60->leave($__internal_1f4a2c9cbfd18e7f4151f5f2215085eb34e1996798050dd6d3b688d8fec85a60_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_b61afb8b47c841fb141906f383f8854ee01807329f9e37352fbf41d3019dc7f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b61afb8b47c841fb141906f383f8854ee01807329f9e37352fbf41d3019dc7f4->enter($__internal_b61afb8b47c841fb141906f383f8854ee01807329f9e37352fbf41d3019dc7f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_a899647209b9cceb28dab64a2b20423c8e2f81c6b4f963c58ec727770d60e54b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a899647209b9cceb28dab64a2b20423c8e2f81c6b4f963c58ec727770d60e54b->enter($__internal_a899647209b9cceb28dab64a2b20423c8e2f81c6b4f963c58ec727770d60e54b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <div class=\"cms-block-exception\">
        <h2>";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["block"]) ? $context["block"] : $this->getContext($context, "block")), "name", array()), "html", null, true);
        echo "</h2>
        <h3>";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo "</h3>
    </div>
";
        
        $__internal_a899647209b9cceb28dab64a2b20423c8e2f81c6b4f963c58ec727770d60e54b->leave($__internal_a899647209b9cceb28dab64a2b20423c8e2f81c6b4f963c58ec727770d60e54b_prof);

        
        $__internal_b61afb8b47c841fb141906f383f8854ee01807329f9e37352fbf41d3019dc7f4->leave($__internal_b61afb8b47c841fb141906f383f8854ee01807329f9e37352fbf41d3019dc7f4_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    <div class=\"cms-block-exception\">
        <h2>{{ block.name }}</h2>
        <h3>{{ exception.message }}</h3>
    </div>
{% endblock %}
", "SonataBlockBundle:Block:block_exception.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle/Resources/views/Block/block_exception.html.twig");
    }
}
