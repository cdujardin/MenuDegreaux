<?php

/* SonataAdminBundle:CRUD:edit_sonata_type_immutable_array.html.twig */
class __TwigTemplate_1a3d645120e6e88fd19c4eaaf812aa082ffa7063ca29d8b0118d049d071861c5 extends Twig_Template
{
    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:edit_sonata_type_immutable_array.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f1565ef26f47455889b43b60201586664f6981ed059285336d3856a5ae9b8f3f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f1565ef26f47455889b43b60201586664f6981ed059285336d3856a5ae9b8f3f->enter($__internal_f1565ef26f47455889b43b60201586664f6981ed059285336d3856a5ae9b8f3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_sonata_type_immutable_array.html.twig"));

        $__internal_b94f29fd83fc4906848f293c6ee0b597d16fe559a651c3c2ab82511028fde73c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b94f29fd83fc4906848f293c6ee0b597d16fe559a651c3c2ab82511028fde73c->enter($__internal_b94f29fd83fc4906848f293c6ee0b597d16fe559a651c3c2ab82511028fde73c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_sonata_type_immutable_array.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f1565ef26f47455889b43b60201586664f6981ed059285336d3856a5ae9b8f3f->leave($__internal_f1565ef26f47455889b43b60201586664f6981ed059285336d3856a5ae9b8f3f_prof);

        
        $__internal_b94f29fd83fc4906848f293c6ee0b597d16fe559a651c3c2ab82511028fde73c->leave($__internal_b94f29fd83fc4906848f293c6ee0b597d16fe559a651c3c2ab82511028fde73c_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_sonata_type_immutable_array.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  9 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}
", "SonataAdminBundle:CRUD:edit_sonata_type_immutable_array.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/edit_sonata_type_immutable_array.html.twig");
    }
}
